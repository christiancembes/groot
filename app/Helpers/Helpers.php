<?php

function getWeek($id)
{
    if ($id == '01') { return 'W1'; }
    if ($id == '02') { return 'W2'; }
    if ($id == '03') { return 'W3'; }
    if ($id == '04') { return 'W4'; }
}

function getBAUFormat($bau)
{
	$tmp = explode('-', $bau);

	$year = $tmp[0];
    $month = $tmp[1];
    $week = getWeek($tmp[2]);

    return $year.'-'.$month.'-'.$week;
}

function appendQueryString($params) {

	$query = Request::all();
	
	foreach ($params as $key => $value) {
		$query[$key] = $value;
	}

	return Request::url(). '?' . http_build_query($query);
}