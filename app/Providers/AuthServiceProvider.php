<?php

namespace App\Providers;

use App\Models\CabProject;
use App\Models\PostProject;
use App\Models\DeclareProject;
use App\Policies\CabProjectPolicy;
use App\Policies\PostProjectPolicy;
use App\Policies\DeclareProjectPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        DeclareProject::class => DeclareProjectPolicy::class,
        CabProject::class => CabProjectPolicy::class,
        PostProject::class => PostProjectPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
