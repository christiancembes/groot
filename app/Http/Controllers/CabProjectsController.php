<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Models\CabProject;
use App\Models\PostProject;
use Illuminate\Http\Request;
use App\Models\DeclareProject;
use App\Models\ProjectOcsOffer;


class CabProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'create',
            'store',
            'edit',
            'update',
        ]]);
    }

    public function index(Request $request)
    {
        $q = $request->input('q');

        $declare_projects = CabProject::where('project_name', 'LIKE', '%'.$q.'%')->where('phase', 'Plan & Define');

        $total_declare_project = $declare_projects->count();
        $declare_projects = $declare_projects->paginate(10);

        $cab_projects = CabProject::where('project_name', 'LIKE', '%'.$q.'%')
            ->where('phase', 'Design & Develop');

        $total_cab_project = $cab_projects->count();
        $cab_projects = $cab_projects->paginate(10);
    	
    	return view('cab-projects.index', compact(
    		'declare_projects',
    		'cab_projects',
    		'total_declare_project',
    		'total_cab_project'
		));
    }

    public function create($development_id)
    {
    	$cab_project = CabProject::where('development_id', $development_id)->first();

    	return view('cab-projects.create', compact('cab_project'));
    }

    public function store(Request $request, $development_id)
    {
        $this->validate($request, [
            'development_schedule' => 'required',
            'uat_schedule' => 'required',
            'actual_uat_schedule' => 'required',
            'release_schedule' => 'required',
            'actual_release_schedule' => 'required',
            'business_flow' => 'required',
            'bill' => 'required',
            'prov' => 'required',
            'rep' => 'required',
            'status_project' => 'required',
        ]);

        // dd($request->all());

        $development_schedule = $request->input('development_schedule');
        $uat_schedule = $request->input('uat_schedule');
        $actual_uat_schedule = $request->input('actual_uat_schedule');
        $release_schedule = $request->input('release_schedule');
        $actual_release_schedule = $request->input('actual_release_schedule');

        $business_flow = $request->input('business_flow');
        $bill = $request->input('bill');
        $prov = $request->input('prov');
        $rep = $request->input('rep');

        $status_project = $request->input('status_project');

        $cab_project = CabProject::where('development_id', $development_id)->firstOrFail();

        $cab_project->phase = 'Design & Develop';
        $cab_project->development_schedule = $development_schedule;
        $cab_project->uat_schedule = $uat_schedule;
        $cab_project->actual_uat_schedule = $actual_uat_schedule;
        $cab_project->release_schedule = $release_schedule;
        $cab_project->actual_release_schedule = $actual_release_schedule;
        $cab_project->business_flow = $business_flow;
        $cab_project->bill = $bill;
        $cab_project->prov = $prov;
        $cab_project->rep = $rep;
        $cab_project->status_project = $status_project;
        $cab_project->user_id = Auth::user()->id;
        $cab_project->save();

        $post_project = PostProject::where('development_id', $cab_project->development_id)->firstOrFail();
        $post_project->phase = 'Design & Develop';
        $post_project->user_id = Auth::user()->id;
        $post_project->save();

        if ($request->input('billing_offers') != null) {

        	foreach ($request->input('billing_offers') as $offer_id) {

        		$project_ocs_offer = new ProjectOcsOffer;
        		$project_ocs_offer->development_id = $cab_project->development_id;
        		$project_ocs_offer->offer_id = $offer_id;
        		$project_ocs_offer->save();

        	}
        }

        \Flash::success('Create Request on Design & Develop Phase Success!');

        return redirect('/cab-projects/'.$cab_project->development_id);
    }

    public function show($development_id)
    {
    	$cab_project = CabProject::join('declare_projects', 'cab_projects.development_id', '=', 'declare_projects.development_id')
    		->where('cab_projects.development_id', $development_id)
    		->firstOrFail();

    	return view('cab-projects.details', compact('cab_project'));
    }

    public function edit($development_id)
    {
        $cab_project = CabProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $cab_project);

        return view('cab-projects.edit', compact('cab_project'));
    }

    public function update(Request $request, $development_id)
    {
        $this->validate($request, [
            'development_schedule' => 'required',
            'uat_schedule' => 'required',
            'actual_uat_schedule' => 'required',
            'release_schedule' => 'required',
            'actual_release_schedule' => 'required',
            'business_flow' => 'required',
            'bill' => 'required',
            'prov' => 'required',
            'rep' => 'required',
            'status_project' => 'required',
        ]);

        // dd($request->all());

        $cab_project = CabProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $cab_project);

        $development_schedule = $request->input('development_schedule');
        $uat_schedule = $request->input('uat_schedule');
        $actual_uat_schedule = $request->input('actual_uat_schedule');
        $release_schedule = $request->input('release_schedule');
        $actual_release_schedule = $request->input('actual_release_schedule');

        $business_flow = $request->input('business_flow');
        $bill = $request->input('bill');
        $prov = $request->input('prov');
        $rep = $request->input('rep');

        $status_project = $request->input('status_project');

        $cab_project->phase = 'Design & Develop';
        $cab_project->development_schedule = $development_schedule;
        $cab_project->uat_schedule = $uat_schedule;
        $cab_project->actual_uat_schedule = $actual_uat_schedule;
        $cab_project->release_schedule = $release_schedule;
        $cab_project->actual_release_schedule = $actual_release_schedule;
        $cab_project->business_flow = $business_flow;
        $cab_project->bill = $bill;
        $cab_project->prov = $prov;
        $cab_project->rep = $rep;
        $cab_project->status_project = $status_project;
        $cab_project->user_id = Auth::user()->id;
        $cab_project->save();

        if ($request->input('billing_offers') != null) {

			ProjectOcsOffer::where('development_id', $cab_project->development_id)->delete();        	

        	foreach ($request->input('billing_offers') as $offer_id) {

        		$project_ocs_offer = new ProjectOcsOffer;
        		$project_ocs_offer->development_id = $cab_project->development_id;
        		$project_ocs_offer->offer_id = $offer_id;
        		$project_ocs_offer->save();

        	}
        }

        \Flash::success('Update Request Success!');

        return redirect('/cab-projects/'.$cab_project->development_id);
    }


}



















