<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\CabProject;
use App\Models\Activity;

class ActivityCOntroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request){
        $cab_projects = CabProject::where('phase', '=', 'CAB')->where('status_project', '=', 'Development')->get();
        $counter = 1;
        $array = array(
            'cab_projects' => $cab_projects,
            'counter' => $counter
        );
        return view('activity.index')->with($array);
        
    }

    public function monitoring(Request $request){
        $cab = $request->segment(2);
        $new_act = new Activity;
        $cab_project = CabProject::where('development_id', '=', $cab)->first();
        $cab_activity = Activity::where('cab_project_id','=' ,$cab)->get();
        $counter = 1;
        $array = array(
            'new_activity' => $new_act,
            'cab_activities' => $cab_activity,
            'cab_project' => $cab_project,
            'counter' => $counter
        );        
        return view('activity.monitoring')->with($array);
    }
    
    public function process(Request $request){
        $this->validate($request, [
            'cab_project_id' => 'required',
            'activity_name' => 'required',
            'weight' => 'required',
            'target_start_date' => 'required',
            'actual_start_date' => 'required', 
            'target_end_date' => 'required', 
            'actual_end_date' => 'required', 
            'status' => 'required', 
            'remarks' => 'required'
        ]);
        if($request->submit == 'Save') {
            $model = Activity::create($request->all());
            $model->save();
            $request->session()->put('notif', 'value');
            \Flash::success('Create Activity Project Success!');
            return redirect("activity/".$request->cab_project_id);
        }else if($request->submit == 'Update'){
            $id_act = $request->id;
            $model = Activity::find($id_act);
            $model->update($request->all());
            $model->save();
             \Flash::success('Update Activity Project Success!');
            return redirect("activity/".$request->cab_project_id);
        } 
    }
    
    public function edit(Request $request) {
        $id_act = $request->segment(2);
        $activity_model = Activity::where('id', '=', $id_act)->first();
        $array = [
            'activity_row' => $activity_model
        ];
        return view('activity.edit')->with($array);
    }
    
    public function delete(Request $request) {
        $id_act = $request->input('id');
        $model = Activity::find($id_act);
        $model->delete();
        \Flash::success('Delete Activity Project Success!');
        return redirect("activity/".$request->cab_project_id);
    }
}



