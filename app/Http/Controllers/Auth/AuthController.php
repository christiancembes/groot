<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Mail;
use Session;
use App\Models\User;
use App\Models\Role;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/';
    protected $success_ldap = false;
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware('user-should-verified');
        $this->middleware('user-status-active');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
	    'username' => 'required|min:4',
            'password' => 'required|min:6|confirmed',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
	print_r($data);exit();
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
	    'username' => $data['username'],
            'password' => bcrypt('dummy'),
            'is_active' => 1,
        ]);

        $userRole = Role::where('name', 'user')->first();
        $user->attachRole($userRole);

        $user->sendVerification();

        return $user;
    }

    public function sendVerification(Request $request) {

        $user = User::where('email', $request->get('email'))->first();

        if ($user && !$user->is_verified) {
        
            $user->sendVerification();

            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"We already send you an email, please follow the instruction to activate your account."
            ]);
        }
        
        return redirect('/login');
    }

    public function verify(Request $request, $token) {
        
        $email = $request->get('email');
        $user = User::where('verification_token', $token)->where('email', $email)->first();

        if ($user) {

            $user->verify();

            Session::flash("toastr", [
                "title"=>"Hallo",
                "message"=> "Welcome ". $user->name,
            ]);

            Auth::login($user);
        }

        return redirect('/');
    }

    /**
     * Override this method to add flash message after login success
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
	    $fake = array(
            'username' => $request->input('username'),
            'password' => 'dummy',
            );
            return $this->authenticated($fake, Auth::guard($this->getGuard())->user());
        }

        \Flash::info("Welcome ". Auth::user()->name .", you are now logged in.");

        return redirect()->intended($this->redirectPath());
    }

    public function login(Request $request){
        $this->validateLogin($request);

        $throttles = $this->isUsingThrottlesLoginsTrait();
//
        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
//
        $credentials = $this->getCredentials($request);
        $success_ldap = $this->authenticateLDAP($credentials);

        $fake = array('username' => $request->input('username'),'password' => 'dummy');
	
        if (Auth::guard($this->getGuard())->attempt($fake, $request->has('remember'))) {
	    if ($success_ldap) {
                return $this->handleUserWasAuthenticated($request, $throttles);
            }else{
		Auth::logout();
		#PAGE GAGAl//Flash::info("Wrong Password.");	//echo "<script>javascript: alert('Wrong Password')></script>";//return false;	
	    }
        }
    }
    /**
     * Override logout method
     */
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();

        \Flash::info('You are successfully logged out.');

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    protected function authenticateLDAP($param_usr){
        $username = $param_usr['username']; //get from post form login
	$password = $param_usr['password']; //get from post form login
        $data = array();

        //$ldap_host = '10.2.126.63'; //Host LDAP
        $ldap_host = 'ldap://10.250.193.116:389'; //Host LDAP
        $ldap_domain = 'telkomsel.co.id'; // LDAP Domain
        $ldap_dn = 'dc=telkomsel,dc=co,dc=id'; // Domain Component
        //$ldap_user = $username . "@" . $ldap_domain;
        $ldap_user = "telkomsel" . "\\" . $username;
        $ldap_pass = $password;
        $attributes = array("displayname", "cn", "title", "mail", "telephonenumber", "extensionattribute1", "manager", "description", "department", "physicaldeliveryofficename");
        $filter1 = "(&(objectCategory=person)(sAMAccountName=" . $username . "))"; //(telephoneNumber=0811978488))";
        $filter2 = "(&(objectCategory=person)(cn=" . $username . "))";
        $filter3 = "(&(objectCategory=person)(userPrincipalName=" . $username . "@telkomsel.co.id" . "))";
        $ldap_conn = ldap_connect($ldap_host) or die("Could not connect to $ldap_host");
        if ($ldap_conn) {
            ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
            if ($bind = @ldap_bind($ldap_conn, $ldap_user, $ldap_pass)) {
                //$result = ldap_search($ldap_conn, $ldap_dn, $filter); //, $attributes
                $result = ldap_search($ldap_conn, $ldap_dn, $filter1, $attributes); //, $attributes
                $entries = ldap_get_entries($ldap_conn, $result);
                
                if (empty($entries[0]['extensionattribute1'][0])) {
                    $result = ldap_search($ldap_conn, $ldap_dn, $filter2, $attributes);
                }
                if (empty($entries[0]['extensionattribute1'][0])) {
                    $result = ldap_search($ldap_conn, $ldap_dn, $filter3, $attributes);
                }
                $entries = ldap_get_entries($ldap_conn, $result);

                if (!empty($entries[0]['extensionattribute1'][0])) {
                    $data['NAMA'] = $entries[0]['cn'][0];
                    $data['NIK'] = $entries[0]['extensionattribute1'][0];
                    $data['EMAIL'] = $entries[0]['mail'][0];
                    $nik = $data['NIK'];
                    $nama = str_replace("_", " ", $data['NAMA']);
                    $email = $data['EMAIL'];

                    //cek title, tlpn, manager dan department apakah ada di ldap atau tidak
                    if (array_key_exists("title", $entries[0])) {
                        $data['TITLE'] = $entries[0]['title'][0];
                        $title = $data['TITLE'];
                    } else {
                        $title = "";
                    }
                    if (array_key_exists("telephonenumber", $entries[0])) {
                        $data['HP'] = $entries[0]['telephonenumber'][0];
                        $hp = $data['HP'];
                    } else {
                        $hp = "";
                    }
                    if (array_key_exists("manager", $entries[0])) {
                        $data['MANAGER'] = $entries[0]['manager'][0];
                        $managerCN = str_replace("CN=", "", $data['MANAGER']);
                        $managerOU = str_replace(",OU=HeadOffice,DC=telkomsel,DC=co,DC=id", "", $managerCN);
                        $managerOU2 = str_replace(",OU=Colour", "", $managerOU);
                        $manager = str_replace("_", " ", $managerOU2);
                        $managerMail = $managerOU2 . "@" . $ldap_domain;
                    } else {
                        $$manager = "";
                        $managerMail = "";
                    }
                    if (array_key_exists("department", $entries[0])) {
                        $data['DEPARTMENT'] = $entries[0]['department'][0];
                        $department = $data['DEPARTMENT'];
                    } else {
                        $department = "";
                    }
                    $success_ldap = true;
		    return true;
                } else {
                    $success_ldap = false;
		    return false;
                }
            } else {
                $success_ldap = false;
		return false;
            }
        } else {
            $success_ldap = false;
	    return false;
        }
    }
}
