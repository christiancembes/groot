<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Models\PostProject;
use Illuminate\Http\Request;

class PostProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'create',
            'store',
            'edit',
            'update',
        ]]);
    }
    
    public function index(Request $request)
    {
        $q = $request->input('q');

        $cab_projects = PostProject::where('project_name', 'LIKE', '%'.$q.'%')->where('phase', 'Design & Develop');

        $total_cab_project = $cab_projects->count();
        $cab_projects = $cab_projects->paginate(10);

        $post_projects = PostProject::where('project_name', 'LIKE', '%'.$q.'%')->where('phase', 'Test & Readiness');

        $total_post_project = $post_projects->count();
        $post_projects = $post_projects->paginate(10);
    	
    	return view('post-projects.index', compact(
    		'cab_projects',
    		'post_projects',
    		'total_cab_project',
    		'total_post_project'
		));
    }

    public function create($development_id)
    {
    	$post_project = PostProject::where('development_id', $development_id)->first();

    	return view('post-projects.create', compact('post_project'));
    }

    public function store(Request $request, $development_id)
    {
        $this->validate($request, [
            'release_test' => 'required',
            'rfs_schedule' => 'required',
            'remark_project' => 'required',
            'status_service' => 'required',
            'justification_release' => 'mimes:pdf,doc,docx',
            'justification_rfs' => 'mimes:pdf,doc,docx',
        ]);

        // dd($request->all());

        $release_test = $request->input('release_test');
        $rfs_schedule = $request->input('rfs_schedule');
        $remark_project = $request->input('remark_project');
        $status_service = $request->input('status_service');

        $post_project = PostProject::where('development_id', $development_id)->firstOrFail();

        $justification_release = '';
        if ($request->hasFile('justification_release')) {
            $justification_release = $this->uploadJustificationRelease($development_id);
        }

        $justification_rfs = '';
        if ($request->hasFile('justification_rfs')) {
            $justification_rfs = $this->uploadJustificationRfs($development_id);
        }

        $post_project->phase = 'Test & Readiness';
        $post_project->release_test = $release_test;
        $post_project->rfs_schedule = $rfs_schedule;
        $post_project->remark_project = $remark_project;
        $post_project->status_service = $status_service;
        $post_project->justification_release = $justification_release;
        $post_project->justification_rfs = $justification_rfs;
        $post_project->user_id = Auth::user()->id;
        $post_project->save();

        \Flash::success('Create Request on Test & Readiness Phase Success!');

        return redirect('/post-projects/'.$post_project->development_id);
    }

    public function show($development_id)
    {
    	$post_project = PostProject::join('declare_projects', 'post_projects.development_id', '=', 'declare_projects.development_id')
    		->join('cab_projects', 'post_projects.development_id', '=', 'cab_projects.development_id')
    		->where('post_projects.development_id', $development_id)
    		->firstOrFail();

    	return view('post-projects.details', compact('post_project'));
    }

    public function edit($development_id)
    {
        $post_project = PostProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $post_project);

        return view('post-projects.edit', compact('post_project'));
    }

    public function update(Request $request, $development_id)
    {
        $this->validate($request, [
            'release_test' => 'required',
            'rfs_schedule' => 'required',
            'remark_project' => 'required',
            'status_service' => 'required',
            'justification_release' => 'mimes:pdf,doc,docx',
            'justification_rfs' => 'mimes:pdf,doc,docx',
        ]);

        // dd($request->all());

        $release_test = $request->input('release_test');
        $rfs_schedule = $request->input('rfs_schedule');
        $remark_project = $request->input('remark_project');
        $status_service = $request->input('status_service');

        $post_project = PostProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $post_project);

        $justification_release = $post_project->justification_release;
        if ($request->hasFile('justification_release')) {
            $justification_release = $this->uploadJustificationRelease($development_id);
        }

        $justification_rfs = $post_project->justification_rfs;
        if ($request->hasFile('justification_rfs')) {
            $justification_rfs = $this->uploadJustificationRfs($development_id);
        }

        $post_project->phase = 'Test & Readiness';
        $post_project->release_test = $release_test;
        $post_project->rfs_schedule = $rfs_schedule;
        $post_project->remark_project = $remark_project;
        $post_project->status_service = $status_service;
        $post_project->justification_release = $justification_release;
        $post_project->justification_rfs = $justification_rfs;
        $post_project->user_id = Auth::user()->id;
        $post_project->save();

        \Flash::success('Update Request Success!');

        return redirect('/post-projects/'.$post_project->development_id);
    }

    /**
     * Uploads
     */

    public function uploadJustificationRelease($development_id)
    {
        $file = request()->file('justification_release');

        $fileName = $development_id. '_JUSTIFICATION_RELEASE' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    }

    public function uploadJustificationRfs($development_id)
    {
        $file = request()->file('justification_rfs');

        $fileName = $development_id. '_JUSTIFICATION_RFS' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    }
}






















