<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Models\CabProject;
use App\Models\PostProject;
use Illuminate\Http\Request;
use App\Models\DeclareProject;

class DeclareProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'create',
            'store',
            'edit',
            'update',
        ]]);
    }
    
    public function index(Request $request)
    {
        $q = $request->input('q');

        $declare_projects = DeclareProject::where('project_name', 'LIKE', '%'.$q.'%')->paginate(10);
    	
    	return view('declare-projects.index', compact('declare_projects'));
    }

    public function create()
    {
    	$declare_project = new DeclareProject;
    	return view('declare-projects.create', compact('declare_project'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'quartal' => 'required',
            'project_year' => 'required',
            'bussiness_owner' => 'required',
            'project_type' => 'required',
            'category' => 'required',

            'bau_ocs_week' => 'required_if:category,BAU',
            'bau_ocs_month' => 'required_if:category,BAU',
            'bau_ocs_year' => 'required_if:category,BAU',
            'bau_oms_week' => 'required_if:category,BAU',
            'bau_oms_month' => 'required_if:category,BAU',
            'bau_oms_year' => 'required_if:category,BAU',

            'project_name' => 'required',
            'project_brief' => 'required',
            'ocs_release' => 'required_if:category,BAU',
            'oms_release' => 'required_if:category,BAU',
            'release_target' => 'required',
            'project_document' => 'mimes:pdf,doc,docx',
            'project_justification' => 'mimes:pdf,doc,docx',
            'srs_document' => 'mimes:pdf,doc,docx',
            'justification_development' => 'mimes:pdf,doc,docx',
        ]);

        // dd($request->all());

        $quartal = $request->input('quartal');
        $project_year = $request->input('project_year');
        $bussiness_owner = $request->input('bussiness_owner');
        $project_type = $request->input('project_type');
        $category = $request->input('category');

        $bau_ocs_week = $request->input('bau_ocs_week');
        $bau_ocs_month = $request->input('bau_ocs_month');
        $bau_ocs_year = $request->input('bau_ocs_year');

        $bau_oms_week = $request->input('bau_oms_week');
        $bau_oms_month = $request->input('bau_oms_month');
        $bau_oms_year = $request->input('bau_oms_year');

        $project_name = $request->input('project_name');
        $project_brief = $request->input('project_brief');
        $revenue_projection = $request->input('revenue_projection');
        $subscriber_projection = $request->input('subscriber_projection');
        $traffic_projection = $request->input('traffic_projection');

        $ocs_release = $request->input('ocs_release');
        $oms_release = $request->input('oms_release');
        $release_target = $request->input('release_target');

        $development_id = $this->generateDevelopmentId($quartal, $project_year, $bussiness_owner, $project_type);

        $bau_ocs = $bau_ocs_year.'-'.$bau_ocs_month.'-'.$bau_ocs_week;
        $bau_oms = $bau_oms_year.'-'.$bau_oms_month.'-'.$bau_oms_week;

        $project_document = '';
        if ($request->hasFile('project_document')) {
            $project_document = $this->uploadProjectDocument($development_id);
        }

        $project_justification = '';
        if ($request->hasFile('project_justification')) {
            $project_justification = $this->uploadJustification($development_id);
        }

        $srs_document = '';
        if ($request->hasFile('srs_document')) {
            $srs_document = $this->uploadSrsDocument($development_id);
        }

        $justification_development = '';
        if ($request->hasFile('justification_development')) {
            $justification_development = $this->uploadJustificationDevelopment($development_id);
        }

        $declare_project = new DeclareProject;
        $declare_project->development_id = $development_id;
        $declare_project->project_name = $project_name;
        $declare_project->phase = 'Plan & Define';
        $declare_project->category = $category;
        $declare_project->bau_ocs = $bau_ocs;
        $declare_project->bau_oms = $bau_oms;
        $declare_project->project_brief = $project_brief;
        $declare_project->ocs_release = $ocs_release;
        $declare_project->oms_release = $oms_release;
        $declare_project->release_target = $release_target;
        $declare_project->revenue_projection = $revenue_projection;
        $declare_project->subscriber_projection = $subscriber_projection;
        $declare_project->traffic_projection = $traffic_projection;
        $declare_project->project_document = $project_document;
        $declare_project->project_justification = $project_justification;
        $declare_project->srs_document = $srs_document;
        $declare_project->justification_development = $justification_development;
        $declare_project->user_id = Auth::user()->id;
        $declare_project->save();

        $cab_project = new CabProject;
        $cab_project->development_id = $development_id;
        $cab_project->project_name = $project_name;
        $cab_project->phase = 'Plan & Define';
        $cab_project->user_id = Auth::user()->id;
        $cab_project->save();

        $post_project = new PostProject;
        $post_project->development_id = $development_id;
        $post_project->project_name = $project_name;
        $post_project->phase = 'Plan & Define';
        $post_project->user_id = Auth::user()->id;
        $post_project->save();

        \Flash::success('Create Request on Plan & Define Phase Success!');

        return redirect('/declare-projects/'.$declare_project->development_id);
    }

    public function show($development_id)
    {
    	$declare_project = DeclareProject::where('development_id', $development_id)->firstOrFail();

    	return view('declare-projects.details', compact('declare_project'));
    }

    public function edit($development_id)
    {
        $declare_project = DeclareProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $declare_project);

        return view('declare-projects.edit', compact('declare_project'));
    }

    public function update(Request $request, $development_id)
    {
        $this->validate($request, [

            'category' => 'required',

            'bau_ocs_week' => 'required_if:category,BAU',
            'bau_ocs_month' => 'required_if:category,BAU',
            'bau_ocs_year' => 'required_if:category,BAU',
            'bau_oms_week' => 'required_if:category,BAU',
            'bau_oms_month' => 'required_if:category,BAU',
            'bau_oms_year' => 'required_if:category,BAU',

            'project_name' => 'required',
            'project_brief' => 'required',
            'ocs_release' => 'required',
            'oms_release' => 'required',
            'release_target' => 'required',
            'project_document' => 'mimes:pdf,doc,docx',
            'project_justification' => 'mimes:pdf,doc,docx',
            'srs_document' => 'mimes:pdf,doc,docx',
            'justification_development' => 'mimes:pdf,doc,docx',
        ]);

        // dd($request->all());

        $category = $request->input('category');

        $bau_ocs_week = $request->input('bau_ocs_week');
        $bau_ocs_month = $request->input('bau_ocs_month');
        $bau_ocs_year = $request->input('bau_ocs_year');

        $bau_oms_week = $request->input('bau_oms_week');
        $bau_oms_month = $request->input('bau_oms_month');
        $bau_oms_year = $request->input('bau_oms_year');

        $project_name = $request->input('project_name');
        $project_brief = $request->input('project_brief');
        $revenue_projection = $request->input('revenue_projection');
        $subscriber_projection = $request->input('subscriber_projection');
        $traffic_projection = $request->input('traffic_projection');

        $ocs_release = $request->input('ocs_release');
        $oms_release = $request->input('oms_release');
        $release_target = $request->input('release_target');

        $declare_project = DeclareProject::where('development_id', $development_id)->firstOrFail();

        $this->authorize('update', $declare_project);

        if ($category == 'BAU') {
            $bau_ocs = $bau_ocs_year.'-'.$bau_ocs_month.'-'.$bau_ocs_week;
            $bau_oms = $bau_oms_year.'-'.$bau_oms_month.'-'.$bau_oms_week;
        } else if ($category == 'PROJECT') {
            $bau_ocs = '';
            $bau_oms = '';
        } else {
            $bau_ocs = $declare_project->bau_ocs;
            $bau_oms = $declare_project->bau_oms;
        }

        $development_id = $declare_project->development_id;

        $project_document = $declare_project->project_document;
        if ($request->hasFile('project_document')) {
            $project_document = $this->uploadProjectDocument($development_id);
        }

        $project_justification = $declare_project->project_justification;
        if ($request->hasFile('project_justification')) {
            $project_justification = $this->uploadJustification($development_id);
        }

        $srs_document = $declare_project->srs_document;
        if ($request->hasFile('srs_document')) {
            $srs_document = $this->uploadSrsDocument($development_id);
        }

        $justification_development = $declare_project->justification_development;
        if ($request->hasFile('justification_development')) {
            $justification_development = $this->uploadJustificationDevelopment($development_id);
        }

        $declare_project->project_name = $project_name;
        $declare_project->category = $category;
        $declare_project->bau_ocs = $bau_ocs;
        $declare_project->bau_oms = $bau_oms;
        $declare_project->project_brief = $project_brief;
        $declare_project->ocs_release = $ocs_release;
        $declare_project->oms_release = $oms_release;
        $declare_project->release_target = $release_target;
        $declare_project->revenue_projection = $revenue_projection;
        $declare_project->subscriber_projection = $subscriber_projection;
        $declare_project->traffic_projection = $traffic_projection;
        $declare_project->project_document = $project_document;
        $declare_project->project_justification = $project_justification;
        $declare_project->srs_document = $srs_document;
        $declare_project->justification_development = $justification_development;
        $declare_project->user_id = Auth::user()->id;
        $declare_project->save();

        \Flash::success('Update Request on Plan & Define Phase Success!');

        return redirect('/declare-projects/'.$declare_project->development_id);
    }


    /**
     * generateDevelopmentId
     * returned format (QUARTAL)-(YYYY)-(BUSSINESS-OWNER)-(PLAN-UNPLAN)-(NOMOR-URUT)
     */
    public function generateDevelopmentId($quartal, $year, $owner, $type_project)
    {
        $declare_project = DeclareProject::orderBy('id', 'desc')->first();

        if (! $declare_project) {

            $last_id = 1;

            $last_id = sprintf("%03s", $last_id);

            $dev_id = $quartal."-".$year."-".$owner."-".$type_project."-".$last_id;

        } else {

            $last_id = (int) substr($declare_project->development_id, -3);
            $last_id = $last_id + 1;

            $last_id = sprintf("%03s", $last_id);

            $dev_id = $quartal."-".$year."-".$owner."-".$type_project."-".$last_id;    
        }

        return $dev_id;
    }

    /**
     * Uploads
     */
    
    public function uploadProjectDocument($development_id)
    {
        $file = request()->file('project_document');

        $fileName = $development_id. '_PROJECT_DOCUMENT' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    }
    
    public function uploadJustification($development_id)
    {
        $file = request()->file('project_justification');

        $fileName = $development_id. '_PROJECT_JUSTIFICATION' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    }

    public function uploadSrsDocument($development_id)
    {
        $file = request()->file('srs_document');

        $fileName = $development_id. '_SRS_DOCUMENT' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    }

    public function uploadJustificationDevelopment($development_id)
    {
        $file = request()->file('justification_development');

        $fileName = $development_id. '_JUSTIFICATION_DEVELOPMENT' . '.' . $file->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'documents';
        $file->move($destinationPath, $fileName);

        return $fileName;
    } 

}
