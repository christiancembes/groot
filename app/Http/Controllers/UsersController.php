<?php

namespace App\Http\Controllers;

use Flash;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;

class UsersController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
    	$q = $request->get('q');

    	$users = User::with('roles')->where('name', 'LIKE', '%'.$q.'%')->paginate(10);

        return view('users.index', compact('users', 'q'));
    }

    public function create()
    {
    	$user = new User;

    	return view('users.create', compact('user'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
	    'username' => 'required',
            'is_verified' => 'required',
            'is_active' => 'required',
            'avatar' => 'mimes:jpeg,png,jpg',
        ]);

        $avatar = '';
        if ($request->hasFile('avatar')) {
            $avatar = $this->uploadAvatar();
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt('dummy');
	$user->username = $request->input('username');
        $user->is_verified = $request->input('is_verified');
        $user->is_active = $request->input('is_active');
        $user->avatar = $avatar;
        $user->save();

        $user->syncRoles();

        \Flash::success('Create new user success');

        return redirect()->route('users.index');
    }

    public function show($id)
    {
    	$user = User::with(['roles'])->where('id', $id)->first();

        if (! $user) {
        	Flash::error('User not found!');
        	return redirect()->back();
        }

        return view('users.details', compact('user'));
    }

    public function edit($id)
    {
    	$user = User::with(['roles'])->where('id', $id)->first();

        if (! $user) {
        	Flash::error('User not found!');
        	return redirect()->back();
        }

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email,'. $id,
	    'username'=> 'required',
            'is_verified' => 'required',
            'is_active' => 'required',
            'avatar' => 'mimes:jpeg,png,jpg',
        ]);

    	$user = User::where('id', $id)->first();

    	if (! $user) {
        	Flash::error('User not found!');
        	return redirect()->back();
        }

        $avatar = $user->avatar;
        if ($request->hasFile('avatar')) {
            $avatar = $this->uploadAvatar();
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->is_verified = $request->input('is_verified');
	$user->username = $request->input('username');
        $user->is_active = $request->input('is_active');
        $user->avatar = $avatar;
        $user->save();

        $user->syncRoles();

        \Flash::success('Update new user success');

        return redirect('/users/'.$user->id);
    }

    public function toogleStatus(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('status');

        $user = User::where('id', $id)->first();

        if (! $user) {
        	Flash::error('User not found!');
        	return redirect()->back();
        }

        $user->is_active = $status;
        $user->save();

        if ($status == 1) {
            $message = 'User successfully activated.';
        } elseif ($status == 0) {
            $message = 'User successfully deactivated.';
        }

        Flash::success($message);

        return redirect()->back();
    }

    public function uploadAvatar()
    {
    	$img = request()->file('avatar');

        $fileName = str_random(40) . '.' . $img->guessClientExtension();
        $destinationPath = public_path() . '/img/avatars/';
        $img->move($destinationPath, $fileName);

        return env('APP_URL').'/img/avatars/'.$fileName;
    }
}
