<?php
namespace App\Http\Controllers;

use Flash;
use App\Http\Requests;
use Illuminate\Http\Request;
use GuzzleHttp\Client;


class CheckProfileController extends Controller
{
    public function index(Request $request)
    {
        return view('check-profile.index');
    }
    public function search(Request $request)
    {

	// validasi field keyword
        $this->validate($request, [
            'search_by' => 'required',
            'keyword' => 'required',
        ]);

        $msisdn = $request->input('keyword');
        $search_by = $request->input('search_by');

	if($search_by == 'uxp_check'){
	try {
            $client = new Client(['headers' => ["Content-Type" => "application/json", "accept"=> "application/json"]]);
            $response = $client->request('GET', 'http://10.2.224.248:10021/subscriber/activebonuses/' . $msisdn);
        } catch (GuzzleHttp\Exception\RequestException $e) {
            echo "HANDLE ERROR IN HERE";
            dd($e->getResponse()->getBody());
        }
     
	$data1 = json_decode($response->getBody()->getContents());
        $dataFromAPI1 = $data1->allowance;

        return view('check-profile.search_checkprofile', [
            'search_by' => $search_by,
            'dataFromAPI1' => $dataFromAPI1
        ]);

     }	
	
	elseif($search_by == 'dsp_check'){
	try {
            $client = new Client(['headers' => ["Content-Type" => "application/json", "accept"=> "application/json"]]);
            $response = $client->request('GET', 'http://10.2.248.25:8001/?oprid=cardmap&show=json&msisdn=' . $msisdn);
        } catch (GuzzleHttp\Exception\RequestException $e) {
            echo "HANDLE ERROR IN HERE";
            dd($e->getResponse()->getBody());
        }

        $data2 = json_decode($response->getBody()->getContents());
        $dataFromAPI2 = $data2;

        return view('check-profile.search_checkprofiledsp', [
            'search_by' => $search_by,
            'dataFromAPI2' => $dataFromAPI2
        ]);

     }
	
           elseif($search_by == 'cm_check'){
        try {
            $client = new Client(['headers' => ["Content-Type" => "application/json", "accept"=> "application/json"]]);
            $response = $client->request('GET', 'http://10.250.200.185:9090/checkapi/check.jsp?msisdn=' . $msisdn);
        } catch (GuzzleHttp\Exception\RequestException $e) {
            echo "HANDLE ERROR IN HERE";
            dd($e->getResponse()->getBody());
        }

        $data3 = json_decode($response->getBody()->getContents());
        $dataFromAPI3 = $data3;

        return view('check-profile.search_checkprofilecm', [
            'search_by' => $search_by,
            'dataFromAPI3' => $dataFromAPI3
        ]);

     }

   }
}
