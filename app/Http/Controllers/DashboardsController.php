<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Http\Requests;
use App\Models\CabProject;
use App\Models\OfferReport;
use App\Models\PostProject;
use Illuminate\Http\Request;
use App\Models\PricePlanReport;
use App\Models\CustomerBaseReport;
use App\Models\DeclareProject;

class DashboardsController extends Controller
{
    public function index()
    {
    	$customer_bases = $this->getCustomerBaseReports();
    	$offer 			= $this->getOfferReports();
    	$priceplan 		= $this->getPriceplanReports();

        $total_declare  = $this->getTotalProjectByPhase('declare');
        $total_cab      = $this->getTotalProjectByPhase('cab');
        $total_post     = $this->getTotalProjectByPhase('post');

        $status_projects = $this->getStatusProjects();
        $status_services = $this->getStatusServices();
        $project_types   = $this->getProjectTypes();
	$request_types 	 = $this->getRequestTypes();

        // dd($status_projects->toArray());
        // dd($status_services->toArray());
        // dd($project_types->toArray());

    	return view('dashboards.index', compact(
    		'customer_bases',
    		'offer',
            'priceplan',
            'total_declare',
            'total_cab',
    		'total_post',
            'status_projects',
            'status_services',
            'project_types',
	    'request_types'
		));
    }

    public function getCustomerBaseReports()
    {
    	$customer_bases = CustomerBaseReport::orderBy('id', 'desc')
    		->limit(6)
    		->get();

    	return $customer_bases->reverse();
    }

    public function getOfferReports()
    {
	return OfferReport::orderBy('id', 'desc')->first();
    }

    public function getPriceplanReports($value='')
    {
    	return PricePlanReport::orderBy('id', 'desc')->first();
    }

    private function getTotalProjectByPhase($phase)
    {
        $year = request()->input('year', 'all');

        $count = 0;

        if ($phase == 'declare') {

           if ($year == 'all') {
               $count = PostProject::count();
           } else {
               $count = PostProject::where('development_id', 'LIKE', '%-'.$year.'-%')->count();
           } 


        }  else if ($phase == 'cab') {

           if ($year == 'all') {
               $count = PostProject::where(function($query) {
                       $query->where('phase', 'cab');
                       $query->orWhere('phase', 'post');
                   })
                   ->count();
           } else {
               $count = PostProject::where('development_id', 'LIKE', '%-'.$year.'-%')
                   ->where(function($query) {
                       $query->where('phase', 'cab');
                       $query->orWhere('phase', 'post');
                   })
                   ->count();
           }


       } else if ($phase == 'post') {

         if ($year == 'all') {
               $count = PostProject::where(function($query) {
                      $query->where('phase', 'post');
                      //$query->orWhere('phase', 'cab');
                   })
                   ->count();
        } else {
               $count = PostProject::where('development_id', 'LIKE', '%-'.$year.'-%')
                   ->where(function($query) {
                       $query->where('phase', 'post');
                       //$query->orWhere('phase', 'cab');
                   })
                   ->count();
           }
         

       }

        return (int) $count;
    }

    public function getStatusProjects()
    {
        //$year = request()->input('year', date('Y'));
          $year = request()->input('year', 'all');

    if ($year == 'all') {

        $data = CabProject::select([
                'status_project',
                DB::raw('count(id) as total')
            ])
            ->whereNotNull('status_project')
            ->groupBy('status_project')
            ->get();

    } else {
        $data = CabProject::select([
            'status_project',
                DB::raw('count(id) as total')
            ])
            ->where('development_id', 'LIKE', '%-'.$year.'-%')
            ->whereNotNull('status_project')
            ->groupBy('status_project')
            ->get(); 

    } // yang sebelum nya else nya lupa ditutup di bagian ini

    return $data;
}

    public function getStatusServices()
    {
        //$year = request()->input('year', date('Y'));
          $year = request()->input('year', 'all');

      if ($year == 'all') {

          $data = PostProject::select([
                'status_service',
                DB::raw('count(id) as total')
            ])
            //->where('development_id', 'LIKE', '%-'.$year.'-%')
            ->whereNotNull('status_service')
            ->groupBy('status_service')
            ->get();
      
      } else {
        $data = PostProject::select([
                'status_service',
                DB::raw('count(id) as total')
            ])
            ->where('development_id', 'LIKE', '%-'.$year.'-%')
            ->whereNotNull('status_service')
            ->groupBy('status_service')
            ->get();
      }
        
        return $data;
  
  }

    public function getProjectTypes()
    {
        //$year = request()->input('year', date('Y'));
        $year = request()->input('year', 'all');

      if ($year == 'all') {

        $plan = CabProject::select([
                DB::raw("'Plan' as project_type"),
                DB::raw("count(id) as total")
            ]);
            //->where('development_id', 'LIKE', '%-'.$year.'-%-P-%');

        $data = CabProject::select([
                DB::raw("'Unplanned' as project_type"),
                DB::raw("count(id) as total")
            ])
            //->where('development_id', 'LIKE', '%-'.$year.'-%-U-%')
            ->union($plan)
            ->get();

    
      } else {
      
        $plan = CabProject::select([
                DB::raw("'Plan' as project_type"),
                DB::raw("count(id) as total")
            ])
            ->where('development_id', 'LIKE', '%-'.$year.'-%-P-%');

        $data = CabProject::select([
                DB::raw("'Unplanned' as project_type"),
                DB::raw("count(id) as total")
            ])
            ->where('development_id', 'LIKE', '%-'.$year.'-%-U-%')
            ->union($plan)
            ->get();     
        }

    return $data;
    
    } 


    public function getRequestTypes()
    {
        //$year = request()->input('year', date('Y'));
          $year = request()->input('year', 'all');

    if ($year == 'all') {

        $data = DeclareProject::select([
                'category',
                DB::raw('count(id) as total')
            ])
            ->whereNotNull('category')
            ->groupBy('category')
            ->get();

    } else {
        $data = DeclareProject::select([
            'category',
                DB::raw('count(id) as total')
            ])
            ->where('development_id', 'LIKE', '%-'.$year.'-%')
            ->whereNotNull('category')
            ->groupBy('category')
            ->get();

    } // yang sebelum nya else nya lupa ditutup di bagian ini

    return $data;
  }
}	
