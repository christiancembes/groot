<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class TestingController extends Controller
{
    public function index()
    {
        # nanti msisdn nya diambil dari imputan user, sekarang hardcode dulu
        $msisdn = '628118118863';

        $client = new Client(['headers' => ["Content-Type" => "application/json", "accept"=> "application/json"]]);
	$response = $client->request('GET', 'http://10.2.224.248:10021/subscriber/activebonuses/' . $msisdn);

        dd($response->getBody()->getContents());
    }
}
