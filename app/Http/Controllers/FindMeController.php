<?php

namespace App\Http\Controllers;

use Flash;
use App\Http\Requests;
use App\Models\Keyword;
use App\Models\OcsBonus;
use App\Models\OcsAllowance;
use App\Models\OcsOffer;
use App\Models\OmsOffer;
use App\Models\Mirroring;
use App\Models\OcsCharge;
use App\Models\Restriction;
use App\Models\Prerequisite;
use Illuminate\Http\Request;
use App\Models\DeclareProject;
use App\Models\ProjectOcsOffer;
use App\Models\UpccQuota;
use App\Models\UpccService;
use App\Models\UpccNotif;
use App\Models\OfferUpcc;
use App\Models\UserLogin;

class FindMeController extends Controller
{
    public function index(Request $request)
    {
    	return view('find-me.index');
    }

    public function search(Request $request)
    {
    	$this->validateSearch($request);

    	$search_by = $request->input('search_by');
    	$keyword = $request->input('keyword');
    	$from = $request->input('from');
    	$to = $request->input('to');

    	if ($search_by == 'development_id' OR $search_by == 'project_name') {

    		$field = 'declare_projects.'.$search_by;

    		$projects = $this->getProjectByDevelopmentIdOrProjectName($field, $keyword);
    	}

    	if ($search_by == 'ocs_release' OR $search_by == 'oms_release') {

    		$field = 'declare_projects.'.$search_by;

    		$projects = $this->getProjectByOcsReleaseOrOmsRelease($field, $from, $to);
    	}

        if ($search_by == 'offer_id' OR $search_by == 'offer_name' OR $search_by == 'group_product' OR $search_by == 'description' OR $search_by == 'subtype1' OR $search_by == 'type1') {

            $field = 'ocs_offers.'.$search_by;

            // $offer = OcsOffer::where($field, $keyword)->first();
            $ocs_offers = OcsOffer::where($field, 'LIKE', '%'.$keyword.'%')
				    ->orderBy('offer_id')
				    ->get();
	    //$ocs_offers1 = OcsOffer::where('owner', 'LIKE', '%'.$keyword.'%')->get();
	    //$ocs_offers2 = OcsOffer::where('subtype1', 'LIKE', '%'.$keyword.'%')->get();

            if (! $ocs_offers) {
                Flash::error("Cant find {$search_by} : {$keyword}");
                return redirect()->back();
            }

		return view('find-me.search', compact(
               
		'ocs_offers',
                //'upcc',
                //'upccquota',
                //'upccservice',
                //'upccnotif',
                // 'ocs_bonus',
                // 'restrictions',
                // 'prerequisites',
                // 'keywords',
                // 'mirroring',
                'search_by',
                'keyword',
                'from',
                'to'
            ));
	}	
	
	if ($search_by == 'upcc') {
	    $field = 'offer_upcc.'.$search_by;
            $upcc = OfferUpcc::where($field, 'LIKE', '%'.$keyword.'%')
                             ->orderBy('upcc')
                             ->get();                  
          if (! $upcc) {
                Flash::error("Cant Find {$search_by} : {$keyword}");
                return redirect()->back();    
          
      		}   
      	    $upccquota = $this->getUpccQuota($keyword);
            $upccservice = $this->getUpccService($keyword);
            $upccnotif = $this->getUpccNotif($keyword); 
      }

	if ($search_by == 'login') {
            $field = 'user_login.'.$search_by;
            $login = UserLogin::where($field, 'LIKE', '%'.$keyword.'%')
                             ->orderBy('login')
                             ->get();
          if (! $login) {
                Flash::error("Cant Find {$search_by} : {$keyword}");
                return redirect()->back();

                }
      }

		return view('find-me.search', compact(
                'projects',
		'upcc',
                'upccquota',
                'upccservice',
                'upccnotif',
                'search_by',
		'login'
            ));
}
    public function validateSearch($request)
    {
    	$search_by = $request->input('search_by');

    	$rules = [
    		'search_by' => 'required'
    	];

    	if ($search_by == 'development_id'
    		OR $search_by == 'project_name'
    		OR $search_by == 'offer_id'
    		OR $search_by == 'offer_name'
		OR $search_by == 'group_product'
		OR $search_by == 'type1'
		OR $search_by == 'subtype1'
		OR $search_by == 'description'
		OR $search_by == 'upcc'
		OR $search_by == 'login'
		) {
    		$rules = array_merge($rules, ['keyword' => 'required']);
		}

		if ($search_by == 'oms_release' OR $search_by == 'ocs_release') {
    		$rules = array_merge($rules, [
    			'from' => 'required',
    			'to' => 'required',
			]);
		}

    	$this->validate($request, $rules);	
    }

    private function getProjectByDevelopmentIdOrProjectName($field, $keyword)
    {
    	return DeclareProject::join(
    			'post_projects',
			'declare_projects.development_id', '=', 'post_projects.development_id'
			//'cab_projects.development_id', '=', 'post_projects.development_id'	
			)
			->where($field, 'LIKE', '%'.$keyword.'%')
			->select([
				'declare_projects.development_id',
				'declare_projects.project_name',
				'post_projects.phase',
				'post_projects.status_service',
				//'cab_projects.status_project',
			])
			->get();
    }

    
    private function getProjectByOfferIdOrOfferName($field, $keyword)
    {
    	return ProjectOcsOffer::leftJoin(
    			'post_projects',
    			'project_ocs_offers.development_id', '=', 'post_projects.development_id'
			)->leftJoin(
				'ocs_offers',
				'project_ocs_offers.offer_id', '=', 'ocs_offers.offer_id'
			)
    		->where($field, $keyword)
    		->select([
				'project_ocs_offers.development_id',
				'post_projects.project_name',
				'post_projects.phase',
				'ocs_offers.offer_id',
				'ocs_offers.offer_name',
			])
    		->groupBy('project_ocs_offers.development_id')
    		->get();
    }

    private function getProjectByOcsReleaseOrOmsRelease($field, $from, $to)
    {
    	return DeclareProject::join(
    			'post_projects',
    			'declare_projects.development_id', '=', 'post_projects.development_id'
			)
			->where($field, '>=', $from)
			->where($field, '<=', $to)
			->select([
				'declare_projects.development_id',
				'declare_projects.project_name',
				'post_projects.phase',
				'post_projects.status_service',
			])
			->get();
    }

    private function getAllProjects($offer_id)
    {
        return ProjectOcsOffer::join(
                'post_projects',
                'project_ocs_offers.development_id', '=', 'post_projects.development_id'
            )
            ->where('offer_id', $offer_id)
            ->groupBy('project_ocs_offers.development_id')
            ->get();
    }

    private function getOcsOffers($offer_id)
    {
        return OcsOffer::where('offer_id', $offer_id)->first();
    }

    private function getOmsOffers($offer_id)
    {
        return OmsOffer::where('offer_id', $offer_id)->first();
    }

    private function getOcsCharges($offer_id)
    {
        return OcsCharge::where('offer_id', $offer_id)->first();
    }

    private function getOcsBonus($offer_id)
    {
        return OcsBonus::where('offer_id', $offer_id)->first();
    }

    private function getOcsAllowance($offer_id)
    {
	return OcsAllowance::where('offer_id',$offer_id)->first();
    }

    private function getRestriction($offer_id)
    {
        return Restriction::where('offer_id', $offer_id)->get();
    }

    private function getPrerequisites($offer_id)
    {
        return Prerequisite::where('offer_id', $offer_id)->get();
    }

    private function getKeywords($offer_id)
    {
        return Keyword::where('offer_id', $offer_id)->get();
    }

    private function getMirroring($offer_id)
    {
        return Mirroring::where('offer_id', $offer_id)->first();
    }
    
    //private function getUpcc($upcc)
    //{
    //	return OfferUpcc::where('upcc', $upcc)->first();
    //}

    private function getUpccQuota($upcc)
    {
        return UpccQuota::where('upcc', $upcc)->get();
    }

    private function getUpccService($upcc)
    {
        return UpccService::where('upcc', $upcc)->get();
    }

    private function getUpccNotif($upcc)
    {
        return UpccNotif::where('upcc', $upcc)->get();
    }
		
    //private function getLogin($login)
    //{
    //    return UserLogin::where('login_name', $login)->get();
    //}
}
