<?php

namespace App\Http\Controllers;

use Flash;
use App\Http\Requests;
use App\Models\Keyword;
use App\Models\OcsBonus;
use App\Models\OcsOffer;
use App\Models\OcsAllowance;
use App\Models\OcsCharge;
use App\Models\OmsOffer;
use App\Models\Mirroring;
use App\Models\Restriction;
use App\Models\Prerequisite;
use App\Models\PricePlan;
use App\Models\Dependent;
use Illuminate\Http\Request;
use App\Models\ProjectOcsOffer;
use App\Models\Seamless;
use App\Models\SeamlessGroup;
use App\Models\Solution;
use App\Models\MappingProduct;

class OffersController extends Controller
{
    public function index($offer_id)
    {
        $projects = OcsOffer::where('offer_id', $offer_id)->first();

        if (! $projects) {
            Flash::error("Offer ID not found.");
            return redirect()->back();
        }

    	$ocs_offer = $this->getOcsOffers($offer_id);
    	$oms_offer = $this->getOmsOffers($offer_id);
    	$all_project = $this->getAllProjects($offer_id);

    	$ocs_charge = $this->getOcsCharges($offer_id);
    	$ocs_bonus = $this->getOcsBonus($offer_id);
	$ocs_allowance = $this->getOcsAllowance($offer_id);

    	$restrictions = $this->getRestriction($offer_id);
    	$prerequisites = $this->getPrerequisites($offer_id);
    	$keywords = $this->getKeywords($offer_id);
    	$mirroring = $this->getMirroring($offer_id);
        $priceplan = $this->getPricePlan($offer_id);
        $dependent = $this->getDependent($offer_id);
	$seamless = $this->getSeamless($offer_id);
	$seamlessgroup = $this->getSeamlessGroup($offer_id);
	$solution = $this->getSolution($offer_id);
	$mapping_product = $this->getMappingProduct($offer_id);
	return view('offers.index', compact(
    		'ocs_offer',
    		'oms_offer',
    		'all_project',
    		'ocs_charge',
    		'ocs_bonus',
		'ocs_allowance',
    		'restrictions',
    		'prerequisites',
                'priceplan',
    		'keywords',
                'dependent',
    		'mirroring',
		'seamless',
		'seamlessgroup',
		'solution',
		'mapping_product'
		));
    }

    private function getAllProjects($offer_id)
    {
    	return ProjectOcsOffer::join(
    			'post_projects',
    			'project_ocs_offers.development_id', '=', 'post_projects.development_id'
			)
    		->where('offer_id', $offer_id)
    		->groupBy('project_ocs_offers.development_id')
    		->get();
    }

    private function getOcsOffers($offer_id)
    {
    	return OcsOffer::where('offer_id', $offer_id)->first();
    }

    private function getOmsOffers($offer_id)
    {
    	return OmsOffer::where('offer_id', $offer_id)->first();
    }

    private function getOcsCharges($offer_id)
    {
    	return OcsCharge::where('offer_id', $offer_id)->get();
    }

    private function getOcsBonus($offer_id)
    {
    	return OcsBonus::where('offer_id', $offer_id)->get();
    }

    private function getOcsAllowance($offer_id)
    {
        return OcsAllowance::where('offer_id', $offer_id)->get();
    }

    private function getRestriction($offer_id)
    {
    	return Restriction::where('offer_id', $offer_id)->get();
    }

    private function getPrerequisites($offer_id)
    {
    	return Prerequisite::where('offer_id', $offer_id)->get();
    }

    private function getPricePlan($offer_id)
    {
        return PricePlan::where('offer_id', $offer_id)->get();
    }

    private function getDependent($offer_id)
    {
        return Dependent::where('offer_id', $offer_id)->get();
    }

    private function getSeamless($offer_id)
    {
	return Seamless::where('offer_id', $offer_id)->get();
    }   

    private function getSeamlessGroup($offer_id)
    {
        return SeamlessGroup::where('offer_id', $offer_id)->get();
    }

       private function getSolution($offer_id)
    {
        return Solution::where('offer_id', $offer_id)->first();
    }

     private function getMappingProduct($offer_id)
    {
        return MappingProduct::where('offer_id', $offer_id)->first();
    }

     private function getKeywords($offer_id)
    {
    	return Keyword::where('offer_id', $offer_id)->get();
    }

    private function getMirroring($offer_id)
    {
    	return Mirroring::where('offer_id', $offer_id)->first();
    }
    
    public function compare(Request $request){
        $array = array(
            'ocs' => '',
            'oms' => '',
            'offer1' => '',
            'offer2' => '',
            'offer3' => ''
        );
        return view('offers.compare')->with($array);        
    }

    public function show(Request $request){
        $ocs_billing1 = $request->has('offer1')? OcsOffer::where('offer_id', $request->input('offer1'))->first() : '';
        $ocs_billing2 = $request->has('offer2')? OcsOffer::where('offer_id', $request->input('offer2'))->first() : '';
        $ocs_billing3 = $request->has('offer3')? OcsOffer::where('offer_id', $request->input('offer3'))->first() : '';
        $oms_billing1 = $request->has('offer1')? OmsOffer::where('offer_id', $request->input('offer1'))->first() : '';
        $oms_billing2 = $request->has('offer2')? OmsOffer::where('offer_id', $request->input('offer2'))->first() : '';
        $oms_billing3 = $request->has('offer3')? OmsOffer::where('offer_id', $request->input('offer3'))->first() : '';
        $array = array(
            'ocs' => [$ocs_billing1, $ocs_billing2, $ocs_billing3 ],
            'oms' => [$oms_billing1, $oms_billing2, $oms_billing3 ],
            'offer1' => $request->input('offer1'),
            'offer2' => $request->input('offer2'),
            'offer3' => $request->input('offer3')
            
        );
        return view('offers.compare')->with($array);
    }
    
    public function search(Request $request){
        $keyword = $request->input('key');
        $ocs_offers = OcsOffer::where('offer_name', 'LIKE', '%'.$keyword.'%')
                ->select('offer_name', 'offer_id')
                ->limit(4)->get();
        if (! $ocs_offers) {
            $respons = 'not-found';            
        }else{
            $respons = $ocs_offers;
        }
        return \Response::json($respons);   
    }

}
