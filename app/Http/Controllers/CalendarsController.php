<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\DeclareProject;

class CalendarsController extends Controller
{
    public function index()
    {
    	$release_dates = $this->getAllReleaseDates();

    	return view('calendars.index', compact('release_dates'));
    }

    public function getAllReleaseDates()
    {
    	$ocs = DeclareProject::join('post_projects', 'declare_projects.development_id', '=', 'post_projects.development_id')
    		->select([
    			'post_projects.project_name',
    			'post_projects.phase',
    			'post_projects.development_id',
    			DB::raw(" declare_projects.ocs_release as release_date"),
    			DB::raw("'OCS' as type")
			])
    		->where('post_projects.phase', 'post');

    	$oms = DeclareProject::join('post_projects', 'declare_projects.development_id', '=', 'post_projects.development_id')
    		->select([
    			'post_projects.project_name',
    			'post_projects.phase',
    			'post_projects.development_id',
    			DB::raw(" declare_projects.oms_release as release_date"),
    			DB::raw("'OMS' as type")
			])
    		->where('post_projects.phase', 'post');

    	$all_data = DeclareProject::join('post_projects', 'declare_projects.development_id', '=', 'post_projects.development_id')
    		->select([
    			'post_projects.project_name',
    			'post_projects.phase',
    			'post_projects.development_id',
    			DB::raw(" declare_projects.release_target as release_date"),
    			DB::raw("'RFC' as type")
			])
    		->where('post_projects.phase', 'post')
    		->union($ocs)
    		->union($oms)
    		->get();

		// dd($all_data->toArray());
    	
    	if (!$all_data) {
    		return [];
    	}

    	$release_dates = [];

    	foreach ($all_data as $data) {

    		if ($data->type == "OCS")
            {
                $bgColor = "#00c0ef"; // aqua
                $borderColor = "#00c0ef"; // aqua
            }
            elseif ($data->type == "OMS")
            {
                $bgColor = "#0073b7"; // blue
                $borderColor = "#0073b7"; // blue
            }
            elseif ($data->type == "RFC")
            {
                $bgColor = "#00a65a"; // green
                $borderColor = "#00a65a"; // green
            }

            $release['title'] 			= "[$data->type] " .$data->project_name;
            $release['start'] 			= $data->release_date;
            $release['backgroundColor'] = $bgColor;
            $release['borderColor']     = $borderColor;
            $release['url']				= url("/".strtolower($data->phase)."-projects/".$data->development_id);

            array_push($release_dates, $release);
    	}

    	// dd($release_dates);

    	return json_encode($release_dates);
    }
}
