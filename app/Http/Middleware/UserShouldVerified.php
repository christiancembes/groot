<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class UserShouldVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check() && !Auth::user()->is_verified) {

            $link = url('auth/send-verification').'?email='.urlencode(Auth::user()->email);

            Auth::logout();

            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "We already sent you an email, please follow the instruction to activate your account. <a class='alert-link' href='$link'>Resend</a>."
            ]);

            return redirect('/login');
        }

        return $response;
    }
}














