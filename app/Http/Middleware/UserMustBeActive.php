<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class UserMustBeActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check() && !Auth::user()->is_active) {

            Auth::logout();

            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Akun anda sedang di nonaktifkan, silahkan hubungi administrator untuk mengaktifkan kembali account anda."
            ]);

            return redirect('/login');
        }

        return $response;
    }
}














