<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

	Route::auth();
	Route::get('auth/verify/{token}', 'Auth\AuthController@verify');
	Route::get('auth/send-verification', 'Auth\AuthController@sendVerification');

	Route::get('/', 'DashboardsController@index');
	Route::get('/dashboards', 'DashboardsController@index');

	Route::get('/calendars', 'CalendarsController@index');

	Route::get('/find-me', 'FindMeController@index');

	Route::post('users/toogle-status', 'UsersController@toogleStatus');
	Route::resource('users', 'UsersController');

	Route::get('/declare-projects', 'DeclareProjectsController@index');
	Route::get('/declare-projects/create', 'DeclareProjectsController@create');
	Route::post('/declare-projects', 'DeclareProjectsController@store');
	Route::get('/declare-projects/{id}', 'DeclareProjectsController@show');
	Route::get('/declare-projects/{id}/edit', 'DeclareProjectsController@edit');
	Route::patch('/declare-projects/{id}', 'DeclareProjectsController@update');

	Route::get('/cab-projects', 'CabProjectsController@index');
	Route::get('/cab-projects/{development_id}/create', 'CabProjectsController@create');
	Route::post('/cab-projects/{development_id}', 'CabProjectsController@store');
	Route::get('/cab-projects/{development_id}', 'CabProjectsController@show');
	Route::get('/cab-projects/{development_id}/edit', 'CabProjectsController@edit');
	Route::patch('/cab-projects/{development_id}', 'CabProjectsController@update');

	Route::get('/post-projects', 'PostProjectsController@index');
	Route::get('/post-projects/{development_id}/create', 'PostProjectsController@create');
	Route::post('/post-projects/{development_id}', 'PostProjectsController@store');
	Route::get('/post-projects/{development_id}', 'PostProjectsController@show');
	Route::get('/post-projects/{development_id}/edit', 'PostProjectsController@edit');
	Route::patch('/post-projects/{development_id}', 'PostProjectsController@update');

	//Updated on 2016-12-26 by hsbakbr
	Route::get('/activity', 'ActivityController@index');
	Route::get('/activity/{development_id}', 'ActivityController@monitoring');
        Route::get('/activity/{development_id}/edit', 'ActivityController@edit');
        Route::get('/activity/{development_id}/delete', 'ActivityController@delete');
        Route::post('/activity/process' , 'ActivityController@process');
        Route::post('/activity/delete' , 'ActivityController@delete');

        Route::get('/offers/{offer_id}', 'OffersController@index');
        Route::get('/compare/', 'OffersController@compare');
        Route::post('/compare/', 'OffersController@show');
        Route::post('/compare/search', 'OffersController@search');
    
        Route::get('/find-me', 'FindMeController@index');
        Route::post('/find-me', 'FindMeController@search');

	//testing API
	Route::get('/check-profile', 'TestingController@index');
	Route::get('/testing2', 'TestingControler2@index');	
	Route::get('/check-profile', 'CheckProfileController@index');
	Route::post('/check-profile', 'CheckProfileController@search');


});

