<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OcsOffer extends Model
{
    protected $table = 'ocs_offers';

    protected $fillable = [
    	'offer_id',
    	'offer_name',
    	'type1',
    	'subtype1',
    ];

}
