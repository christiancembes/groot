<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeclareProject extends Model
{
    protected $table = 'declare_projects';

    protected $fillable = [
    	'development_id',
    	'project_name',
    	'phase',
    	'category',
    	'bau_ocs',
    	'bau_oms',
    	'project_brief',
    	'project_document',
    	'project_justification',
    	'ocs_release',
    	'oms_release',
    	'release_target',
    	'revenue_projection',
    	'subscriber_projection',
    	'traffic_projection',
    	'srs_document',
    	'justification_development',
    	'status',
	'user_id',
    ];

    /**
     * Relationships
     */
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Methods
     */
    
    public static function quartalList()
    {
        return [
            '' => 'Select Quartal',
            'Q1' => 'Q1',
            'Q2' => 'Q2',
            'Q3' => 'Q3',
            'Q4' => 'Q4',
        ];
    }

    public static function weekList()
    {
        return [
            '' => 'Select Week',
            '01' => 'W1',
            '02' => 'W2',
            '03' => 'W3',
            '04' => 'W4',
	    '05' => 'W5',
        ];
    }

    public static function monthList()
    {
        return [
            '' => 'Select Month',
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
    }

    public static function yearList()
    {
        return [
            '' => 'Select Year',
            //'2016' => '2016',
            '2017' => '2017',
            '2018' => '2018',
            //'2019' => '2019',
            //'2020' => '2020',
       
	];    
    }

    public static function statusRequest()
    {
        return [
            '' => 'Select Status Request',
            'Submit' => 'Submit',
            'Drop' => 'Drop',
            'Pending' => 'Pending',
     	];
    }


    public static function bussinessOwnerList()
    {
        return [
            '' => 'Select Quartal',
            'KH' => 'Consumer',
            'EN' => 'Enterprise',
            'IR' => 'International',
        ];
    }

    public static function projectTypeList()
    {
        return [
            '' => 'Select Quartal',
            'P' => 'PLAN',
            'U' => 'UNPLAN',
        ];
    }

    public static function categoryList()
    {
        return [
            '' => 'Select Category',
            'BAU' => 'BAU',
            'PROJECT' => 'PROJECT',
	    'BPT' => 'BPT',
	    'Fixing Issue' => 'Fixing Issue'
        ];
    }

}
















