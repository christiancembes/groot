<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CabProject extends Model
{
    protected $table = 'cab_projects';

    protected $appends = ['project_ocs_offers'];

    protected $fillable = [
    	'development_id',
    	'project_name',
    	'phase',
    	'business_flow',
    	'bill',
    	'prov',
    	'rep',
    	'development_schedule',
    	'uat_schedule',
    	'actual_uat_schedule',
    	'release_schedule',
    	'actual_release_schedule',
    	'status_project',
    	'user_id',
    ];

    /**
     * Relationships
     */
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    
    /**
     * Methods
     */
    /**
     * Methods
     */
    
    public static function statusProjectList()
    {
        return [
            '' => 'Select Status Project',
            'Development' => 'Development',
            'Drop' => 'Drop',
            'Pending' => 'Pending',
            'Release' => 'Release',
        ];
    }

    public function getProjectOcsOffersAttribute()
    {
        return ProjectOcsOffer::leftJoin('ocs_offers', 'project_ocs_offers.offer_id', '=', 'ocs_offers.offer_id')
            ->where('project_ocs_offers.development_id', $this->development_id)
            ->select([
                'project_ocs_offers.development_id',
                'project_ocs_offers.offer_id',
                'ocs_offers.offer_name',
            ])
            ->get();
    }
    
}
