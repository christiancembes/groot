<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prerequisite extends Model
{
    protected $table = 'prerequisites';

    protected $fillable = [
    	'offer_id',
    	'criteria_prerequisite',
    	'offer_id_prerequisite',
    	'offer_name_prerequisite',
    ];
}
