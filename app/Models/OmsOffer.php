<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OmsOffer extends Model
{
    protected $table = 'oms_offers';

    protected $fillable = [
    	'offer_id',
    	'offer_name',
    	'code_component',
    	'component',
    	'owner',
    	'sales_channel',
    	'user_type',
    	'customer_type',
    	'sub_type',
    	'vas_code',
    	'apn',
    	'sapc',
    	'rim',
    	'code_act',
    	'code_deact',
    	'activation',
    	'deactivation',
    	'sch_1',
    	'sch_2',
    	'sch_3',
    	'scheduler_scenario',
    	'contract_duration',
    	'rules24h',
    ];
}











