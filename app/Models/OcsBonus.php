<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OcsBonus extends Model
{
    protected $table = 'ocs_bonuses';

    protected $fillable = [
    	'offer_id',
    	'offer_name',
    	'bonus_id',
    	'bonus_type',
    	'bonus_detail',
    	'bonus_info',
    	'package_id',
    	'package_name',
    	'priority'
    ];
}
