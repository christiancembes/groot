<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostProject extends Model
{
    protected $table = 'post_projects';

    protected $appends = ['project_ocs_offers'];

    protected $fillable = [
    	'development_id',
    	'project_name',
    	'phase',
    	'release_test',
    	'justification_release',
    	'rfs_schedule',
    	'remark_project',
    	'justification_rfs',
    	'status_service',
    	'user_id',
    ];

    /**
     * Relationships
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getProjectOcsOffersAttribute()
    {
        return ProjectOcsOffer::leftJoin('ocs_offers', 'project_ocs_offers.offer_id', '=', 'ocs_offers.offer_id')
            ->where('project_ocs_offers.development_id', $this->development_id)
            ->select([
                'project_ocs_offers.development_id',
                'project_ocs_offers.offer_id',
                'ocs_offers.offer_name',
            ])
            ->get();
    }


    public static function statusServiceList()
    {
        return [
            '' => 'Select Status Service',
            'Open' => 'Open',
            'Close' => 'Close',
        ];
    }
}
