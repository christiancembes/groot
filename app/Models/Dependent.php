<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    protected $table = 'dependent';

    protected $fillable = [
    	'offer_id',
    	'dependent_offer_id',
    	'action',
    	'dependent_offer_name',
    ];
}

