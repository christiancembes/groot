<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'keywords';

    protected $fillable = [
    	'offer_id',
    	'keyword',
    	'order_action',
    	'adn',
    	'lws',
    	'comp_id',
    ];
}
