<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
    protected $table = 'restrictions';

    protected $fillable = [
    	'offer_id',
    	'offer_id_restriction',
    	'offer_name_restriction',
    ];
}
