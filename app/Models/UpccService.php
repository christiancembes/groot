<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpccService extends Model
{
    protected $table = 'upcc_service';

    protected $fillable = [
        'upcc',
        'servicetype',
        'precedence',
        'qosmode',
        'durationtype',
        'calculatetime',
        'durationvalue',
        'description',
        'customerattribute',
        'resubscriptionoperation',
        'wifiaccess',
];
}
