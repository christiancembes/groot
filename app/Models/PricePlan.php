<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricePlan extends Model
{
    protected $table = 'price_plans';

    protected $fillable = [
    	'offer_id',
    	'offer_name',
    	'price_plan',
    	'price_plan_type',
    ];
}
