<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpccQuota extends Model
{
    protected $table = 'upcc_quota';

    protected $fillable = [
        'no',
	'upcc',
        'service_validity',
        'quota_name',
        'quota_value',
        'quota_manager',
        'isreset',
        'quota_interval',
        'mkstring',
        'billingcyclemode',
        'resettype',
        'resetdate',
	'days',
	'cycleunit',
	'quotasilecevalue',
	'rat',
	'description',
	'timeband',
	'qos',
	'servicezone',
	'terminalname',
	'roamregionplmn',
	
];
}
