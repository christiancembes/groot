<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeamlessGroup extends Model
{
    protected $table = 'seamless_group';

    protected $fillable = [
        'offer_id',
        'group_desc',
    ];
}
