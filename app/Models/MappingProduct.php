<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MappingProduct extends Model
{
    protected $table = 'mapping_product';
}
