<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferUpcc extends Model
{
    protected $table = 'offer_upcc';

    protected $fillable = [
        'no',
        'offer_id',
        'offer_name',
        'upcc',
        'config',
 ];
}
