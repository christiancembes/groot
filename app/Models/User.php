<?php

namespace App\Models;

use Mail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
	'username',
        'verification_token',
        'is_verified',
        'is_active',
        'last_login',
        'avatar',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    protected $casts = [
        'is_verified' => 'boolean',
    ];

    /**
     * Relationship
     */
    
    public function declareProjecs()
    {
        return $this->hasMany(DeclareProject::class);
    }

    public function cabProjecs()
    {
        return $this->hasMany(CabProject::class);
    }

    public function postProjecs()
    {
        return $this->hasMany(PostProject::class);
    }


    /**
     * Methods
     */

    public function hasPassword() {
        return $this->password !== '';
    }

    public function generateVerificationToken()
    {
        $token = $this->verification_token;

        if (!$token) {
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }

        return $token;
    }

    public function sendVerification()
    {
        $token = $this->generateVerificationToken();

        $user = $this;
        
        Mail::queue('auth.emails.verification', compact('user', 'token'), function ($m) use ($user) {
              $m->to($user->email, $user->name)->subject('Zoro Account Verification');
        }); 
    }

    public function verify() {
        $this->is_verified = 1;
        $this->verification_token = null;
        $this->save();
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function syncRoles()
    {
        $roles = array();

        if (request()->has('roles')) {
            foreach (request()->input('roles') as $role)
            {
                array_push($roles, $role);
            }
        }

        $this->roles()->sync($roles);
    }

    public static function statusList() {
        return [
            '' => 'Select Status',
            '1' => 'Active',
            '0' => 'Not Active',
        ];
    }

    public static function verifiedStatusList() {
        return [
            '' => 'Select Status',
            '1' => 'Verified',
            '0' => 'Not Verified',
        ];
    }
}
