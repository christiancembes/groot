<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpccNotif extends Model
{
    protected $table = 'upcc_notif';

    protected $fillable = [
        'upcc',
        'mapping',
        'notification',
        'smsflag',
        'emailflag',
	'soapflag',
	'sendinginterval',
	'uniquekey',
	'messagetitle',
	'messagecontent',
	'desciption',
];
}
