<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model{
    //
    protected $table = 'activity';
    
    protected $fillable = [
        'cab_project_id',
        'activity_name',
        'weight',
        'target_start_date',
        'actual_start_date', 
        'target_end_date', 
        'actual_end_date', 
        'status', 
        'remarks'
    ];
    
    
}

