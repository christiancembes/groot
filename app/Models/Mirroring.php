<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mirroring extends Model
{
    protected $table = 'mirrorings';

    protected $fillable = [
    	'offer_id',
    	'offer_id_mirroring',
    	'offer_name_mirroring',
    ];
}
