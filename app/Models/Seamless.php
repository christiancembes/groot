<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seamless extends Model
{
    protected $table = 'seamless';

    protected $fillable = [
    	'offer_id',
    	'seamless_offer_id',
    	'seamless_offer_name',
    	'x_day_number',
    ];
}
