<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectOcsOffer extends Model
{
    protected $table = 'project_ocs_offers';

    protected $fillable = [
    	'development_id',
    	'offer_id',
    ];

    public function declareProject()
    {
    	return $this->belongsToMany(DeclareProject::class);
    }

    public function cabProject()
    {
    	return $this->belongsToMany(CabProject::class, 'project_ocs_offers');
    }

    public function postProject()
    {
    	return $this->belongsToMany(PostProject::class);
    }
}
