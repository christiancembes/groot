<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OcsAllowance extends Model
{
    protected $table = 'ocs_allowances';

    protected $fillable = [
    	'offer_id',
    	'package_id',
    	'item_param_attr_name',
    	'item_param_attr_dimension',
    	'priority',
    	'offer_name',
    	'package_name',
    	'item_name',
    	'item_type',
    	'item_type_id',
    	'item_role',
    	'item_param_attr_uom',
    	'item_param_attr_value',
    ];
}
