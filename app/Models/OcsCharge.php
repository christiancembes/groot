<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OcsCharge extends Model
{
    protected $table = 'ocs_charges';

    protected $fillable = [
    	'offer_id',
    	'offer_name',
    	'charging_type',
    	'charge',
    	'vascode_name',
    	'period',
    	'proration',
    	'penalty_indicator',
    	'penalty_formula',
    	'billing_category',
    ];
}
