<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PostProject;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostProjectPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }
    
    public function update(User $user, PostProject $postProject)
    {
        return $user->id === $postProject->user_id;
    }
}
