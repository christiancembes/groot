<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CabProject;
use Illuminate\Auth\Access\HandlesAuthorization;

class CabProjectPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function update(User $user, CabProject $cabProject)
    {
        return $user->id === $cabProject->user_id;
    }
}
