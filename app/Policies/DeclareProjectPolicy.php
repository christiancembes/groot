<?php

namespace App\Policies;

use App\Models\User;
use App\Models\DeclareProject;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeclareProjectPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function update(User $user, DeclareProject $declareProject)
    {
        return $user->id === $declareProject->user_id;
    }


}
