// slideUp alert after 5 seconds
$('.alert-info, .alert-success').delay(5000).slideUp(300);

$(document.body).on('click', '.js-submit-confirm', function(event) {
    event.preventDefault()
    var $form = $(this).closest('form')
    var $el = $(this)
    var text = $el.data('confirm-message') ? $el.data('confirm-message') : 'Kamu tidak akan bisa membatalkan proses ini!'
    swal({
            title: 'Kamu yakin?',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ya, lanjutkan!',
            cancelButtonText: 'Batal',
            closeOnConfirm: true
        },
        function() {
            $form.submit()
        }
    );
});

