<div class="modal fade" id="upccquota" tabindex="-1" role="dialog" aria-labelledby="upccquota">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Quota</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Profile</th>
                    <th class="success">Validity</th>
                    <th class="success">Quota Name</th>
                    <th class="success">Quota Value</th>
                    <th class="success">Quota Manager</th>
	       {{-- <th class="success">Quota Interval</th>
		    <th class="success">Reset</th>
		    <th class="success">Reset Type</th>
		    <th class="success">Reset Date</th>
		    <th class="success">MK String</th>
		    <th class="success">BC Mode</th>
		    <th class="success">Days</th>
		    <th class="success">Cycle Unit</th>
		    <th class="success">Quota Slice</th>
 		    <th class="success">Rat</th>
		    <th class="success">Description</th>
		    <th class="success">Time Band</th>
		    <th class="success">Region</th> --}}

                </tr>
		  @forelse($upccquota as $upccquota)
                <tr>
                    <td>{{ $upccquota->upcc }}</td>
                    <td>{{ $upccquota->service_validity }}</td>
                    <td>{{ $upccquota->quota_name }}</td>
                    <td>{{ $upccquota->quota_value }}</td>
                    <td>{{ $upccquota->quota_manager }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="8">No Data</td>
                </tr>
                @endforelse
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
