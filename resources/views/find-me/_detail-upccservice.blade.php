<div class="modal fade" id="upccservice" tabindex="-1" role="dialog" aria-labelledby="upccservice">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Service</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Profile</th>
                    <th class="success">Type</th>
                    <th class="success">Priority</th>
                    <th class="success">QOS</th>
                    <th class="success">Duration Type</th>
		    <th class="success">Duration Value</th>
		    <th class="success">Calculate Time</th>
		    <th class="success">Multiple Add</th>
		    <th class="success">Description</th>
		    <th class="success">Wifi Type</th>
                </tr>
   		@forelse($upccservice as $upser)
                <tr>
                    <td>{{ $upser->upcc }}</td>
                    <td>{{ $upser->servicetype }}</td>
                    <td>{{ $upser->precedence }}</td>
                    <td>{{ $upser->qosmode }}</td>
                    <td>{{ $upser->durationtype }}</td>
		    <td>{{ $upser->calculatetime }}</td>
		    <td>{{ $upser->durationvalue }}</td>
		    <td>{{ $upser->resubscriptionoperation }}</td>
		    <td>{{ $upser->description }}</td>
		    <td>{{ $upser->wifiaccess }}</td>

                </tr>
                @empty
                <tr>
                    <td colspan="8">No Data</td>
                </tr>
                @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
