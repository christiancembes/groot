<div class="row">
        <div class="col-lg-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Find Me </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					
                    {!! Form::open(['url' => '/find-me', 'method'=>'post'])!!}

                        <div class="form-group {!! $errors->has('search_by') ? 'has-error' : '' !!}">
                            {!! Form::label('search_by', 'Search By') !!}
                            {!! Form::select('search_by', [
                                    ''=>'Search By',
                                    'development_id' => 'Development ID',
                                    'project_name'   => 'Project Name',
                                    'offer_id'       => 'Offer ID',
				    'offer_name'     => 'Offer Name',
				    'group_product'  => 'Group Product',
				    'type1'	     => 'Type',
				    'subtype1' 	     => 'Subtype',
				    'description'    => 'Description',
                                    'ocs_release'    => 'OCS Release',
                                    'oms_release'    => 'OMS Release',
				    'upcc'	     => 'Profile UPCC', 
				    'login' 	     => 'User CRM',
					

				], (request()->input('search_by') !== null) ? request()->input('search_by') : '', ['class'=>'form-control', 'id'=>'search_by']) !!}
                            {!! $errors->first('search_by', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group s-search-key hide {!! $errors->has('keyword') ? 'has-error' : '' !!}">
                            {!! Form::label('keyword', 'Keyword') !!}
                            {!! Form::text('keyword', (request()->input('keyword') !== null) ? request()->input('keyword') : '', ['class'=>'form-control', 'id' => 'keyword']) !!}
                            {!! $errors->first('keyword', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="date-from hide {!! $errors->has('from') ? 'has-error' : '' !!}" id="date_from">
                            {!! Form::label('from', 'From') !!}
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('from', (request()->input('from') !== null) ? request()->input('from') : '', ['class'=>'form-control']) !!}
                            </div>
                            {!! $errors->first('from', '<p class="help-block">:message</p>') !!}
                            <br>
                        </div>
                        
                        <div class="date-to hide {!! $errors->has('to') ? 'has-error' : '' !!}" id="date_to">
                            {!! Form::label('to', 'To') !!}
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('to', (request()->input('to') !== null) ? request()->input('to') : '', ['class'=>'form-control']) !!}
                            </div>
                            {!! $errors->first('to', '<p class="help-block">:message</p>') !!}
                            <br>
                        </div>

                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            </span>
                        </div>
                        
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
