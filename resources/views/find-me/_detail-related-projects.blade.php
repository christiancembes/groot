<div class="modal fade" id="related_projects" tabindex="-1" role="dialog" aria-labelledby="related_projects">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Related Projects</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Development ID</th>
                    <th class="success">Projet Name</th>
                    <th class="success">Phase</th>
                </tr>
                @forelse ($all_project as $project)
                <tr>
                    <?php
                    if ($project->phase == 'Plan & Define') {
                        $phase = 'declare-projects';
                    } else if ($project->phase == 'Design & Develop') {
                        $phase = 'cab-projects';
                    } else if ($project->phase == 'Test & Readiness') {
                        $phase = 'post-projects';
                    }
                    ?>
                    <td> <a href="{{ url('/'.$phase.'/'.$project->development_id) }}">{{ $project->development_id }}</a></td>
                    <td>{{ $project->project_name }}</td>
                    <td>{{ $project->phase }}</td>
                </tr>
                @empty
                <tr>
                    <td>No Data</td>
                </tr>
                @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
