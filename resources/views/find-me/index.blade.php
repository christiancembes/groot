@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Find Me</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')

	@include('find-me._search')

@endsection

@section('raw-script')

    <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == null || val == '')
        {
            $(".date-from, .date-to, .s-search-key").addClass("hide");
        }
        else if (val == "development_id" || val == "project_name" || val == "offer_id" || val == "offer_name" || val == "group_product" || val == "description" || val == "subtype1" || val == "type1" || val == "upcc" || val == "login")
        {
            $(".s-search-key").removeClass("hide");
            $(".date-from, .date-to").addClass("hide");
        }
        else if (val == "ocs_release" || val == "oms_release")
        {
            $(".date-from, .date-to").removeClass("hide");
            $(".s-search-key").addClass("hide");
        }

    });

    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == null || val == '')
        {
            $(".date-from, .date-to, .s-search-key").addClass("hide");
        }
        else if (val == "development_id" || val == "project_name" || val == "offer_id" || val == "offer_name" || val == "group_product" || val == "description" || val == "subtype1" || val == "type1" || val == "upcc" || val == "login")
        {
            $(".s-search-key").removeClass("hide");
            $(".date-from, .date-to").addClass("hide");
        }
        else if (val == "ocs_release" || val == "oms_release")
        {
            $(".date-from, .date-to").removeClass("hide");
            $(".s-search-key").addClass("hide");
        }

    });

    $('#date_from .input-group.date, #date_to .input-group.date').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    </script>

@endsection
