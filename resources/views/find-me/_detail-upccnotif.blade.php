<div class="modal fade" id="upccnotif" tabindex="-1" role="dialog" aria-labelledby="upccnotif">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Notif</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Profile</th>
                    <th class="success">Code Mapping</th>
                    <th class="success">Code Notification</th>
                    <th class="success">SMS Flag</th>
                    <th class="success">Message Content</th>
		    <th class="success">Email Flag</th>
		    <th class="success">Message Content</th>
		    <th class="success">Sender</th>
		    <th class="success">Service Key</th>
		    <th class="success">Interval</th> 
		   </tr>

                @forelse($upccnotif as $upccnotif)
                <tr>
                    <td>{{ $upccnotif->upcc }}</td>
                    <td>{{ $upccnotif->mapping }}</td>
                    <td>{{ $upccnotif->notification }}</td>
                    <td>{{ $upccnotif->smsflag }}</td>
                    <td>{{ $upccnotif->messagecontent }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="8">No Data</td>
                </tr>
                @endforelse 
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
