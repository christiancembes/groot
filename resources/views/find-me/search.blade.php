@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Find Me</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }

</style>
@endsection

@section('content')

	@include('find-me._search')

	@if ($search_by == 'development_id' OR $search_by == 'project_name')    
	<div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Find Me Results </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        @if (count($projects))
                        <center>
                            <strong>
                                We found {{ count($projects) }} results.
                            </strong>
                        </center>
                        <br>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Development ID</th>
                                <th>Project Name</th>
                                <th>Phase</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($projects as $project)
                                <tr>
                                    <td>{{ $project->development_id }}</td>
                                    <td>{{ $project->project_name }}</td>
                                    <td>{{ $project->phase }}</td>
                                    <td>
                                        <?php
                                        if ($project->phase == 'Plan & Define') {
                                            $phase = 'declare-projects';
                                        } else if ($project->phase == 'Design & Develop') {
                                            $phase = 'cab-projects';
                                        } else if ($project->phase == 'Test & Readiness') {
                                            $phase = 'post-projects';
                                        }
                                        ?>
                                        <a class="btn btn-xs btn-info" href="{{ url('/'.$phase.'/'.$project->development_id) }}"><i class="fa fa-eye"></i> Details</a>
                                        
                                        @can('update', $project)
                                        <a class="btn btn-xs btn-warning" href="{{ url('/'.$phase.'/'.$project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                                        @endcan

                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">There are no declare project data.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
@elseif ($search_by == 'upcc')
<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

		@if (count($upcc))
                        <center>
                            <strong>
                                We found {{ count($upcc) }} Configuration
                            </strong>
                        </center>
                        <br>
                        @endif

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Profile</th>
                            <th>Offer ID</th>
                            <th>Offer Name</th>
                            <th>Config</th>
                            <th>View Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @forelse ($upcc as $upcc)
				<tr>
                                <td>{{ $upcc->upcc }}</td>
                                <td>{{ $upcc->offer_id }}</td>
                                <td>{{ $upcc->offer_name }}</td>
                                <td>{{ $upcc->config }}</td>
                                <td>
				    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#upccservice"><i class="fa fa-eye"></i>Service</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#upccquota"><i class="fa fa-eye"></i>Quota</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#upccnotif"><i class="fa fa-eye"></i>Notification</a>
                                </td>
				</tr>
                                @empty
                                <td colspan="5">No Data</td>
                                @endforelse
			    </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

  @include('find-me._detail-upccservice')
  @include('find-me._detail-upccquota')
  @include('find-me._detail-upccnotif')

@elseif ($search_by == 'login')
<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>User Login Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                @if (count($login))
                        <center>
                            <strong>
                                We found {{ count($login) }} User
                            </strong>
                        </center>
                        <br>
                        @endif

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>User Login</th>
                            <th>Code User Role</th>
                            <th>Type User Role </th>
                            <th>Sales Channel</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @forelse ($login as $login)
                                <tr>
                                <td>{{ $login->login }}</td>
                                <td>{{ $login->code_userrole }}</td>
                                <td>{{ $login->userrole }}</td>
                                <td>{{ $login->sales_channel }}</td>
                                </tr>
                                @empty
                                <td colspan="5">No Data</td>
                                @endforelse
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@else
        {{-- {{ dd("OK") }} --}}
        {{-- display search by offer_id or offer_name --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Find Me Results </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {{-- @if (count($projects))
                        <center>
                            <strong>
                                We found {{ count($projects) }} results.
                            </strong>
                        </center>
                        <br>
                        @endif --}}
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Offer ID</th>
                                <th>Offer Name</th>
				<th>Group Product</th>
                                <th>Type</th>
                                <th>Sub Type</th>
                                <th>Description</th>
                                <th>Action</th>
                               {{--  <th>Sub Type</th>
                                <th>View Detail Billing</th>
                                <th>View Detail Provisioning</th>
                                <th>Project Related</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($ocs_offers as $ocs_offer)
                                <tr>
                                    <td>{{ $ocs_offer->offer_id }}</td>
                                    <td>{{ $ocs_offer->offer_name }}</td>
				    <td>{{ $ocs_offer->group_product }}</td>
                                    <td>{{ $ocs_offer->type1 }}</td>
                                    <td>{{ $ocs_offer->subtype1 }}</td>
                                    <td>{{ $ocs_offer->description }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="{{ url('/offers/'.$ocs_offer->offer_id) }}">
                                            <i class="fa fa-eye"></i>
                                            View Details
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3">No data found.</td>
                                </tr>
                                @endforelse

                                {{-- <tr> --}}
                                    

                                    {{-- @if ($ocs_offer != null)
                                    
                                    <td>{{ $ocs_offer->offer_id }}</td>
                                    <td>{{ $ocs_offer->offer_name }}</td>
                                    <td>
                                        <a href="{{ url('/offers/'.$ocs_offer->offer_id) }}">
                                            <i class="fa fa-eye"></i>
                                            View Details
                                        </a>
                                    </td>
                                    <td>{{ $ocs_offer->type1 }}</td>
                                    <td>{{ $ocs_offer->subtype1 }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#ocs_charges"><i class="fa fa-eye"></i> Show Charge</a>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#ocs_bonus"><i class="fa fa-eye"></i> Show Bonus</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#eligibility"><i class="fa fa-eye"></i> Show Eligibility</a>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#network_site"><i class="fa fa-eye"></i> Show Network Sites</a>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#notification"><i class="fa fa-eye"></i> Show Notification</a>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#keywords"><i class="fa fa-eye"></i> Show Keyword</a>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#scheduler"><i class="fa fa-eye"></i> Show Scheduler</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#related_projects"><i class="fa fa-eye"></i> Show Related Projects</a>
                                    </td>
                                    @else
                                    <td colspan="5">No Data</td>
                                    @endif --}}
                                {{-- </tr> --}}
                            </tbody>
                        </table>
                        <center>
                            {{-- {{ $ocs_offers->appends(compact('q'))->links() }} --}}
                        </center>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- @include('offers._detail-charges')
        @include('offers._detail-bonuses')
        @include('offers._detail-eligibilities')
        @include('offers._detail-network-sites')
        @include('offers._detail-notifications')
        @include('offers._detail-keywords')
        @include('offers._detail-schedulers')
        @include('find-me._detail-related-projects') --}}

@endif

@endsection

@section('raw-script')

    <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == null || val == '')
        {
            $(".date-from, .date-to, .s-search-key").addClass("hide");
        }
        else if (val == "development_id" || val == "project_name" || val == "offer_id" || val == "offer_name" || val == "group_product" || val == "description" || val == "subtype1" || val == "type1" || val == "upcc" || val == "login")
        {
            $(".s-search-key").removeClass("hide");
            $(".date-from, .date-to").addClass("hide");
        }
        else if (val == "ocs_release" || val == "oms_release")
        {
            $(".date-from, .date-to").removeClass("hide");
            $(".s-search-key").addClass("hide");
        }

    });

    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == null || val == '')
        {
            $(".date-from, .date-to, .s-search-key").addClass("hide");
        }
        else if (val == "development_id" || val == "project_name" || val == "offer_id" || val == "offer_name" || val == "group_product" || val == "description" || val == "subtype1" || val == "type1" || val == "upcc" || val == "login") 
        {
            $(".s-search-key").removeClass("hide");
            $(".date-from, .date-to").addClass("hide");
        }
        else if (val == "ocs_release" || val == "oms_release")
        {
            $(".date-from, .date-to").removeClass("hide");
            $(".s-search-key").addClass("hide");
        }

    });

    $('#date_from .input-group.date, #date_to .input-group.date').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    </script>


@endsection
