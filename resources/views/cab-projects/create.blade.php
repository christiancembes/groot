@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
                    <a href="/cab-projects">Design & Develop</a>
	            </li>
                <li class="active">
                    <strong>Create</strong>
                </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> Design & Develop Phase</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    {!! Form::model($cab_project, ['url' => url('/cab-projects/'.$cab_project->development_id), 'files' => true])!!}
                        
                        @include('cab-projects._form', ['model' => $cab_project]) 
                        <br>
                        <div class="form-group">    
                            <button type="submit" id="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i> Save
                            </button>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
