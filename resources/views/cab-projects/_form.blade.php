@if ($cab_project->id != null)
    <div class="form-group">
        {!! Form::label('ID', 'Design & Develop ID') !!}
        {!! Form::text('id', $cab_project->id, ['class'=>'form-control', 'disabled']) !!}
    </div>
@endif

@if ($cab_project->development_id != null)
    <div class="form-group">
        {!! Form::label('development_id', 'Development ID') !!}
        {!! Form::text('development_id', $cab_project->development_id, ['class'=>'form-control', 'disabled']) !!}
    </div>
    <input type="hidden" name="development_id" value="{{$cab_project->development_id}}">

    <div class="form-group">
        {!! Form::label('project_name', 'Project Name') !!}
        {!! Form::text('project_name', $cab_project->project_name, ['class'=>'form-control', 'disabled']) !!}
    </div>
    <input type="hidden" name="project_name" value="{{$cab_project->project_name}}">
@endif

<br>
<div class="{!! $errors->has('development_schedule') ? 'has-error' : '' !!}" id="development_schedule">
    {!! Form::label('development_schedule', 'Development Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('development_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('development_schedule', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="{!! $errors->has('uat_schedule') ? 'has-error' : '' !!}" id="uat_schedule">
    {!! Form::label('uat_schedule', 'UAT Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('uat_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('uat_schedule', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="{!! $errors->has('actual_uat_schedule') ? 'has-error' : '' !!}" id="actual_uat_schedule">
    {!! Form::label('actual_uat_schedule', 'Actual UAT Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('actual_uat_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('actual_uat_schedule', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="{!! $errors->has('release_schedule') ? 'has-error' : '' !!}" id="release_schedule">
    {!! Form::label('release_schedule', 'Release Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('release_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('release_schedule', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="{!! $errors->has('actual_release_schedule') ? 'has-error' : '' !!}" id="actual_release_schedule">
    {!! Form::label('actual_release_schedule', 'Actual Release Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('actual_release_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('actual_release_schedule', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="form-group {!! $errors->has('business_flow') ? 'has-error' : '' !!}">
    {!! Form::label('business_flow', 'Project Brief') !!}
        {!! Form::textarea('business_flow', null, ['class'=>'form-control']) !!}
    {!! $errors->first('business_flow', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('bill') ? 'has-error' : '' !!}">
    {!! Form::label('bill', 'Billing System') !!}
        {!! Form::textarea('bill', null, ['class'=>'form-control']) !!}
    {!! $errors->first('bill', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('prov') ? 'has-error' : '' !!}">
    {!! Form::label('prov', 'Provisioning System') !!}
        {!! Form::textarea('prov', null, ['class'=>'form-control']) !!}
    {!! $errors->first('prov', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('rep') ? 'has-error' : '' !!}">
    {!! Form::label('rep', 'Reporting') !!}
        {!! Form::textarea('rep', null, ['class'=>'form-control']) !!}
    {!! $errors->first('rep', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('billing_offers') ? 'has-error' : '' !!}">
    {!! Form::label('billing_offers', 'Billing Offers') !!}
    {!! Form::select('billing_offers[]',
        ['0' =>'']+App\Models\OcsOffer::lists('offer_name','offer_id')->all(),
        App\Models\ProjectOcsOffer::where('project_ocs_offers.development_id', $cab_project->development_id)->lists('offer_id')->toArray(),
        ['id'=>'billing_offers', 'class'=>'form-control', 'multiple']) !!}
    {!! $errors->first('billing_offers', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('status_project') ? 'has-error' : '' !!}">
    {!! Form::label('status_project', 'Status Project') !!}
    {!! Form::select('status_project', [''=>'Select Category']+App\Models\CabProject::statusProjectList(), null, ['class'=>'form-control', 'id'=>'status-project-list']) !!}
    {!! $errors->first('status_project', '<p class="help-block">:message</p>') !!}
</div>

@section('raw-script')

    <script src="{{ URL::to('/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">

        $('#quartal-list').select2({
            'placeholder':'Select Quartal',
        });

        $('#status-project-list').select2({
            'placeholder':'Select Status Project',
        });

        $('#billing_offers').select2({
            'placeholder':'Choose OCS Offers...',
            tags:true,
            multiple: true,
        });

        $('#development_schedule .input-group.date, #uat_schedule .input-group.date, #actual_uat_schedule .input-group.date, #release_schedule .input-group.date, #actual_release_schedule .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        var editor_config = {
            path_absolute : "/",
            selector: "textarea",
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
              if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
              });
            }
          };

          tinymce.init(editor_config);

    </script>
@endsection
