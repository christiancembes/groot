@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
                    <a href="/cab-projects">Design & Develop</a>
	            </li>
                <li class="active">
                    <strong>Details</strong>
                </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Design & Develop Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    @can('update', $cab_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/cab-projects/'.$cab_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 200px">Development ID</th>
                            <td>{{ $cab_project->development_id }}</td>
                        </tr>
                        <tr>
                            <th>Project Name</th>
                            <td>{{ $cab_project->project_name }}</td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td>{{ $cab_project->category }}</td>
                        </tr>

                        @if ($cab_project->category == 'BAU')
                        <tr>
                            <th>BAU OCS</th>
                            <td>{{ getBAUFormat($cab_project->bau_ocs) }}</td>
                        </tr>
                        <tr>
                            <th>BAU OMS</th>
                            <td>{{ getBAUFormat($cab_project->bau_oms) }}</td>
                        </tr>
                        @endif

                        <tr>
                            <th>Project Brief</th>
                            <td>{!! $cab_project->project_brief !!}</td>
                        </tr>
                        <tr>
                            <th>Revenue Projection</th>
                            <td>{!! $cab_project->revenue_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Subscriber Projection</th>
                            <td>{!! $cab_project->subscriber_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Traffic Projection</th>
                            <td>{!! $cab_project->traffic_projection !!}</td>
                        </tr>

                        <tr>
                            <th>Project Document</th>
                            <td>
                                @if ($cab_project->project_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$cab_project->project_document.'?ver='.time()) }}"><i class="fa fa-eye"></i> View Document</a>
				    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Project Justification</th>
                            <td>
                                @if ($cab_project->project_justification !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$cab_project->project_justification) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>OCS Release</th>
                            <td>{{ $cab_project->ocs_release }}</td>
                        </tr>
                        <tr>
                            <th>OMS Release</th>
                            <td>{{ $cab_project->oms_release }}</td>
                        </tr>
                        <tr>
                            <th>Release Target</th>
                            <td>{{ $cab_project->release_target }}</td>
                        </tr>
                        <tr>
                            <th>SRS Document</th>
                            <td>
                                @if ($cab_project->srs_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$cab_project->srs_document) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Justification Development</th>
                            <td>
                                @if ($cab_project->justification_development !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$cab_project->justification_development) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Buessiness Flow</th>
                            <td>{!! $cab_project->business_flow !!}</td>
                        </tr>

                        <tr>
                            <th>Billing System</th>
                            <td>{!! $cab_project->bill !!}</td>
                        </tr>

                        <tr>
                            <th>Provisioning System</th>
                            <td>{!! $cab_project->prov !!}</td>
                        </tr>

                        <tr>
                            <th>Reporting</th>
                            <td>{!! $cab_project->rep !!}</td>
                        </tr>

                        <tr>
                            <th>Development Schedule</th>
                            <td>{{ $cab_project->development_schedule }}</td>
                        </tr>

                        <tr>
                            <th>UAT Schedule</th>
                            <td>{{ $cab_project->uat_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Release Schedule</th>
                            <td>{{ $cab_project->release_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Actual Release Schedule</th>
                            <td>{{ $cab_project->actual_release_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Actual UAT Schedule</th>
                            <td>{{ $cab_project->actual_uat_schedule }}</td>
                        </tr>
                        

                        <tr>
                            <th>Billing Offers</th>
                            <td>
                                
                                {{-- {{ dd($cab_project->projectOcsOffers) }} --}}

                                @forelse ($cab_project->projectOcsOffers as $ocs_offer)
                                    <ul>
                                        <li><a class="btn btn-info" href="{{ url('/offers/'.$ocs_offer->offer_id) }}"> <i class="fa fa-eye"></i> {{ $ocs_offer->offer_id }} : {{ ($ocs_offer->offer_name) ?: '' }}</a></li>
                                    </ul>
                                @empty
                                    This project does not have ocs offers.
                                @endforelse

                                {{-- <div class="form-group {!! $errors->has('billing_offers') ? 'has-error' : '' !!}">
                                    {!! Form::select('billing_offers[]',
                                        ['0' =>'']+App\Models\OcsOffer::lists('offer_name','offer_id')->all(),
                                        App\Models\ProjectOcsOffer::where('project_ocs_offers.development_id', $cab_project->development_id)->lists('offer_id')->toArray(),
                                        ['id'=>'billing_offers', 'class'=>'form-control', 'multiple']) !!}
                                    {!! $errors->first('billing_offers', '<p class="help-block">:message</p>') !!}
                                </div> --}}

                            </td>

                        </tr>

                        <tr>
                            <th>Status Project</th>
                            <td>
                                @if ($cab_project->status_project == 'Pending')
                                    <span class="text-warning">
                                        {{ $cab_project->status_project }}
                                    </span>
                                @elseif ($cab_project->status_project == 'Development')
                                    <span class="text-info">
                                        {{ $cab_project->status_project }}
                                    </span>
                                @elseif ($cab_project->status_project == 'Drop')
                                    <span class="text-danger">
                                        {{ $cab_project->status_project }}
                                    </span>
                                @elseif ($cab_project->status_project == 'Release')
                                    <span class="text-success">
                                        {{ $cab_project->status_project }}
                                    </span>
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Created at</th>
                            <td>{{ $cab_project->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td>{{ $cab_project->updated_at }}</td>
                        </tr>
                        
                    </table>

                    @can('update', $cab_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/cab-projects/'.$cab_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')

    <script src="{{ URL::to('/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">

        $('#billing_offers').select2({
            'placeholder':'Choose OCS Offers...',
            tags:true,
            multiple: true,
        });

    </script>
@endsection
