@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>DESIGN & DEVELOP</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Search Projects </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					{{-- add button --}}
					{{-- search fields --}}
					{!! Form::open(['url' => '/cab-projects', 'method'=>'get'])!!}
	                        <div class="input-group">
                        		<input type="text" class="form-control" name="q" placeholder="Search Project Name...">
		                        <span class="input-group-btn">
		                        	<button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
		                    	</span>
	                    	</div>
                    {!! Form::close() !!}
                    
                    @include('layouts._show-all-data', ['url' => '/cab-projects'])

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="declare">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List Item in Plan & Define Phase </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <center>
                        @if ($total_declare_project > 0)
                        <strong>
                            There are {{ $total_declare_project }} project still in Plan & Define  Phase.
                        </strong>
                        @else
                        <strong>
                            There are no project in Plan & Define phase
                        </strong>
                        @endif
                    </center>

                    <br>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Development ID</th>
                            <th>Project Name</th>
                            <th>Phase</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@forelse($declare_projects as $dp)
                            <tr>
                                <td>{{ $dp->id }}</td>
                                <td>{{ $dp->development_id }}</td>
                                <td>{{ $dp->project_name }}</td>
                                <td>{{ $dp->phase }}</td>
                                <td>
                                    
                                    @if (Auth::check())
                                    <a class="btn btn-xs btn-warning" href="{{ url('/cab-projects/'.$dp->development_id.'/create') }}"><i class="fa fa-edit"></i> Process to Design & Develop</a>
                                    @endif
                                    
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">There are no project in Plan & Define phase.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <center>
	                    {{ $declare_projects->appends(compact('q'))->fragment('declare')->links() }}
	                </center>

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="cab">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> List item in Design & Develop Phase </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <center>
                        @if ($total_cab_project > 0)
                        <strong>
                            There are {{ $total_cab_project }} project still in Design & Develop Phase.
                        </strong>
                        @else
                        <strong>
                            There are no project in Design & Develop phase
                        </strong>
                        @endif
                    </center>

                    <br>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Development ID</th>
                            <th>Project Name</th>
                            <th>Phase</th>
                            <th>Status Project</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($cab_projects as $cab)
                            <tr>
                                <td>{{ $cab->id }}</td>
                                <td>{{ $cab->development_id }}</td>
                                <td>{{ $cab->project_name }}</td>
                                <td>{{ $cab->phase }}</td>
                                <td>
                                    @if ($cab->status_project == 'Pending')
                                        <span class="text-warning">
                                            {{ $cab->status_project }}
                                        </span>
                                    @elseif ($cab->status_project == 'Development')
                                        <span class="text-info">
                                            {{ $cab->status_project }}
                                        </span>
                                    @elseif ($cab->status_project == 'Drop')
                                        <span class="text-danger">
                                            {{ $cab->status_project }}
                                        </span>
                                    @elseif ($cab->status_project == 'Release')
                                        <span class="text-success">
                                            {{ $cab->status_project }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    
                                    <a class="btn btn-xs btn-info" href="{{ url('/cab-projects/'.$cab->development_id) }}"><i class="fa fa-eye"></i> Details</a>

                                    @can('update', $cab)
                                    <a class="btn btn-xs btn-warning" href="{{ url('/cab-projects/'.$cab->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                                    @endcan
                                    
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">There are no Design & Develop Info.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <center>
                        {{ $cab_projects->appends(compact('q'))->fragment('cab')->links() }}
                    </center>

                </div>
            </div>
        </div>
    </div>
@endsection
