@extends('layouts.app')

@section('breadcrumb')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">
                        <strong>Check Profile</strong>
                    </li>
                </ol>
            </div>
        </div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }

</style>

@endsection

@section('content')

        @include('check-profile._search_checkprofile')

        {{-- display search by Check DSP --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Check DSP </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        @if (count($dataFromAPI2))
                        <center>
                            <strong>
                                We found {{ count($dataFromAPI2) }} results.
                            </strong>
                        </center>
                        <br>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brand</th>
                                <th>IMEI</th>
                                <th>Result</th>
                                <th>Card Type</th>
                                <th>LAC</th>
                                <th>CI</th>
                                <th>SCP</th>
                                <th>IMSI</th>
                                <th>LTE</th>
                                <th>Kota/Kabupaten</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($dataFromAPI2->ResultCode != 1038)
                                <tr>
                                    <td>{{ $dataFromAPI2->product_id }}</td>
                                    <td>{{ $dataFromAPI2->imei }}</td>
                                    <td>{{ $dataFromAPI2->ResultDesc }}</td>
                                    <td>{{ $dataFromAPI2->card_type}}</td>
                                    <td>{{ $dataFromAPI2->lac}}</td>
                                    <td>{{ $dataFromAPI2->ci}}</td>
                                    <td>{{ $dataFromAPI2->scp_id}}</td>
                                    <td>{{ $dataFromAPI2->imsi}}</td>
                                    <td>{{ $dataFromAPI2->lte}}</td>
                                    <td>{{ $dataFromAPI2->kabupaten}}</td>
                                </tr>
                               @else
                                <td colspan="5">No Data</td>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('raw-script')

     <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }

    });
    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }


    });


    </script>
@endsection
