@extends('layouts.app')

@section('breadcrumb')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">
                        <strong>Check Profile</strong>
                    </li>
                </ol>
            </div>
        </div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }

</style>
@endsection

@section('content')

        @include('check-profile._search_checkprofile')

        {{-- display search by Check Bonus --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Check Bonus </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
		    <div class="ibox-content">
                        @if (count($dataFromAPI1))
                        <center>
                            <strong>
                                We found {{ count($dataFromAPI1) }} results.
                            </strong>
                        </center>
                        <br>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Bonus ID</th>
                                <th>Bonus Description</th>
                                <th>Type</th>
                                <th>Sub Type</th>
                                <th>Consumption Time</th>
                                <th>Remaining Quota</th>
                                <th>Expire Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($dataFromAPI1 as $API1)
                                <tr>
                                    <td>{{ $API1->bucketid }}</td>
                                    <td>{{ $API1->bucketdescription }}</td>
                                    <td>{{ $API1->class }}</td>
                                    <td>{{ $API1->type}}</td>
                                    <td>{{ $API1->consumptiontime}}</td>
                                    <td>{{ $API1->remainingquota}}</td>
                                    <td>{{ $API1->expirydate}}</td>
				</tr>
				@empty
                                <tr>
                                    <td colspan="3">No data found.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('raw-script')

     <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }

    });
    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }


    });


    </script>
@endsection
