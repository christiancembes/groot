<div class="row">
        <div class="col-lg-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Check </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    {!! Form::open(['url' => '/check-profile', 'method'=>'post'])!!}

                        <div class="form-group {!! $errors->has('search_by') ? 'has-error' : '' !!}">
                            {!! Form::label('search_by', 'Search By') !!}
                            {!! Form::select('search_by', [
                                    ''=>'Search By',
                                    'cm_check'      => 'Check Customer',
                                    'dsp_check'     => 'Check DSP',
                                    'uxp_check'     => 'Check Bonus',

                                ], (request()->input('search_by') !== null) ? request()->input('search_by') : '', ['class'=>'form-control', 'id'=>'search_by']) !!}
                            {!! $errors->first('search_by', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group s-search-key hide {!! $errors->has('keyword') ? 'has-error' : '' !!}">
                            {!! Form::label('MSISDN', 'MSISDN') !!}
                            {!! Form::text('keyword', (request()->input('keyword') !== null) ? request()->input('keyword') : '', ['class'=>'form-control', 'id' => 'keyword']) !!}
                            {!! $errors->first('keyword', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            </span>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
