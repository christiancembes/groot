
@extends('layouts.app')

@section('breadcrumb')

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">
                        <strong>Customer Info</strong>
                    </li>
                </ol>
            </div>
        </div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }

</style>

@endsection

@section('content')

        @include('check-profile._search_checkprofile')

        {{-- display search by Customer Check --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Customer Info </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        @if (count($dataFromAPI3))
                        <center>
                            <strong>
                                We found {{ count($dataFromAPI3) }} results.
                            </strong>
                        </center>
                        <br>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>ID</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($dataFromAPI3  != null)
                                <tr>
                                    <td>{{ $dataFromAPI3->FirstName }}</td>
                                    <td>{{ $dataFromAPI3->LastName }}</td>
                                    <td>{{ $dataFromAPI3->ID }}</td>
                                    <td>{{ $dataFromAPI3->Type}}</td>
                                </tr>
                               @else
                                <td colspan="5">No Data</td>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('raw-script')

     <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }

    });
    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }


    });


    </script>
@endsection
