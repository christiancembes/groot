@extends('layouts.app')

@section('breadcrumb')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">
                        <strong>Check Profile</strong>
                    </li>
                </ol>
            </div>
        </div>
@endsection

@section('content')

        @include('check-profile._search')

@endsection

@section('raw-script')

    <script type="text/javascript">

        /**
     * Function for displaying form at FindMe
     */
    $("#search_by").change(function()
    {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }

    });
    /**
     * setup findMe form when document ready
     */
    $(document).ready(function() {

        var val = $("#search_by").val();
        // alert(val);

        if (val == "cm_check" || val == "dsp_check" || val == "uxp_check")
        {
            $(".s-search-key").removeClass("hide");
        }


    });


    </script>

@endsection
