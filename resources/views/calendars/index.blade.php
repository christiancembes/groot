@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Calendars</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12 col-md-6 animated fadeInDown">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Release Schedules </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					
                    <div class="animated fadeInDown" id="calendar"></div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')

    <script type="text/javascript">

    $(document).ready(function() {

        /* initialize the calendar
         -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            events: {!! $release_dates !!}
        });

    });

    </script>

@endsection
