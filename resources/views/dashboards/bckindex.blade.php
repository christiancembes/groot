@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-12">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Dashboards</strong>
	            </li>
	        </ol>

            <div class="pull-right">
               <select name="dashboard_year" id="dashboard_year">
                   @for ($i = 2015; $i <= 2017; $i ++)
                       <option value="{{$i}}" {{ ($i == request()->input('year', date('Y'))) ? 'selected' : '' }}>{{$i}}</option>
                   @endfor
                   <option value="all" {{ (request()->input('year') == 'all') ? 'selected' : '' }}> All</option>
               </select>
           </div>

	    </div>
	</div>
@endsection

@section('content')
    
    <?php

    /**
     * CUSTOMER BASE
     */
    
    $reg_total = 0;
    $cor_total = 0;
    $all_total = 0;

    foreach ($customer_bases as $cb) {
        $reg_total = $reg_total + $cb->number_of_regular;
        $cor_total = $cor_total + $cb->number_of_corporate;
        $all_total = $all_total + $cb->total;
    }

    $customer_base_labels = $customer_bases->lists('month_and_year');

    // dd($customer_base_labels);

    $customer_base_regular = $customer_bases->lists('number_of_regular');
    $customer_base_corporate = $customer_bases->lists('number_of_corporate');
    $customer_base_total = $customer_bases->lists('total');

    /**
     * TOP OFFERS
     */
    $month_and_year     = ($offer->month_and_year) ? $offer->month_and_year : '';
    $legacy_name        = ($offer->legacy_name) ? $offer->legacy_name : '';
    $legacy_number      = ($offer->legacy_number) ? $offer->legacy_number : 0;
    $core_name          = ($offer->core_name) ? $offer->core_name : '';
    $core_number        = ($offer->core_number) ? $offer->core_number : 0;
    $bundling_name      = ($offer->bundling_name) ? $offer->bundling_name : ''; 
    $bundling_number    = ($offer->bundling_number) ? $offer->bundling_number : 0;

    $offer_data = [
        ['name' => $legacy_name, 'number' => $legacy_number],
        ['name' => $core_name, 'number' => $core_number],
        ['name' => $bundling_name, 'number' => $bundling_number],
    ];

    $offer_data = collect($offer_data);
    $offer_data = $offer_data->sortByDesc('number');
    $offer_data = $offer_data->values()->all();
    // dd($offer_data);
    
    /**
     * PRICE PLAN
     */
    $month_and_year         = ($priceplan->month_and_year) ? $priceplan->month_and_year : '';
    $price_plan1            = ($priceplan->price_plan1) ? $priceplan->price_plan1 : '';
    $number_price_plan1     = ($priceplan->number_price_plan1) ? $priceplan->number_price_plan1 : 0;
    $price_plan2            = ($priceplan->price_plan2) ? $priceplan->price_plan2 : '';
    $number_price_plan2     = ($priceplan->number_price_plan2) ? $priceplan->number_price_plan2 : 0;
    $price_plan3            = ($priceplan->price_plan3) ? $priceplan->price_plan3 : '';
    $number_price_plan3     = ($priceplan->number_price_plan3) ? $priceplan->number_price_plan3 : 0;

    $priceplan_data = [
        ['name' => $price_plan1, 'number' => $number_price_plan1],
        ['name' => $price_plan2, 'number' => $number_price_plan2],
        ['name' => $price_plan3, 'number' => $number_price_plan3],
    ];

    $priceplan_data = collect($priceplan_data);
    $priceplan_data = $priceplan_data->sortByDesc('number');
    $priceplan_data = $priceplan_data->values()->all();

    /**
     * PROJECT_STATUS
     */
    $project_status_labels = $status_projects->lists('status_project');
    $project_status_values = $status_projects->lists('total');

    /**
     * STATUS_SERVICES
     */
    $status_service_labels = $status_services->lists('status_service');
    $status_service_values = $status_services->lists('total');

    /**
     * PROJECT_TYPES
     */
    $project_type_labels = $project_types->lists('project_type');
    $project_type_values = $project_types->lists('total');

    ?>

    <div class="row">
        
        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">Total</span>
                    <h5>Top Customer Base</h5>
                </div>
                <a data-toggle="modal" data-target="#customerBase">
                    <div class="ibox-content">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%;" class="text-info">Regular</td>
                                <td class="pull-right">&nbsp;&nbsp;&nbsp;{{ number_format($reg_total) }}</td>
                            </tr>
                            <tr>
                                <td class="text-info">Corporate</td>
                                <td class="pull-right">&nbsp;&nbsp;&nbsp;{{ number_format($cor_total) }}</td>
                            </tr>
                            <tr>
                                <td class="text-info">Total</td>
                                <td class="pull-right">&nbsp;&nbsp;&nbsp;{{ number_format($all_total) }}</td>
                            </tr>
                        </table>
                        {{-- <br>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> --}}
                    </div>
                </a>
            </div>
        </div>
        
        
        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Total</span>
                    <h5>Top Offers</h5>
                </div>
                <a data-toggle="modal" data-target="#topOffers">
                    <div class="ibox-content">
                        <table style="width: 100%;">
                            @forelse ($offer_data as $offer)
                            <tr>
                                <td style="width: 50%;" class="text-info">{{ ucfirst($offer['name']) }}</td>
                                <td class="pull-right">&nbsp;&nbsp;&nbsp;{{ number_format($offer['number']) }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No data.</td>
                            </tr>
                            @endforelse
                        </table>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Total</span>
                    <h5>Top Price Plans</h5>
                </div>
                <a data-toggle="modal" data-target="#topPriceplans">
                    <div class="ibox-content">
                        <table style="width: 100%;">
                            @forelse ($priceplan_data as $priceplan)
                            <tr>
                                <td style="width: 50%;" class="text-info">{{ ucfirst($priceplan['name']) }}</td>
                                <td class="pull-right">&nbsp;&nbsp;&nbsp;{{ number_format($priceplan['number']) }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No data.</td>
                            </tr>
                            @endforelse
                        </table>
                    </div>
                </a>
            </div>
        </div>
        
    </div>

    <div class="row">
        
        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-warning pull-right">DECLARE</span>
                    <h5>Declare Project</h5>
                </div>
                <a href="{{ url('/declare-projects') }}">
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $total_declare }}</h1>
                        <div class="stat-percent font-bold text-success"> 
                            @if ($total_declare !== 0)
                            {{ round(($total_declare / $total_declare) * 100) }} % <i class="fa fa-bolt"></i>
                            @endif
                        </div>
                        <small>Total Declare Project</small>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">CAB</span>
                    <h5>CAB Project</h5>
                </div>
                <a href="{{ url('/cab-projects?#cab') }}">
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $total_cab }}</h1>
                        <div class="stat-percent font-bold text-success">
                            @if ($total_declare !== 0)
                            {{ round(($total_cab / $total_declare) * 100) }} % <i class="fa fa-bolt"></i>
                            @endif
                        </div>
                        <small>Total CAB Project</small>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">POST</span>
                    <h5>POST Project</h5>
                </div>
                <a href="{{ url('/post-projects?#post') }}">
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $total_post }}</h1>
                        <div class="stat-percent font-bold text-success">
                            @if ($total_declare !== 0)
                            {{ round(($total_post / $total_declare) * 100) }} % <i class="fa fa-bolt"></i>
                            @endif
                        </div>
                        <small>Total POST Project</small>
                    </div>
                </a>
            </div>
        </div>
        
    </div>

    <div class="row">

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">PROJECT STATUS</span>
                    <h5>Project Status</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="project_status_charts"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">STATUS SERVICE</span>
                    <h5>Status Service</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="status_service_charts"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 animated bounceIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">PROJECT TYPE</span>
                    <h5>Project Types</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="project_type_charts"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>
	
    {{-- MODAL --}}
    <!-- Modal -->
    <div class="modal fade" id="customerBase" tabindex="-1" role="dialog" aria-labelledby="customerBase">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Top Customer Base</h4>
          </div>
          <div class="modal-body">
            <div>
                <canvas id="customer_base_reports"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="topOffers" tabindex="-1" role="dialog" aria-labelledby="topOffers">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Top Offers</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <th class="success">Date</th>
                        <th class="success">Legacy Name</th>
                        <th class="success">Legacy Number</th>
                        <th class="success">Core Name</th>
                        <th class="success">Core Number</th>
                        <th class="success">Bundling Name</th>
                        <th class="success">Bundling Number</th>
                    </tr>
                    <tr>
                        <td>{{$month_and_year}}</td>
                        <td>{{$legacy_name}}</td>
                        <td>{{number_format($legacy_number)}}</td>
                        <td>{{$core_name}}</td>
                        <td>{{number_format($core_number)}}</td>
                        <td>{{$bundling_name}}</td>
                        <td>{{number_format($bundling_number)}}</td>
                    </tr>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="topPriceplans" tabindex="-1" role="dialog" aria-labelledby="topPriceplans">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Top Priceplans</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <th class="success">Date</th>
                        <th class="success">PricePlan 1</th>
                        <th class="success">Number PricePlan1</th>
                        <th class="success">PricePlan 2</th>
                        <th class="success">Number PricePlan2</th>
                        <th class="success">PricePlan 3</th>
                        <th class="success">Number PricePlan3</th>
                    </tr>
                    <tr>
                        <td>{{$month_and_year}}</td>
                        <td>{{$price_plan1}}</td>
                        <td>{{number_format($number_price_plan1 )}}</td>
                        <td>{{$price_plan2}}</td>
                        <td>{{number_format($number_price_plan2)}}</td>
                        <td>{{$price_plan3}}</td>
                        <td>{{number_format($number_price_plan3)}}</td>
                    </tr>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('raw-script')

    <script type="text/javascript">
    
    $(function () {

        $('#dashboard_year').on('change', function(){
            year = $(this).val();
            // alert(year);
            window.location.href = "{{ url('/dashboards/') }}" + "?year=" + year;
        });

        $('#customerBase').on('show.bs.modal', function () {

            var ctx = document.getElementById('customer_base_reports').getContext('2d');

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: {!! $customer_base_labels !!},
                    datasets: [
                        {
                            label: 'Regular',
                            data: {{ $customer_base_regular }},
                            backgroundColor: "#64B5F6"
                        },
                        {
                            label: 'Corporate',
                            data: {{ $customer_base_corporate }},
                            backgroundColor: "#FF8A65"
                        },
                        {
                            label: 'Total',
                            data: {{ $customer_base_total }},
                            backgroundColor: "#9E9E9E"
                        }
                    ]
                }
            });

        }); // end customerBase

        // show project_status charts
        var ctx = document.getElementById("project_status_charts").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: {!! $project_status_labels !!},
            datasets: [{
              backgroundColor: [
                "#2ecc71",
                "#3498db",
                "#f1c40f",
                "#e74c3c",
              ],
              data: {!! $project_status_values !!}
            }]
          }
        });

        // show status_service charts
        var ctx = document.getElementById("status_service_charts").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: {!! $status_service_labels !!},
            datasets: [{
              backgroundColor: [
                "#e74c3c",
                "#3498db",
              ],
              data: {!! $status_service_values !!}
            }]
          }
        });

        // show status_service charts
        var ctx = document.getElementById("project_type_charts").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: {!! $project_type_labels !!},
            datasets: [{
              backgroundColor: [
                "#e74c3c",
                "#3498db",
              ],
              data: {!! $project_type_values !!}
            }]
          }
        });

    });

    </script>

@endsection
