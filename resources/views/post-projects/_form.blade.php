@if ($post_project->id != null)
    <div class="form-group">
        {!! Form::label('ID', 'CAB Project ID') !!}
        {!! Form::text('id', $post_project->id, ['class'=>'form-control', 'disabled']) !!}
    </div>
@endif

@if ($post_project->development_id != null)
    <div class="form-group">
        {!! Form::label('development_id', 'Development ID') !!}
        {!! Form::text('development_id', $post_project->development_id, ['class'=>'form-control', 'disabled']) !!}
    </div>
    <input type="hidden" name="development_id" value="{{$post_project->development_id}}">

    <div class="form-group">
        {!! Form::label('project_name', 'Project Name') !!}
        {!! Form::text('project_name', $post_project->project_name, ['class'=>'form-control', 'disabled']) !!}
    </div>
    <input type="hidden" name="project_name" value="{{$post_project->project_name}}">
@endif

<br>
<div class="{!! $errors->has('release_test') ? 'has-error' : '' !!}" id="release_test">
    {!! Form::label('release_test', 'Release Test') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('release_test', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('release_test', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="{!! $errors->has('rfs_schedule') ? 'has-error' : '' !!}" id="rfs_schedule">
    {!! Form::label('rfs_schedule', 'RFS Schedule') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('rfs_schedule', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('rfs_schedule', '<p class="help-block">:message</p>') !!}
</div>

<br>
<div class="form-group {!! $errors->has('remark_project') ? 'has-error' : '' !!}">
    {!! Form::label('remark_project', 'Project Brief') !!}
        {!! Form::textarea('remark_project', null, ['class'=>'form-control']) !!}
    {!! $errors->first('remark_project', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('justification_release') ? 'has-error' : '' !!}">
    {!! Form::label('justification_release', 'Justification Development') !!}
    {!! Form::file('justification_release') !!}
    {!! $errors->first('justification_release', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('justification_rfs') ? 'has-error' : '' !!}">
    {!! Form::label('justification_rfs', 'SRS Document') !!}
    {!! Form::file('justification_rfs') !!}
    {!! $errors->first('justification_rfs', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('status_service') ? 'has-error' : '' !!}">
    {!! Form::label('status_service', 'Status Project') !!}
    {!! Form::select('status_service', [''=>'Select Category']+App\Models\PostProject::statusServiceList(), null, ['class'=>'form-control', 'id'=>'status-service-list']) !!}
    {!! $errors->first('status_service', '<p class="help-block">:message</p>') !!}
</div>

@section('raw-script')

    <script src="{{ URL::to('/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">

        $('#status-service-list').select2({
            'placeholder':'Select Status Project',
        });

        $('#release_test .input-group.date, #rfs_schedule .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        var editor_config = {
            path_absolute : "/",
            selector: "textarea",
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
              if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
              });
            }
          };

          tinymce.init(editor_config);

    </script>
@endsection