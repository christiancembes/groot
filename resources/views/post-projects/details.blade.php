@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
                    <a href="/post-projects">POST Projects</a>
	            </li>
                <li class="active">
                    <strong>Details</strong>
                </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>POST Project Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    @can('update', $post_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/post-projects/'.$post_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 200px">Development ID</th>
                            <td>{{ $post_project->development_id }}</td>
                        </tr>
                        <tr>
                            <th>Project Name</th>
                            <td>{{ $post_project->project_name }}</td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td>{{ $post_project->category }}</td>
                        </tr>

                        @if ($post_project->category == 'BAU')
                        <tr>
                            <th>BAU OCS</th>
                            <td>{{ getBAUFormat($post_project->bau_ocs) }}</td>
                        </tr>
                        <tr>
                            <th>BAU OMS</th>
                            <td>{{ getBAUFormat($post_project->bau_oms) }}</td>
                        </tr>
                        @endif

                        <tr>
                            <th>Project Brief</th>
                            <td>{!! $post_project->project_brief !!}</td>
                        </tr>
                        <tr>
                            <th>Revenue Projection</th>
                            <td>{!! $post_project->revenue_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Subscriber Projection</th>
                            <td>{!! $post_project->subscriber_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Traffic Projection</th>
                            <td>{!! $post_project->traffic_projection !!}</td>
                        </tr>

                        <tr>
                            <th>Project Document</th>
                            <td>
                                @if ($post_project->project_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->project_document.'?ver='.time()) }}"><i class="fa fa-eye"></i> View Document</a>
				    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Project Justification</th>
                            <td>
                                @if ($post_project->project_justification !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->project_justification) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>OCS Release</th>
                            <td>{{ $post_project->ocs_release }}</td>
                        </tr>
                        <tr>
                            <th>OMS Release</th>
                            <td>{{ $post_project->oms_release }}</td>
                        </tr>
                        <tr>
                            <th>Release Target</th>
                            <td>{{ $post_project->release_target }}</td>
                        </tr>
                        <tr>
                            <th>SRS Document</th>
                            <td>
                                @if ($post_project->srs_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->srs_document) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Justification Development</th>
                            <td>
                                @if ($post_project->justification_development !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->justification_development) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Buessiness Flow</th>
                            <td>{!! $post_project->business_flow !!}</td>
                        </tr>

                        <tr>
                            <th>Billing System</th>
                            <td>{!! $post_project->bill !!}</td>
                        </tr>

                        <tr>
                            <th>Provisioning System</th>
                            <td>{!! $post_project->prov !!}</td>
                        </tr>

                        <tr>
                            <th>Reporting</th>
                            <td>{!! $post_project->rep !!}</td>
                        </tr>

                        <tr>
                            <th>Development Schedule</th>
                            <td>{{ $post_project->development_schedule }}</td>
                        </tr>

                        <tr>
                            <th>UAT Schedule</th>
                            <td>{{ $post_project->uat_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Release Schedule</th>
                            <td>{{ $post_project->release_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Actual Release Schedule</th>
                            <td>{{ $post_project->actual_release_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Actual UAT Schedule</th>
                            <td>{{ $post_project->actual_uat_schedule }}</td>
                        </tr>
                        

                        <tr>
                            <th>Billing Offers</th>

                            <td>
                                @forelse ($post_project->projectOcsOffers as $ocs_offer)
                                    <ul>
                                        <li><a class="btn btn-info" href="{{ url('/offers/'.$ocs_offer->offer_id) }}"> <i class="fa fa-eye"></i> {{ $ocs_offer->offer_id }} : {{ ($ocs_offer->offer_name) ?: '' }}</a></li>
                                    </ul>
                                @empty
                                    This project does not have ocs offers.
                                @endforelse
                            </td>

                        </tr>

                        <tr>
                            <th>Status Project</th>
                            <td>
                                @if ($post_project->status_project == 'Pending')
                                    <span class="text-warning">
                                        {{ $post_project->status_project }}
                                    </span>
                                @elseif ($post_project->status_project == 'Development')
                                    <span class="text-info">
                                        {{ $post_project->status_project }}
                                    </span>
                                @elseif ($post_project->status_project == 'Drop')
                                    <span class="text-danger">
                                        {{ $post_project->status_project }}
                                    </span>
                                @elseif ($post_project->status_project == 'Release')
                                    <span class="text-success">
                                        {{ $post_project->status_project }}
                                    </span>
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Release Test</th>
                            <td>{{ $post_project->release_test }}</td>
                        </tr>

                        <tr>
                            <th>RFS Schedule</th>
                            <td>{{ $post_project->rfs_schedule }}</td>
                        </tr>

                        <tr>
                            <th>Justification Release</th>
                            <td>
                                @if ($post_project->justification_release !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->justification_release) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Justification RFS</th>
                            <td>
                                @if ($post_project->justification_rfs !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$post_project->justification_rfs) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Remark Project</th>
                            <td>{!! $post_project->remark_project !!}</td>
                        </tr>

                        <tr>
                            <th>Status Service</th>
                            <td>
                                @if ($post_project->status_service == 'Open')
                                    <span class="text-info">
                                        {{ $post_project->status_service }}
                                    </span>
                                @else 
                                    <span class="text-danger">
                                        {{ $post_project->status_service }}
                                    </span>
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Created at</th>
                            <td>{{ $post_project->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td>{{ $post_project->updated_at }}</td>
                        </tr>
                        
                    </table>

                    @can('update', $post_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/post-projects/'.$post_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')

    <script src="{{ URL::to('/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">

        $('#billing_offers').select2({
            'placeholder':'Choose OCS Offers...',
            tags:true,
            multiple: true,
        });

    </script>
@endsection
