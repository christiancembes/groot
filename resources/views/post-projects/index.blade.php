@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>TEST & READINESS</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Search Projects </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					{{-- add button --}}
					{{-- search fields --}}
					{!! Form::open(['url' => '/post-projects', 'method'=>'get'])!!}
	                        <div class="input-group">
                        		<input type="text" class="form-control" name="q" placeholder="Search Project Name...">
		                        <span class="input-group-btn">
		                        	<button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
		                    	</span>
	                    	</div>
                    {!! Form::close() !!}
                    
                    @include('layouts._show-all-data', ['url' => '/post-projects'])

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="cab">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List Item in Design & Develop Phase </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <center>
                        @if ($total_cab_project > 0)
                        <strong>
                            There are {{ $total_cab_project }} project still in Design & Develop Phase.
                        </strong>
                        @else
                        <strong>
                            There are no project still in Design & Develop Phase
                        </strong>
                        @endif
                    </center>

                    <br>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Development ID</th>
                            <th>Project Name</th>
                            <th>Phase</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@forelse($cab_projects as $dp)
                            <tr>
                                <td>{{ $dp->id }}</td>
                                <td>{{ $dp->development_id }}</td>
                                <td>{{ $dp->project_name }}</td>
                                <td>{{ $dp->phase }}</td>
                                <td>
                                    
                                    @if (Auth::check())
                                    <a class="btn btn-xs btn-warning" href="{{ url('/post-projects/'.$dp->development_id.'/create') }}"><i class="fa fa-edit"></i> Process to Test & Readiness</a>
                                    @endif
                                    
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">There are no project in cab phase.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <center>
	                    {{ $cab_projects->appends(compact('q'))->fragment('cab')->links() }}
	                </center>

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="post">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> List Item in Test & Readiness Phase </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <center>
                        @if ($total_post_project > 0)
                        <strong>
                            There are {{ $total_post_project }} project still in Test & Readiness Phase.
                        </strong>
                        @else
                        <strong>
                            There are no project in Test & Readiness phase
                        </strong>
                        @endif
                    </center>

                    <br>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Development ID</th>
                            <th>Project Name</th>
                            <th>Phase</th>
                            <th>Status Service</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($post_projects as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->development_id }}</td>
                                <td>{{ $post->project_name }}</td>
                                <td>{{ $post->phase }}</td>
                                <td>
                                    @if ($post->status_service == 'Open')
                                        <span class="text-info">
                                            {{ $post->status_service }}
                                        </span>
                                    @else 
                                        <span class="text-danger">
                                            {{ $post->status_service }}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    
                                    <a class="btn btn-xs btn-info" href="{{ url('/post-projects/'.$post->development_id) }}"><i class="fa fa-eye"></i> Details</a>
                                    
                                    @can('update', $post)
                                    <a class="btn btn-xs btn-warning" href="{{ url('/post-projects/'.$post->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                                    @endcan
                                    
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">There are no info Test & Readiness Phase.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <center>
                        {{ $post_projects->appends(compact('q'))->fragment('post')->links() }}
                    </center>

                </div>
            </div>
        </div>
    </div>
@endsection
