@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
                    <a href="/declare-projects">Plan & Define</a>
	            </li>
                <li class="active">
                    <strong>Details</strong>
                </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Plan & Define Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    
                    @can('update', $declare_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/declare-projects/'.$declare_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 200px">Development ID</th>
                            <td>{{ $declare_project->development_id }}</td>
                        </tr>
                        <tr>
                            <th>Project Name</th>
                            <td>{{ $declare_project->project_name }}</td>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <td>{{ $declare_project->category }}</td>
                        </tr>

                        @if ($declare_project->category == 'BAU' )
                        <tr>
                            <th>BAU OCS</th>
                            <td>{{ getBAUFormat($declare_project->bau_ocs) }}</td>
                        </tr>
                        <tr>
                            <th>BAU OMS</th>
                            <td>{{ getBAUFormat($declare_project->bau_oms) }}</td>
                        </tr>
                        @endif

                        <tr>
                            <th>Project Brief</th>
                            <td>{!! $declare_project->project_brief !!}</td>
                        </tr>
                        <tr>
                            <th>Revenue Projection</th>
                            <td>{!! $declare_project->revenue_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Subscriber Projection</th>
                            <td>{!! $declare_project->subscriber_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Traffic Projection</th>
                            <td>{!! $declare_project->traffic_projection !!}</td>
                        </tr>
                        <tr>
                            <th>Project Document</th>
                            <td>
                                @if ($declare_project->project_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$declare_project->project_document.'?ver='.time()) }}"><i class="fa fa-eye"></i> View Document</a>
				    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Project Justification</th>
                            <td>
                                @if ($declare_project->project_justification !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$declare_project->project_justification) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>OCS Release</th>
                            <td>{{ $declare_project->ocs_release }}</td>
                        </tr>
                        <tr>
                            <th>OMS Release</th>
                            <td>{{ $declare_project->oms_release }}</td>
                        </tr>
                        <tr>
                            <th>Release Target</th>
                            <td>{{ $declare_project->release_target }}</td>
                        </tr>
                        <tr>
                            <th>SRS Document</th>
                            <td>
                                @if ($declare_project->srs_document !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$declare_project->srs_document) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Justification Development</th>
                            <td>
                                @if ($declare_project->justification_development !== '')
                                    @if (Auth::check())
                                    <a target="_blank" class="btn btn-info" href="{{ asset('/documents/'.$declare_project->justification_development) }}"><i class="fa fa-eye"></i> View Document</a>
                                    @else
                                    <span class="text-danger">You must logged in to view this document</span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Created at</th>
                            <td>{{ $declare_project->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td>{{ $declare_project->updated_at }}</td>
                        </tr>
                        
                    </table>

                    
                    @can('update', $declare_project)
                    <div class="form-group">    
                        <a class="btn btn-warning" href="{{ url('/declare-projects/'.$declare_project->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    @endcan

                </div>
            </div>
        </div>
    </div>
@endsection
