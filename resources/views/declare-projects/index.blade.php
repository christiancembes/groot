@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>PLAN & DEFINE</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> Plan & Define </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					{{-- add button --}}
					{{-- search fields --}}
					{!! Form::open(['url' => '/declare-projects', 'method'=>'get'])!!}

                            @if (Auth::check())
							<div class="input-group">
								<a class="btn btn-warning" href="{{ url('/declare-projects/create') }}"><i class="fa fa-plus"></i> Add New Request</a>
							</div>
                            @endif
	                        <div class="input-group">
                        		<input type="text" class="form-control" name="q" placeholder="Search Project Name...">
		                        <span class="input-group-btn">
		                        	<button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
		                    	</span>
	                    	</div>
		                
                    {!! Form::close() !!}
                    
                    @include('layouts._show-all-data', ['url' => '/declare-projects'])

                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> List tem in Plan & Define Phase </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Development ID</th>
                            <th>Project Name</th>
                            <th>Phase</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@forelse($declare_projects as $dp)
                            <tr>
                                <td>{{ $dp->id }}</td>
                                <td>{{ $dp->development_id }}</td>
                                <td>{{ $dp->project_name }}</td>
                                <td>{{ $dp->phase }}</td>
                                <td>
                                    <a class="btn btn-xs btn-info" href="{{ url('/declare-projects/'.$dp->development_id) }}"><i class="fa fa-eye"></i> Details</a>
                                    
                                    @can('update', $dp)
                                    <a class="btn btn-xs btn-warning" href="{{ url('/declare-projects/'.$dp->development_id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                                    @endcan

                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">There are no Data Info.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <center>
	                    {{ $declare_projects->appends(compact('q'))->links() }}
	                </center>

                </div>
            </div>
        </div>
    </div>
@endsection
