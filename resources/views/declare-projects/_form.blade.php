@if ($declare_project->id != null)
    <div class="form-group">
        {!! Form::label('ID', 'Plan & Define ID') !!}
        {!! Form::text('id', $declare_project->id, ['class'=>'form-control', 'disabled']) !!}
    </div>
@endif

@if ($declare_project->development_id != null)
    <div class="form-group">
        {!! Form::label('development_id', 'Development ID') !!}
        {!! Form::text('development_id', $declare_project->development_id, ['class'=>'form-control', 'disabled']) !!}
    </div>
@endif

<div class="form-group {!! $errors->has('project_name') ? 'has-error' : '' !!}">
    {!! Form::label('project_name', 'Project Name') !!}

    @if ($declare_project->project_name !== null)
    {!! Form::text('project_name', null, ['class'=>'form-control', 'disabled']) !!}
    <input type="hidden" name="project_name" value="{{ ($declare_project->project_name) ?: '' }}">
    @else
    {!! Form::text('project_name', null, ['class'=>'form-control']) !!}
    @endif
    {!! $errors->first('project_name', '<p class="help-block">:message</p>') !!}
</div>

@if ($declare_project->id === null)
<div class="form-group {!! $errors->has('quartal') ? 'has-error' : '' !!}">
    {!! Form::label('quartal', 'Quartal Project') !!}
    {!! Form::select('quartal', [''=>'Select Category']+App\Models\DeclareProject::quartalList(), null, ['class'=>'form-control', 'id'=>'quartal-list']) !!}
    {!! $errors->first('quartal', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('project_year') ? 'has-error' : '' !!}">
    {!! Form::label('project_year', 'Year Project') !!}
    {!! Form::select('project_year', [''=>'Select Category']+App\Models\DeclareProject::yearList(), null, ['class'=>'form-control', 'id'=>'project-year-list']) !!}
    {!! $errors->first('project_year', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('bussiness_owner') ? 'has-error' : '' !!}">
    {!! Form::label('bussiness_owner', 'Bussiness Owner') !!}
    {!! Form::select('bussiness_owner', [''=>'Select Category']+App\Models\DeclareProject::bussinessOwnerList(), null, ['class'=>'form-control', 'id'=>'bussiness-owner-list']) !!}
    {!! $errors->first('bussiness_owner', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('project_type') ? 'has-error' : '' !!}">
    {!! Form::label('project_type', 'Project Type') !!}
    {!! Form::select('project_type', [''=>'Select Category']+App\Models\DeclareProject::projectTypeList(), null, ['class'=>'form-control', 'id'=>'project-type-list']) !!}
    {!! $errors->first('project_type', '<p class="help-block">:message</p>') !!}
</div>
@endif

<div class="form-group {!! $errors->has('category') ? 'has-error' : '' !!}">
    {!! Form::label('category', 'Category') !!}
    {!! Form::select('category', [''=>'Select Category']+App\Models\DeclareProject::categoryList(), null, ['class'=>'form-control', 'id'=>'project-category-list']) !!}
    {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
</div>

@if ($declare_project->category == 'BAU')
    <?php

    $tmp_ocs = explode('-', $declare_project->bau_ocs);
    $tmp_oms = explode('-', $declare_project->bau_oms);

    $bau_ocs_week = $tmp_ocs[2];
    $bau_ocs_month = $tmp_ocs[1];
    $bau_ocs_year = $tmp_ocs[0];

    $bau_oms_week = $tmp_oms[2];
    $bau_oms_month = $tmp_oms[1];
    $bau_oms_year = $tmp_oms[0];
    ?>
@else 
    <?php
    $bau_ocs_week = '';
    $bau_ocs_month = '';
    $bau_ocs_year = '';

    $bau_oms_week = '';
    $bau_oms_month = '';
    $bau_oms_year = '';
    ?>
@endif

<div class="bau_ocs hide">
    <div class="form-group {!! ($errors->has('bau_ocs_week') or $errors->has('bau_ocs_month') or $errors->has('bau_ocs_year')) ? 'has-error' : '' !!}">
        
        {!! Form::label('bau_ocs', 'BAU OCS') !!} <br>
        
        <div class="row">
            <div class="col-md-4">
                {!! Form::select('bau_ocs_week', [''=>'Select Week']+App\Models\DeclareProject::weekList(), $bau_ocs_week, ['class'=>'form-control week-list']) !!}    
            </div>
            <div class="col-md-4">
                {!! Form::select('bau_ocs_month', [''=>'Select Month']+App\Models\DeclareProject::monthList(), $bau_ocs_month, ['class'=>'form-control month-list']) !!}    
            </div>
            <div class="col-md-4">
                {!! Form::select('bau_ocs_year', [''=>'Select Year']+App\Models\DeclareProject::yearList(), $bau_ocs_year, ['class'=>'form-control year-list']) !!}    
            </div>
        </div>

        {!! $errors->first('bau_ocs_week', '<p class="help-block">:message</p>') !!}
        {!! $errors->first('bau_ocs_month', '<p class="help-block">:message</p>') !!}
        {!! $errors->first('bau_ocs_year', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="bau_oms hide">
    <div class="form-group {!! ($errors->has('bau_oms_week') or $errors->has('bau_oms_month') or $errors->has('bau_oms_year')) ? 'has-error' : '' !!}">
        
        {!! Form::label('bau_oms', 'BAU OMS') !!} <br>
        
        <div class="row">
            <div class="col-lg-4">
                {!! Form::select('bau_oms_week', [''=>'Select Week']+App\Models\DeclareProject::weekList(), $bau_oms_week, ['class'=>'form-control week-list']) !!}    
            </div>
            <div class="col-lg-4">
                {!! Form::select('bau_oms_month', [''=>'Select Month']+App\Models\DeclareProject::monthList(), $bau_oms_month, ['class'=>'form-control month-list']) !!}    
            </div>
            <div class="col-lg-4">
                {!! Form::select('bau_oms_year', [''=>'Select Year']+App\Models\DeclareProject::yearList(), $bau_oms_year, ['class'=>'form-control year-list']) !!}    
            </div>
        </div>

        {!! $errors->first('bau_oms_week', '<p class="help-block">:message</p>') !!}
        {!! $errors->first('bau_oms_month', '<p class="help-block">:message</p>') !!}
        {!! $errors->first('bau_oms_year', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {!! $errors->has('project_brief') ? 'has-error' : '' !!}">
    {!! Form::label('project_brief', 'Project Brief') !!}
        {!! Form::textarea('project_brief', null, ['class'=>'form-control']) !!}
    {!! $errors->first('project_brief', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('revenue_projection') ? 'has-error' : '' !!}">
    {!! Form::label('revenue_projection', 'Revenue Projection') !!}
        {!! Form::textarea('revenue_projection', null, ['class'=>'form-control']) !!}
    {!! $errors->first('revenue_projection', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('subscriber_projection') ? 'has-error' : '' !!}">
    {!! Form::label('subscriber_projection', 'Subscriber Projection') !!}
        {!! Form::textarea('subscriber_projection', null, ['class'=>'form-control']) !!}
    {!! $errors->first('subscriber_projection', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('traffic_projection') ? 'has-error' : '' !!}">
    {!! Form::label('traffic_projection', 'Traffic Projection') !!}
        {!! Form::textarea('traffic_projection', null, ['class'=>'form-control']) !!}
    {!! $errors->first('traffic_projection', '<p class="help-block">:message</p>') !!}
</div>

<div class="{!! $errors->has('ocs_release') ? 'has-error' : '' !!}" id="ocs_release">
    {!! Form::label('ocs_release', 'OCS Release') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('ocs_release', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('ocs_release', '<p class="help-block">:message</p>') !!}
</div>

<div class="{!! $errors->has('oms_release') ? 'has-error' : '' !!}" id="oms_release">
    {!! Form::label('oms_release', 'OMS Release') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('oms_release', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('oms_release', '<p class="help-block">:message</p>') !!}
</div>

<div class="{!! $errors->has('release_target') ? 'has-error' : '' !!}" id="release_target">
    {!! Form::label('release_target', 'Release Target') !!}
    <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {!! Form::text('release_target', null, ['class'=>'form-control']) !!}
    </div>
    {!! $errors->first('release_target', '<p class="help-block">:message</p>') !!}
</div>

<br>

<div class="form-group {!! $errors->has('project_document') ? 'has-error' : '' !!}">
    {!! Form::label('project_document', 'Project Document') !!}
    {!! Form::file('project_document') !!}
    {!! $errors->first('project_document', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {!! $errors->has('project_justification') ? 'has-error' : '' !!}">
    {!! Form::label('project_justification', 'Project Justification') !!}
    {!! Form::file('project_justification') !!}
    {!! $errors->first('project_justification', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {!! $errors->has('srs_document') ? 'has-error' : '' !!}">
    {!! Form::label('srs_document', 'SRS Document') !!}
    {!! Form::file('srs_document') !!}
    {!! $errors->first('srs_document', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {!! $errors->has('justification_development') ? 'has-error' : '' !!}">
    {!! Form::label('justification_development', 'Justification Development') !!}
    {!! Form::file('justification_development') !!}
    {!! $errors->first('justification_development', '<p class="help-block">:message</p>') !!}
</div>



@section('raw-script')

    <script src="{{ URL::to('/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">

        $('#quartal-list').select2({
            'placeholder':'Select Quartal',
        });

        $('#project-year-list').select2({
            'placeholder':'Select Project Year',
        });

        $('#project-type-list').select2({
            'placeholder':'Select Project Type',
        });

        $('#bussiness-owner-list').select2({
            'placeholder':'Select Bussiness Owner',
        });

        $('#project-category-list').select2({
            'placeholder':'Select Category',
        });

        // $('.week-list').select2({
        //     'placeholder':'Select Week',
        // });

        // $('.month-list').select2({
        //     'placeholder':'Select Month',
        // });

        // $('.year-list').select2({
        //     'placeholder':'Select Year',
        // });

        /**
         * Function for displaying form at declare project
         */
        $("#project-category-list").change(function() {

            var val = $("#project-category-list").val();

            if (val == "BAU" || val == "PROJECT" || val == "BPT") {
                $(".bau_ocs, .bau_oms").removeClass("hide");
            } else {
                $(".bau_ocs, .bau_oms").addClass("hide");
            }

        });
	

        /**
         * auto fill category value when form validation occured
         */ 
        $(document).ready(function() {

            var val = $("#project-category-list").val();

            if (val == "BAU") {
                $(".bau_ocs, .bau_oms").removeClass("hide");
            } else {
                $(".bau_ocs, .bau_oms").addClass("hide");
            }

        });
	

        $('#ocs_release .input-group.date, #oms_release .input-group.date, #release_target .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        var editor_config = {
            path_absolute : "/",
            selector: "textarea",
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
              if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
              });
            }
          };

          tinymce.init(editor_config);

    </script>
@endsection
