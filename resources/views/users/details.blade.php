@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
                    <a href="/users">Users</a>
                </li>
                <li class="active">
                    <strong>Details</strong>
                </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>POST Project Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="form-group">    
                        <a class="btn btn-info" href="{{ route('users.edit', $user->id)}}"><i class="fa fa-edit"></i> Edit</a>
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', $user->name, ['class'=>'form-control', 'disabled']) !!}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', $user->email, ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('roles', 'Roles') !!}
                        
                        {!! Form::select('roles[]',
                            ['0'=>'No Roles']+App\Models\Role::lists('name','id')->all(),
                            array_keys($user->roles()->lists('name','id')->toArray()),
                            ['id'=>'roles', 'class'=>'form-control', 'multiple', 'disabled']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('is_verified', 'Verified') !!}
                        {!! Form::text('is_verified', ($user->is_verified == 1) ? 'Yes' : 'No' , ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_active', 'Status') !!}
                        {!! Form::text('is_active', ($user->is_active == 1) ? 'Active' : 'Not Active' , ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_login', 'Last Login') !!}
                        {!! Form::text('last_login', $user->last_login, ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('avatar', 'Avatar') !!} <br>
                        @if ($user->avatar != null)
                            <img src="{{ $user->avatar }}" alt="" style="width: 150px;">
                        @else
                            {!! Form::text('avatar', 'Does not have avatar', ['class'=>'form-control', 'disabled']) !!}
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('created_at', 'Created At') !!}
                        {!! Form::text('created_at', $user->created_at, ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('updated_at', 'Updated At') !!}
                        {!! Form::text('updated_at', $user->updated_at, ['class'=>'form-control', 'disabled']) !!}
                    </div>

                    <div class="form-group">    
                        <a class="btn btn-info" href="{{ route('users.edit', $user->id)}}"><i class="fa fa-edit"></i> Edit</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')

    <script type="text/javascript">

        $('#roles').select2({
            'placeholder':'Select roles...',
            tags:true,
            multiple: true
        });

    </script>
@endsection