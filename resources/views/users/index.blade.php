@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Users</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All Users </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					
                    <h3>
                        <small>
                            <a href="{{ route('users.create') }}" class="btn btn-warning"> <i class="fa fa-plus-circle"></i> Tambah Users</a>
                        </small>
                    </h3>
                    
                    {!! Form::open(['url' => 'users', 'method'=>'get', 'class'=>'form-inline'])!!}
                        <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                            {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Cari berdasarkan nama...']) !!}
                            {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                        </div>
                        {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                    {!! Form::close() !!}

                    @include('layouts._show-all-data', ['url' => '/users'])

                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover white-bg">
                            <thead>
                                <tr>
                                    <th class="active">#ID</th>
                                    <th class="active">Name</th>
                                    <th class="active">Email</th>
                                    <th class="active">Status</th>
                                    <th class="active">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if ($user->is_active)
                                            <span class="text-info">
                                                Active
                                            </span>
                                        @else
                                            <span class="text-danger">
                                                Not Active
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        {!! Form::open(['url' => 'users/toogle-status', 'class' => 'form-inline', 'method' => 'post'] ) !!}
                                            
                                            <a class="btn btn-xs btn-success" href="{{ route('users.show', $user->id)}}"><i class="fa fa-eye"></i> Details</a>
                                            <a class="btn btn-xs btn-warning" href="{{ route('users.edit', $user->id)}}"><i class="fa fa-edit"></i> Edit</a>

                                            <input type="hidden" name="id" value="{{$user->id}}">

                                            @if ($user->is_active == 1)
                                                <input type="hidden" name="status" value="0">
                                                <button type="submit" class="btn btn-xs btn-danger js-submit-confirm" data-confirm-message="Are you sure to deactivate this account ?">
                                                    <i class="fa fa-trash"> Deactivate</i>
                                                </button>
                                            @elseif ($user->is_active == 0)
                                                <input type="hidden" name="status" value="1">
                                                <button type="submit" class="btn btn-xs btn-info js-submit-confirm" data-confirm-message="Are you sure to activate this account ?">
                                                    <i class="fa fa-check"> Activate</i>
                                                </button>
                                            @endif

                                        {!! Form::close()!!}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">Tidak ada data users.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')

    <script type="text/javascript">



    </script>

@endsection
