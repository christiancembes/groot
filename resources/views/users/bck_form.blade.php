@if ($user->id !== null)
<div class="form-group {!! $errors->has('id') ? 'has-error' : '' !!}">
	{!! Form::label('id', 'User ID') !!}
	{!! Form::text('id', $user->id, ['class'=>'form-control', 'disabled']) !!}
	{!! $errors->first('id', '<p class="help-block">:message</p>') !!}
</div>
@endif

<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', null, ['class'=>'form-control']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
	{!! Form::label('email', 'Email') !!}
	{!! Form::text('email', null, ['class'=>'form-control']) !!}
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
    {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class'=>'form-control']) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
    {!! Form::label('password_confirmation', 'Confirm Password') !!}
        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
	{!! Form::label('roles', 'Roles') !!}
	
	{!! Form::select('roles[]',
		App\Models\Role::lists('name','id')->all(),
		array_keys($user->roles()->lists('name','id')->toArray()),
		['id'=>'roles', 'class'=>'form-control', 'multiple']) !!}

</div>

<div class="form-group {!! $errors->has('is_verified') ? 'has-error' : '' !!}">
	{!! Form::label('is_verified', 'Verified Status') !!}
	{!! Form::select('is_verified', App\Models\User::verifiedStatusList(), null, ['id'=>'verified_list', 'class'=>'form-control']) !!}
	{!! $errors->first('is_verified', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('is_active') ? 'has-error' : '' !!}">
	{!! Form::label('is_active', 'Status') !!}
	{!! Form::select('is_active', App\Models\User::statusList(), null, ['id'=>'status_list', 'class'=>'form-control']) !!}
	{!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('avatar') ? 'has-error' : '' !!}">
	{!! Form::label('avatar', 'Avatar') !!}
	{!! Form::file('avatar') !!}
	{!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
</div>

@if ($user->avatar != null)
	<div class="form-group">
		<label for="items_serial_number">Current Avatar: </label>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
					<img src="{{ $user->avatar }}" style="width: 200px; height: 200px;">
				</div>
			</div>
		</div>
	</div>
@endif

@section('raw-script')

    <script type="text/javascript">

        $('#status_list').select2({
            'placeholder':'Pilih Status',
        });

        $('#verified_list').select2({
            'placeholder':'Pilih Verified Status',
        });

        $('#roles').select2({
            'placeholder':'Select roles...',
            tags:true,
            multiple: true
        });

    </script>
@endsection