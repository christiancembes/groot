<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>GROOT</title>

    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    @yield('raw-style')
    <style type="text/css">
        .table>tbody>tr>td, .table>thead>tr>th{
            vertical-align: middle;
        }
    </style>

</head>

<body>
    <div id="wrapper">

        @include('layouts._sidebar')

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        
                        @if (Auth::guest())
                            <li>
                                <a href="{{url('/login')}}">
                                    <i class="fa fa-sign-in"></i> Login
                                </a>
                            </li>
                        @else
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user"></i>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInDown">
                                    <li>
                                        <a href="{{url('/logout')}}">
                                            <div>
                                                <i class="fa fa-sign-out"></i>
                                                Log out
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                    </ul>

                </nav>
            </div>

            @yield('breadcrumb')

            <div class="wrapper-content">

                @include('layouts._flash')
                @yield('content')

            </div>
            

            <div class="footer">
                <div class="pull-right">
                    {{-- 10GB of <strong>250GB</strong> Free. --}}
                </div>
                <div>
                    <strong>Copyright</strong> GROOT &copy; 2018
                </div>
            </div>
        </div>
    
    </div>

    <!-- Mainly scripts -->
    <script src="{{ elixir('js/app.js') }}"></script>
    
    @if (session()->has('toastr.message'))
        <script>
            toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "progressBar": true,
                  "preventDuplicates": true,
                  "positionClass": "toast-top-right",
                  "onclick": null,
                  "showDuration": "100",
                  "hideDuration": "1000",
                  "timeOut": "2000",
                  "extendedTimeOut": "100",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
            }
            toastr.info("{{ session()->get('toastr.message')  }}", "{{ session()->get('toastr.title')  }}")
        </script>
    @endif

    @yield('raw-script')

</body>
</html>
