<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <h3 style="margin-top: -15px;">
                        <a href="/" title="">GROOT</a>
                    </h3>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ (request()->path() == 'dashboards') ? 'landing_link' : '' }}">
                <a {{-- target="_blank"  --}} href="{{url('/dashboards')}}">
                <i class="fa fa-th-large"></i>
                <span class="nav-label">Dashboard</span>
            </a>
            </li>
            <li class="{{ (request()->path() == 'calendars') ? 'landing_link' : '' }}">
                <a href="{{url('/calendars')}}">
                    <i class="fa fa-calendar"></i>
                    <span class="nav-label">Calendar </span>
                    {{-- <span class="label label-info pull-right">62</span> --}}
                </a>
            </li>
            {{-- <li class="{{ (request()->path() == 'find-me') ? 'landing_link' : '' }}"> --}}
            {{--    <a href="{{url('/find-me')}}"> --}}
            
		<li class="{{ ( str_contains(request()->fullUrl(), 'find-me') OR str_contains(request()->fullUrl(), 'check-profile')) ? 'active' : '' }}">
                    <a href="#">
	            <i class="fa fa-search"></i>
                    <span class="nav-label">Find Me </span>
		    <span class="fa arrow"></span>
                </a>
            	<ul class="nav nav-second-level">
                        <li class="{{ ( str_contains(request()->fullUrl(), 'find-me') ) ? 'landing_link' : '' }}"><a href="{{ url('/find-me') }}">Service & Product</a></li>
			@if (Auth::check())
                        <li class="{{ ( str_contains(request()->fullUrl(), 'check-profile') ) ? 'landing_link' : '' }}"><a href="{{ url('/check-profile') }}">Profile</a></li>
                        @endif

                    </ul>
	    </li>
            
                <li class="{{ ( str_contains(request()->fullUrl(), 'declare-projects') OR str_contains(request()->fullUrl(), 'cab-projects') OR str_contains(request()->fullUrl(), 'post-projects') OR str_contains(request()->fullUrl(), 'activity') ) ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Projects</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="{{ ( str_contains(request()->fullUrl(), 'declare-projects') ) ? 'landing_link' : '' }}"><a href="{{ url('/declare-projects') }}">Plan & Define</a></li>
                        @if (Auth::check())
                        <li class="{{ ( str_contains(request()->fullUrl(), 'cab-projects') ) ? 'landing_link' : '' }}"><a href="{{ url('/cab-projects') }}">Design & Develop</a></li>
                        <li class="{{ ( str_contains(request()->fullUrl(), 'post-projects') ) ? 'landing_link' : '' }}"><a href="{{ url('/post-projects') }}">Test & Readiness</a></li>
                        <li class="{{ ( str_contains(request()->fullUrl(), 'activity')) ? 'landing_link' : '' }}"><a href="{{ url('/activity') }}">Activity</a></li>
			@endif
                        
                    </ul>
                </li>
                @role('admin')
                <li class="{{ str_contains(request()->fullUrl(), 'compare')  ? 'active' : '' }}">
                    <a href="{{url('/compare')}}">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Compare</span>
                    </a>
                </li>
                <li class="{{ (request()->path() == 'users') ? 'landing_link' : '' }}">
                    <a href="{{url('/users')}}">
                        <i class="fa fa-gear"></i>
                        <span class="nav-label">Manage Users </span>
                        {{-- <span class="label label-info pull-right">62</span> --}}
                    </a>
                </li>
                @endrole

            
        </ul>

    </div>
</nav>
