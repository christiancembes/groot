@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <a href="{{ url('/activity') }}"><strong>Activity</strong></a>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
	<div class="row" id="row-activity">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All Requests</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered" id="table-activity">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Request ID</th>
                            <th class="text-center">Request Name</th>
                            <th class="text-center">Monitoring</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($cab_projects as $cab)
                            <tr>
                                <td class="text-center">{{ $counter++ }}</td>
                                <td class="text-center">{{ $cab->development_id }}</td>
                                <td class="text-center">{{ $cab->project_name }}</td>
                                <td class="text-center">
                                    <a class="btn btn-outline btn-primary dim" href="{{ url("/activity/".$cab->development_id) }}"> <i class="fa fa-desktop"></i></a>
                                </td>
                                <td class="text-center"><span class="text-info">{{ $cab->status_project }}</span></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">There are no cab project data.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

