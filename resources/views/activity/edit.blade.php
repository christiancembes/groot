@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
                <li class="active">
                    <a href="{{ url('/activity') }}">Activity</a>
                </li>
	            <li class="active">
                    <a href="{{ url('/monitoring') }}"><strong>Monitoring Progress</strong></a>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Activity of <strong>{{ $activity_row->cab_project_id }} </strong></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!! Form::model($activity_row, ['url' => url('activity/process')]) !!}
                        
                        <div class="row">
                            {!! Form::hidden('id', $activity_row->id, ['class'=>'form-control']) !!}
                        </div>
                        <div class="row">
                            {!! Form::hidden('cab_project_id', $activity_row->cab_project_id, ['class'=>'form-control']) !!}
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">            
                                    {!! Form::label('activity_name', 'Activity Name*', ['class'=>'col-sm-4','control-label']) !!}
                                    <div class="col-sm-8">{!! Form::text('activity_name', $activity_row->activity_name, ['class'=>'form-control']) !!}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('weight', 'Activity Score (of 100%)*', ['class'=>'col-sm-4','control-label']) !!}
                                    <div class="col-sm-8">{!! Form::text('weight', $activity_row->weight, ['class'=>'form-control']) !!}</div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('target_start_date', 'Target Start Date*', ['class'=>'col-sm-4','control-label']) !!}
                                    <div class="col-sm-8" id="target_start_date">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            {!! Form::text('target_start_date', $activity_row->target_start_date, ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('target_end_date', 'Target End Date*', ['class'=>'col-sm-4', 'control-label']) !!}
                                    <div class="col-sm-8" id="target_end_date">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            {!! Form::text('target_end_date', $activity_row->target_end_date, ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('actual_start_date', 'Actual Start Date*', ['class'=>'col-sm-4', 'control-label']) !!}
                                    <div class="col-sm-8" id="actual_start_date">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            {!! Form::text('actual_start_date', $activity_row->actual_start_date, ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('actual_end_date', 'Actual End Date*', ['class'=>'col-sm-4', 'control-label']) !!}
                                    <div class="col-sm-8" id="actual_end_date">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            {!! Form::text('actual_end_date', $activity_row->actual_end_date, ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('status', 'Status*', ['class'=>'col-sm-2', 'control-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::select('status', array('open' => 'Open', 'close' => 'Close'), $activity_row->status, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('remarks', 'Remarks*', ['class'=>'col-sm-2', 'control-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('remarks', $activity_row->remarks, ['class'=>'form-control' ,'rows' => 3]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-10">
                                    {!! Form::submit('Update', ['class' => 'btn btn-primary', 'name'=>'submit']) !!}
                                    {!! Form::reset('Reset', ['class' => 'btn btn-white']) !!}
                                </div>
                            </div>
                        </div>
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('raw-script')
    <script type="text/javascript">
    $(function(){
        $('#target_start_date .input-group.date, #target_end_date .input-group.date, #actual_start_date .input-group.date, #actual_end_date .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    }); 
    </script>
@endsection

