<div class="row">
    {!! Form::hidden('cab_project_id', $cab_project->development_id, ['class'=>'form-control']) !!}
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">            
            {!! Form::label('activity_name', 'Activity Name*', ['class'=>'col-sm-4','control-label']) !!}
            <div class="col-sm-8">{!! Form::text('activity_name', '', ['class'=>'form-control']) !!}</div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('weight', 'Activity Score (of 100%)*', ['class'=>'col-sm-4','control-label']) !!}
            <div class="col-sm-8">{!! Form::text('weight', '', ['class'=>'form-control']) !!}</div>
        </div>
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('target_start_date', 'Target Start Date*', ['class'=>'col-sm-4','control-label']) !!}
            <div class="col-sm-8" id="target_start_date">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    {!! Form::text('target_start_date', '', ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('target_end_date', 'Target End Date*', ['class'=>'col-sm-4', 'control-label']) !!}
            <div class="col-sm-8" id="target_end_date">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    {!! Form::text('target_end_date', '', ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('actual_start_date', 'Actual Start Date*', ['class'=>'col-sm-4', 'control-label']) !!}
            <div class="col-sm-8" id="actual_start_date">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    {!! Form::text('actual_start_date', '', ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('actual_end_date', 'Actual End Date*', ['class'=>'col-sm-4', 'control-label']) !!}
            <div class="col-sm-8" id="actual_end_date">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    {!! Form::text('actual_end_date', '', ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('status', 'Status*', ['class'=>'col-sm-2', 'control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('status', array('open' => 'Open', 'close' => 'Close'), 'open', ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('remarks', 'Remarks*', ['class'=>'col-sm-2', 'control-label']) !!}
            <div class="col-sm-10">
                {!! Form::textarea('remarks', '', ['class'=>'form-control' ,'rows' => 3]) !!}
            </div>
        </div>
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="row">
    <div class="form-group">
        <div class="col-sm-2 col-sm-offset-10">
            {!! Form::submit('Save', ['class' => 'btn btn-primary', 'name'=>'submit']) !!}
            {!! Form::reset('Reset', ['class' => 'btn btn-white']) !!}
        </div>
    </div>
</div>
