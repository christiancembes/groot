@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
                <li class="active">
                    <a href="{{ url('/activity') }}">Activity</a>
                </li>
	            <li class="active">
                    <a href="{{ url('/monitoring') }}"><strong>Monitoring Progress</strong></a>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Define Activity of <strong>{{ $cab_project->development_id }} : {{$cab_project->project_name}}</strong></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!! Form::model($new_activity, ['url' => url('activity/process')]) !!}
                        
                        @include('activity._form', ['model' => $new_activity]) 
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="row-activity">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Activities of <strong>{{$cab_project->development_id}} : {{$cab_project->project_name}}</strong></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-activity">
                            <thead>
                            <tr>
                                <th class="text-center" rowspan="2">#</th>
                                <th class="text-center" rowspan="2">Activity</th>
                                <th class="text-center" rowspan="2">Weight</th>
                                <th class="text-center" colspan="2">Start Date</th>
                                <th class="text-center" colspan="2">End Date</th>
                                <th class="text-center" rowspan="2">Status</th>
                                <th class="text-center" rowspan="2">Action</th>
                                <th class="text-center" rowspan="2">Remarks</th>
                            </tr>
                            <tr>
                                <th class="text-center">Target Date</th>
                                <th class="text-center">Actual Date</th>
                                <th class="text-center">Target Date</th>
                                <th class="text-center">Actual Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($cab_activities as $cab)
                                <tr>
                                    <td class="text-center">{{ $counter++ }}</td>
                                    <td class="text-center">{{ $cab->activity_name }}</td>
                                    <td class="text-center">{{ $cab->weight }}</td>
                                    <td class="text-center">{{ $cab->target_start_date }}</td>
                                    <td class="text-center">{{ $cab->actual_start_date }}</td>
                                    <td class="text-center">{{ $cab->target_end_date }}</td>
                                    <td class="text-center">{{ $cab->target_end_date }}</td>
                                    <td class="text-center">{{ $cab->status }}</td>
                                    <td class="text-center">
                                        <div style="display:inline-block; vertical-align: middle;">
                                        <a class="btn btn-outline btn-primary dim" href="{{ url("/activity/".$cab->id."/edit") }}"><i class="fa fa-edit"></i></a>
                                        {!! Form::open(['url' => url('activity/delete'), 'class' => 'form-inline', 'style' => 'margin:0;padding:0;']) !!}
                                        {!! Form::hidden('id', $cab->id); !!}
                                        <button class="btn btn-outline btn-primary dim" style="margin:0;"> <i class="fa fa-remove"></i></button>                                
                                        {!! Form::close() !!}
                                        </div>    
                                    </td>
                                    <td class="text-center">{{ $cab->remarks }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" align='center'>There are no activity data.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('raw-script')
    <script type="text/javascript">
    $(function(){
        $('#target_start_date .input-group.date, #target_end_date .input-group.date, #actual_start_date .input-group.date, #actual_end_date .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    }); 
    </script>
@endsection

