<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	Dear Mr/Mrs {{ $user->name }},
	<br>
	<br>
	Thank you for register at <a href="{{url('/')}}">{{url('/')}}</a>
	<br>
	<br>

	Klik this link to activate your account.
	<br>
	<a href="{{ $link = env('APP_URL').'/auth/verify/'.$token.'?email='.urlencode($user->email) }}"> {{ $link }} </a>
	<br>
	<br>
	<br>
	Terimakasih, <br>
	Zoro
</body>
</html>
