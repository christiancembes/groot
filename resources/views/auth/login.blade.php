@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default animated fadeInDown">
                <div class="panel-heading">Login</div>
                <div class="panel-body">

                    {!! Form::open(['url' => 'login', 'class' => 'form-horizontal']) !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('username', 'LDAP User', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('username', null, ['class'=>'form-control']) !!}
                                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::label('password', 'Password', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password', ['class'=>'form-control']) !!}
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('remember')!!} Remember me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                  <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot password?</a>
                            </div>
                        </div>
                        
                        {{-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                atau login dengan
                                <br><br>
                                <div class="btn btn-primary">
                                    <a style="color: white; text-decoration: none;" href="/auth/facebook">
                                        <i class="fa fa-facebook-official"></i>
                                        Facebook
                                    </a>
                                </div>
                                <div class="btn btn-danger">
                                    <a style="color: white; text-decoration: none;" href="/auth/google">
                                        <i class="fa fa-google"></i>
                                        <!-- <i class="fa fa-google-plus-square"></i> -->
                                        Google
                                    </a>
                                </div>
                                <div class="btn btn-info">
                                    <a href="/auth/twitter">
                                        <i class="fa fa-twitter"></i>
                                        Twitter
                                    </a>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <br>
                                Dont have an account?
                                <br>
                                <a class="" href="{{ url('/register') }}" style="font-size: 15px;">
                                    <i class="fa fa-user-plus"></i> Register
                                </a>
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
