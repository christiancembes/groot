<div class="modal fade" id="solution" tabindex="-1" role="dialog" aria-labelledby="solution">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Solution</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Solution</th>
                    <th class="success">Package</th>
                    <th class="success">Briefing</th>
                    <th class="success">AppsID Charge</th>
		    <th class="success">Amount</th>
		    <th class="success">Validity</th>
		    <th class="success">Type Package</th>
		    <th class="success">Type Solution</th>
		    <th class="success">Subtype Solution</th>
                </tr>
                {{-- @forelse($solution as $solution) --}}
                <tr>
                    @if ($solution != '')
                    <td>{{ ($solution->solution != null) ?  $solution->solution : 'No Data'}}</td>
		    <td>{{ ($solution->package != null) ?  $solution->package : 'No Data'}}</td>
		    <td>{{ ($solution->brief != null) ?  $solution->brief : 'No Data'}}</td>
		    <td>{{ ($solution->vas_code != null) ?  $solution->vas_code : 'No Data'}}</td>
		    <td>{{ ($solution->amount != null) ?  $solution->amount : 'No Data'}}</td>
		    <td>{{ ($solution->Validity != null) ?  $solution->validity : 'No Data'}}</td>
		    <td>{{ ($solution->type_package != null) ?  $solution->type_pacakge : 'No Data'}}</td>
		    <td>{{ ($solution->type_solution != null) ?  $solution->type_solution : 'No Data'}}</td>
		    <td>{{ ($solution->subtype_solution != null) ?  $solution->subtype_solution : 'No Data'}}</td>
		    @else
                    <td>No Data</td>
                    @endif   
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
