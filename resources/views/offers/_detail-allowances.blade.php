<div class="modal fade" id="ocs_allowance" tabindex="-1" role="dialog" aria-labelledby="ocs_allowance">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Charges</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Offer Name</th>
                    <th class="success">Package ID</th>
 		    <th class="success">Package Name</th>
		    <th class="success">Param Attribute Name</th>
                    <th class="success">Param Attribute Dimension</th>
                    <th class="success">Priority</th>
                    <th class="success">Item Name</th>
                    <th class="success">Item Type</th>
                    <th class="success">Item Type ID</th>
                    <th class="success">Role</th>
                    <th class="success">UOM</th>
                    <th class="success">Attribute Value</th>
                </tr>
                @forelse($ocs_allowance as $allowance)
                <tr>

                    <td>{{ $allowance->offer_name }}</td>
                    <td>{{ $allowance->package_id }}</td>
                    <td>{{ $allowance->package_name }}</td>
                    <td>{{ $allowance->item_param_attr_name }}</td>
                    <td>{{ $allowance->item_param_attr_dimension }}</td>
                    <td>{{ $allowance->priority }}</td>
                    <td>{{ $allowance->item_name }}</td>
                    <td>{{ $allowance->item_type }}</td>
                    <td>{{ $allowance->item_type_id }}</td>
                    <td>{{ $allowance->item_role }}</td>
                    <td>{{ $allowance->item_param_attr_uom }}</td>
                    <td>{{ $allowance->item_param_attr_value }}</td>
                 </tr>
                 @empty
                 <tr>
                      <td colspan="8">No data</td>
                 </tr>
                 @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
