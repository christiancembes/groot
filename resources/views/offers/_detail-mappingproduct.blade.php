<div class="modal fade" id="mapping_product" tabindex="-1" role="dialog" aria-labelledby="mapping_product">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Mapping Product</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Group ID</th>
                    <th class="success">Group Name</th>
                    <th class="success">Description</th>
                    <th class="success">Kategori</th>
                    <th class="success">Marketing Name</th>
                   
                    
                    
                  
                </tr>
                {{-- @forelse($solution as $solution) --}}
                <tr>
                    @if ($mapping_product != '')
                    <td>{{ ($mapping_product->group_id != null) ?  $mapping_product->group_id : 'No Data'}}</td>
                    <td>{{ ($mapping_product->group_name != null) ?  $mapping_product->group_name : 'No Data'}}</td>
                    <td>{{ ($mapping_product->description != null) ?  $mapping_product->description : 'No Data'}}</td>
                    <td>{{ ($mapping_product->kategori != null) ?  $mapping_product->kategori : 'No Data'}}</td>
                    <td>{{ ($mapping_product->marketing_name != null) ?  $mapping_product->marketing_name : 'No Data'}}</td>

                   {{--  <td>{{ ($oms_offer->apn != null) ? $oms_offer->apn : 'No Data'}}</td>
                    <td>{{ ($oms_offer->upcc != null) ?  $oms_offer->upcc : 'No Data'}}</td>
                    <td>{{ ($oms_offer->rim != null) ? $oms_offer->rim : 'No Data'}}</td> --}}
                    @else
                    <td> colspan="8">No Data</td>
                    @endif
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
