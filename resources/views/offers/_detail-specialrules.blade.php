<div class="modal fade" id="specialrules" tabindex="-1" role="dialog" aria-labelledby="Special Rules">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Special Rules</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr class="info">
                    <th rowspan="2"><center>Restriction</center></th>
                    <th colspan="2"><center>Prerequisite</center></th>
                   {{--  <th rowspan="2"><center>Price Plan</center></th>--}}
                    <th rowspan="2"><center>Dependent</center></th>
                    <th rowspan="2"><center>Seamless</center></th>
                    <th rowspan="2"><center>Seamless Group</center></th>
                </tr>
                <tr >
                    <th class="danger"><center>Criteria Prerequisite</center></th>
                    <th class="danger"><center>Offer Prerequisite</center></th>
                </tr>
		<tr>
		     <td>
                        <ul>
                        @forelse ($restrictions as $rst)
                        <li>{{ $rst->offer_id_restriction }}:{{ $rst->offer_name_restriction }}</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @forelse ($prerequisites as $prs)
                        <li>{{ $prs->criteria_prerequisite }}</li>
                        @empty
                        <li>No Data</li>
			@endforelse
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @forelse ($prerequisites as $prs)
                        <li>{{ $prs->offer_id_prerequisite }}</li>
                        <li>{{ $prs->offer_name_prerequisite }}</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
                 {{--   <td>
                        <ul>
                        @forelse ($priceplan as $pp)
                        <li>{{ $pp->price_plan }} : {{ $pp->price_plan_type }}</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td> --}}
                    <td>
                        <ul>
                        @forelse ($dependent as $dp)
                        <li>{{ $dp->dependent_offer_id }} : {{ $dp->dependent_offer_name }} : {{ $dp->action }}</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @forelse ($seamless as $sm)
                        <li>{{ $sm->seamless_offer_id }} : {{ $sm->seamless_offer_name }} : {{ $sm->x_days_number }}Hari</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @forelse ($seamlessgroup as $smg)
                        <li>{{ $smg->group_desc }} </li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
               
		 </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
