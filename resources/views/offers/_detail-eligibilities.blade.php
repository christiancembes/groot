<div class="modal fade" id="eligibility" tabindex="-1" role="dialog" aria-labelledby="eligibility">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Elegibility</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr class="info">
		    <th rowspan="1" style="width: 80px;"><center>Sales Channel</center></th>
                    <th rowspan="1" style="width: 80px;"><center>User CRM</center></th>
                    <th rowspan="1" style="width: 80px;"><center>Customer Type</center></th>
                    <th rowspan="1" style="width: 80px;"><center>Subtype</center></th>
                    <th rowspan="1"><center>Price Plan</center></th>
		</tr>
                <tr>
		    <td>
                        <ul>
                        @foreach (explode('|', $oms_offer->sales_channel) as $sc)
                          
		 	  @php 
			  switch($sc){
			   	case "WI":	$sc = "Walkin/Grapari";	break;
			   	case "CC":	$sc = "Call Center";	break;
				case "RA":	$sc = "RAM Corporate";  break;
				case "SA":	$sc = "SAM Corporate";  break;
				case "UM":	$sc = "UMB"; 		break;
				case "SM";	$sc = "SMS";		break;
				case "OD";	$sc = "OD/App Client";	break;
				case "TC";	$sc = "Telkomsel Care"; break;
				case "IV";	$sc = "IVR";		break;
				case "MS";	$sc = "Batch/MEO";	break;
				case "CO";	$sc = "Cobrand App";	break;
			   	case "CM": 	$sc = "Campaign"; 	break;
				case "AC";	$sc = "Auto Cancel";	break;
				case "TR";	$sc = "Third Party";	break;
				case "IPTV";	$sc = "IP TV";		break;
				case "COM";	$sc = "App CUG";	break;
				default: $sc = $sc;
			  }
			  @endphp
			

			    @if ($sc != '')
                            <li>{{ $sc }}</li>
                            @endif
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @foreach (explode('|', $oms_offer->user_type) as $ut)
                            @if ($ut != '')
                            <li>{{ $ut }}</li>
                            @endif
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @foreach (explode('|', $oms_offer->customer_type) as $ct)
                        
			@php
                          switch($ct){
                                case "O":      $ct = "Corporate"; break;
                                case "S":      $ct = "Consumer";    break;
                                case "K":      $ct = "Telkomsel";  break;
                                default: $ct = $ct;
                          }
                          @endphp
    
			    @if ($ct != '')
                            <li>{{ $ct }}</li>
                            @endif
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @foreach (explode('|', $oms_offer->sub_type) as $st)
                        @php
                          switch($st){
                                case "Z":      $st = "Strategic"; 		break;
                                case "I":      $st = "Regular";    		break;
                                case "C":      $st = "Enterprise";  		break;
				case "M";      $st = "SME";			break;	
				case "E";      $st = "Dinas Pegawai ";		break;
				case "N";      $st = "Test";			break;
                                default: $st = $st;
   			}
			@endphp
 
			    @if ($st != '')
                            <li>{{ $st }}</li>
                            @endif
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                        @forelse ($priceplan as $pp)
                        <li>{{ $pp->price_plan }} : {{ $pp->price_plan_type }}</li>
                        @empty
                        <li>No Data</li>
                        @endforelse
                        </ul>
                    </td>
                    
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
