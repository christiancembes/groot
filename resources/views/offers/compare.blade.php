@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Product Compare</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }
    .serc{
        padding-left: 0;
    }
    .searchdrop{
        display: none; 
        position: absolute;
        background-color: #ffffff;
        box-shadow: 1px 1px 1px;
        z-index: 4;
        width: 79%;
    }
    .searchitem{
        padding: 6px 4px;
        border-bottom: 1px solid #e7eaec;
        border-left: 1px solid #e7eaec;
        border-right: 1px solid #e7eaec;
    }
</style>
@endsection

@section('content')
	
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Compare Product</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content clearfix">
                {!! Form::open(['url' => '/compare', 'method'=>'post', 'class' => 'form-inline', 'id' => 'formoffer'] ) !!}
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Offer Search</label>
                    </div>
                </div>
                <div class="col-lg-3 ">
                    <div class="form-group">
                        <input type="text" value="{{ $ocs[0]->offer_name or '' }}" {{ (empty($offer1)) ? '' : 'readonly' }} placeholder="Offer.." class="form-control offerfield" />
                        <div class="searchdrop"></div>

                        {!! Form::hidden('offer1', $offer1, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-3 ">
                    <div class="form-group serc">
                        <input type="text" value="{{ $ocs[1]->offer_name or '' }}" {{ (empty($offer2)) ? '' : 'readonly' }}  placeholder="Offer.." class="form-control offerfield" />
                        <div class="searchdrop"></div>
                        {!! Form::hidden('offer2', $offer2, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-3 ">
                    <div class="form-group serc">
                        <input type="text" value="{{ $ocs[2]->offer_name or '' }}" {{ (empty($offer3)) ? '' : 'readonly' }}  placeholder="Offer.." class="form-control offerfield" />
                        <div class="searchdrop"></div>
                        {!! Form::hidden('offer3', $offer3, ['class'=>'form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Billing Details</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                @if($ocs !== '')                    
                    <table class="table table-bordered" id="table-activity">
                        <thead>
                            <tr>
                                <th class="text-center col-lg-3">Comparation</th>
                                <th class="text-center col-lg-3">{{ $ocs[0]->offer_name or 'Product 1' }}</th>
                                <th class="text-center col-lg-3">{{ $ocs[1]->offer_name or 'Product 2' }}</th>
                                <th class="text-center col-lg-3">{{ $ocs[2]->offer_name or 'Product 3' }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Info</td>
                                <td class="text-center">{{ $ocs[0]->charge or '-' }}</td>
                                <td class="text-center">{{ $ocs[1]->charge or '-' }}</td>
                                <td class="text-center">{{ $ocs[2]->charge or '-' }}</td>
                            </tr>
                            <tr>
                                <td class="text-center">Allowance</td>
                                <td class="text-center">{{ $ocs[0]->allowance or '-' }}</td>
                                <td class="text-center">{{ $ocs[1]->allowance or '-' }}</td>
                                <td class="text-center">{{ $ocs[2]->allowance or '-' }}</td>
                            </tr>
                            <tr>
                                <td class="text-center">Kriteria</td>
                                <td class="text-center">{{ $ocs[0]->criteria or '-' }}</td>
                                <td class="text-center">{{ $ocs[1]->criteria or '-' }}</td>
                                <td class="text-center">{{ $ocs[2]->criteria or '-' }}</td>
                            </tr>
                        </tbody>
                    </table>
                @else
                    <div colspan="10" align='center'>There are no offer data.</div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ordering Details</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                @if($oms !== '')                    
                    <table class="table table-bordered" id="table-activity">
                        <thead>
                            <tr>
                                <th class="text-center col-lg-3">Comparation</th>
                                <th class="text-center col-lg-3">{{ $oms[0]->offer_name or 'Product 1' }}</th>
                                <th class="text-center col-lg-3">{{ $oms[1]->offer_name or 'Product 2' }}</th>
                                <th class="text-center col-lg-3">{{ $oms[2]->offer_name or 'Product 3' }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Sales Channel</td>
                                <td>
                                    <ul>
                                    @if ($oms[0] != '')
                                        @foreach (explode(',', $oms[0]->sales_channel) as $ut)
                                            @if ($ut != '')
                                            <li>{{ $ut }}</li>
                                            @endif
                                        @endforeach
                                    @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                    @if ($oms[1] != '')
                                    @foreach (explode(',', $oms[1]->sales_channel) as $ut)
                                        @if ($ut != '')
                                        <li>{{ $ut }}</li>
                                        @endif
                                    @endforeach
                                    @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                    @if ($oms[2] != '')
                                    @foreach (explode(',', $oms[2]->sales_channel) as $ut)
                                        @if ($ut != '')
                                        <li>{{ $ut }}</li>
                                        @endif
                                    @endforeach
                                    @endif
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Customer Type</td>
                                <td>
                                    <ul>
                                    @if ($oms[0] != '')
                                        @foreach (explode(',', $oms[0]->customer_type) as $ut)
                                            @if ($ut != '')
                                            <li>{{ $ut }}</li>
                                            @endif
                                        @endforeach
                                    @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                    @if ($oms[1] != '')
                                    @foreach (explode(',', $oms[1]->customer_type) as $ut)
                                        @if ($ut != '')
                                        <li>{{ $ut }}</li>
                                        @endif
                                    @endforeach
                                    @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                    @if ($oms[2] != '')
                                    @foreach (explode(',', $oms[2]->customer_type) as $ut)
                                        @if ($ut != '')
                                        <li>{{ $ut }}</li>
                                        @endif
                                    @endforeach
                                    @endif
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">User Role</td>
                                <td>
                                    <ul>
                                        @if ($oms[0] != '')
                                        @foreach (explode(',', $oms[0]->user_type) as $ut)
                                            @if ($ut != '')
                                            <li>{{ $ut }}</li>
                                            @endif
                                        @endforeach
                                        @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                    @if ($oms[1] != '')
                                        @foreach (explode(',', $oms[1]->user_type) as $ut)
                                            @if ($ut != '')
                                            <li>{{ $ut }}</li>
                                            @endif
                                        @endforeach
                                        @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                        @if ($oms[2] != '')
                                        @foreach (explode(',', $oms[2]->user_type) as $ut)
                                            @if ($ut != '')
                                            <li>{{ $ut }}</li>
                                            @endif
                                        @endforeach
                                        @endif
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @else
                    <div colspan="10" align='center'>There are no offer data.</div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('raw-script')
<script type="text/javascript">
$('.offerfield').keyup(function(){
        var x = $(this).val();
        var temp =  $(this);
        $(this).next().empty();
        if (x.length > 3) {
            $.post(
            '{{ url('/compare/search') }}',
            { key: x, _token: '{{ csrf_token() }}'},
                function(data){
                    if(data) {
                        $.each(data, function(index, element) {
                            temp.next().append("<div class='searchitem' value='"+element.offer_id+"'>"+element.offer_name+"<div>");
                        });
                        temp.next().slideDown();
                    }
                }
            );         
        }   
    });

$('.searchdrop').on('click', '.searchitem', function(){
    var offern = $(this).text();var offerid = $(this).attr('value');
    $(this).parent().next().val(offerid);
    $(this).parent().prev().val(offern);
    $(this).parent().slideUp();
    $('#formoffer').submit();
});
</script>

@endsection














