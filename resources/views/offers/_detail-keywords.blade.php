<div class="modal fade" id="keywords" tabindex="-1" role="dialog" aria-labelledby="keywords">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Keywords</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Keyword</th>
                    <th class="success">ADN</th>
                    <th class="success">Type</th>
                    <th class="success">Description</th>
                    <th class="success">Component ID</th>
                </tr>
                @forelse ($keywords as $keyword)
                <tr>
                    <td>{{ ($keyword->keyword != null) ? $keyword->keyword : 'No Data' }}</td>
                    <td>{{ ($keyword->adn != null) ? $keyword->adn : 'No Data' }}</td>
                    <td>{{ ($keyword->order_action != null) ? $keyword->order_action : 'No Data'  }}</td>
                    <td>{{ ($keyword->order_description != null) ? $keyword->order_description : 'No Data' }}</td>
                    <td>{{ ($keyword->comp_id != null) ? $keyword->comp_id : 'No Data'  }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5">No Data</td>
                </tr>
                @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
