<div class="modal fade" id="network_site" tabindex="-1" role="dialog" aria-labelledby="network_site">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Resources Profile</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Appsid Charge</th>
                    <th class="success">APN Name</th>
                    <th class="success">UPCC Profile</th>
                    <th class="success">RIM Profile</th>
                    <th class="success">Content Profile 1</th>
		</tr>
		{{-- @forelse($oms_offer as $oms) --}}
                <tr>
                    @if ($oms_offer != '')
                    <td>{{ ($oms_offer->vas_code != null) ?  $oms_offer->vas_code : 'No Data'}}</td>
		    <td>{{ ($oms_offer->apn != null) ? $oms_offer->apn : 'No Data'}}</td>                    
		    <td>{{ ($oms_offer->upcc != null) ?  $oms_offer->upcc : 'No Data'}}</td>
		    <td>{{ ($oms_offer->rim != null) ? $oms_offer->rim : 'No Data'}}</td>
                    <td>{{ ($oms_offer->content_profile1 != null) ? $oms_offer->content_profile1 : 'No Data'}}</td>
		    @else
		    <td> colspan="8">No Data</td>
                    @endif
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
