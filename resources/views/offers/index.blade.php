@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Offer Details</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('raw-style')
<style>
    ul {
        margin: 0;
    }
    li {
        margin: 0;
    }

    .modal .modal-dialog { width: 90%; }

</style>
@endsection

@section('content')
	
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Billing Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Offer ID</th>
                            <th>Offer Name</th>
                            <th>Group Product</th>
			    <th>Owner</th>
                            <th>Charge</th>
                            <th>Allowance</th>
                            <th>Period</th>
                            <th>Criteria</th>
                            <th>View Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @if ($ocs_offer != null)
                                <td>{{ $ocs_offer->offer_id }}</td>
                                <td>{{ $ocs_offer->offer_name }}</td>
                                <td>{{ $ocs_offer->group_product }}</td>
                                <td>{{ $ocs_offer->owner }}</td>
				<td>{{ $ocs_offer->charge }}</td>
                                <td>{{ $ocs_offer->allowance }}</td>
                                <td>{{ $ocs_offer->period }}</td>
                                <td>{{ $ocs_offer->criteria }}</td>
                                <td>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#ocs_charges"><i class="fa fa-eye"></i> Charge</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#ocs_bonus"><i class="fa fa-eye"></i> Bonus</a>
                                   {{-- <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#mapping_product"><i class="fa fa-eye"></i> Mapping Product</a> --}}
				<a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#ocs_allowance"><i class="fa fa-eye"></i>Allowance</a>
				</td>
                                @else
                                <td colspan="5">No Data</td>
                                @endif
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ordering Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Offer ID</th>
                            <th>Offer Name</th>
                            <th>Offer Description</th>
                            <th>Path</th>
                            <th>Code Component</th>
                            <th>Component</th>
                            <th>Effective Date</th>
                            <th>Expire Date</th>
                            {{-- <th>Offer Description</th> --}}
                            <th>View Details</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @if ($oms_offer != null)
                                <td>{{ $oms_offer->offer_id }}</td>
                                <td>{{ $oms_offer->offer_name }}</td>
                                <td>{{ $oms_offer->offer_description }}</td>
                                <td>{{ $oms_offer->path }}</td>
                                <td>{{ $oms_offer->code_component }}</td>
                                <td>{{ $oms_offer->component }}</td>
                                <td>{{ $oms_offer->eff_date }}</td>
                                <td>{{ $oms_offer->exp_date }}</td>
                                {{-- <td>
                                    <ul>
                                    @foreach (explode('|', $oms_offer->sales_channel) as $sc)
                                        @if ($sc != '')
                                        <li>{{ $sc }}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                </td> --}}
                                {{-- <td>
                                    <ul>
                                    @foreach (explode('|', $oms_offer->user_type) as $ut)
                                        @if ($ut != '')
                                        <li>{{ $ut }}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                </td> --}}
                                <td>
				    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#eligibility"><i class="fa fa-eye"></i> Eligibility</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#network_site"><i class="fa fa-eye"></i> Resources Profile</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#notification"><i class="fa fa-eye"></i> Notification</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#keywords"><i class="fa fa-eye"></i> Keyword</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#scheduler"><i class="fa fa-eye"></i> Scheduler</a>
                                    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#specialrules"><i class="fa fa-eye"></i> Special Rules</a>
				    <a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target="#solution"><i class="fa fa-eye"></i> Solution</a>
				</td>
                                @else
				<td colspan="8">No Data</td>
    				@endif
			</tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All Projects</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    @if (count($all_project)) 
                        <center>
                            <strong>
                                <span>There {{ (count($all_project) > 1 ? 'are' : 'is' ) }} {{ count($all_project) }} Project with Offer ID '{{$ocs_offer->offer_id}}'</span>
                            </strong>
                        </center>
                        <br>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Development ID</th>
                            <th>Projet Name</th>
                            <th>Phase</th>
                        </tr>
                        </thead>
                        <tbody>
                            {{-- {{ dd($all_project) }} --}}
                            @forelse ($all_project as $project)
                            <tr>
                                <?php
                                if ($project->phase == 'Plan & Define') {
                                    $phase = 'declare-projects';
                                } else if ($project->phase == 'Design & Develop') {
                                    $phase = 'cab-projects';
                                } else if ($project->phase == 'Test & Readiness') {
                                    $phase = 'post-projects';
                                }
                                ?>
                                <td> <a href="{{ url('/'.$phase.'/'.$project->development_id) }}">{{ $project->development_id }}</a></td>
                                <td>{{ $project->project_name }}</td>
                                <td>{{ $project->phase }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td>No Data</td>
                            </tr>
                            @endforelse
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    {{-- MODALS --}}
    
    @include('offers._detail-charges')
    @include('offers._detail-bonuses')
    @include('offers._detail-allowances')
    @include('offers._detail-eligibilities')
    @include('offers._detail-network-sites')
    @include('offers._detail-notifications')
    @include('offers._detail-keywords')
    @include('offers._detail-schedulers')
    @include('offers._detail-specialrules')
    @include('offers._detail-solution')
    @include('offers._detail-mappingproduct')
@endsection














