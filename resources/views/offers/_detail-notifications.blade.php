<div class="modal fade" id="notification" tabindex="-1" role="dialog" aria-labelledby="notification">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Notifications</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Code Activation</th>
                    <th class="success">Code Deactivation</th>
                    <th class="success">Activation</th>
                    <th class="success">Deactivation</th>
                    <th class="success">SCH 1</th>
                    <th class="success">SCH 2</th>
                    <th class="success">SCH 3</th>
                </tr>
                <tr>
                    @if ($oms_offer != null)
                    <td>{{ $oms_offer->code_act }}</td>
                    <td>{{ $oms_offer->code_deact }}</td>
                    <td>{{ $oms_offer->activation }}</td>
                    <td>{{ $oms_offer->deactivation }}</td>
                    <td>{{ $oms_offer->sch_1 }}</td>
                    <td>{{ $oms_offer->sch_2 }}</td>
                    <td>{{ $oms_offer->sch_3 }}</td>
                    @else
                    <td colspan="7">No Data</td>
                    @endif
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>