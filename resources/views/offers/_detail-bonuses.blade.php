<div class="modal fade" id="ocs_bonus" tabindex="-1" role="dialog" aria-labelledby="ocs_bonus">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Bonus</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Offer Name</th>
                    <th class="success">Bonus ID</th>
                    <th class="success">Bonus Name</th>
                    <th class="success">Bonus Description</th>
                    <th class="success">Packag Criterion</th >
                    <th class="success">Priority</th>
                    <th class="success">Package ID</th>
                    <th class="success">Package Name</th>
                </tr>
                @forelse($ocs_bonus as $bonus)
                <tr>
                    <td>{{ $bonus->offer_name }}</td>
                    <td>{{ $bonus->bonus_id }}</td>
                    <td>{{ $bonus->bonus_name }}</td>
                    <td>{{ $bonus->bonus_desc }}</td>
                    <td>{{ $bonus->package_criterion_ }}</td>
                    <td>{{ $bonus->priority }}</td>
                    <td>{{ $bonus->package_id }}</td>
                    <td>{{ $bonus->package_name }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="8">No Data</td>
                </tr>
                @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
