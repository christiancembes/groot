<div class="modal fade" id="scheduler" tabindex="-1" role="dialog" aria-labelledby="scheduler">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Scheduler</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Scheduler Scenario</th>
                    <th class="success">Contract Duration</th>
                    <th class="success">Contract Period</th>
		    <th class="success">24H Rules</th>
		    <th class="success">Block Allowed</th>
		    <th class="success">Reestablished</th>
		    <th class="success">Whitelist Rule</th>
                    <th class="success">Mirroring</th>
                  {{--  <th class="success">Offer Name Mirroring</th>--}}
                </tr>
                
                <tr>
                    @if ($oms_offer != null)
                    <td>{{ ($oms_offer->scheduler_scenario != null) ? $oms_offer->scheduler_scenario : 'No Data' }}</td>
                    <td>{{ ($oms_offer->contract_duration != null) ? $oms_offer->contract_duration : 'No Data'  }}</td>
                    <td>{{ ($oms_offer->contract_period != null) ? $oms_offer->contract_period : 'No Data'  }}</td>
		    <td>{{ ($oms_offer->rules24h != null) ? $oms_offer->rules24h : 'No Data' }}</td>
		    <td>{{ ($oms_offer->block1_allowed != null) ? $oms_offer->block1_allowed : 'No Data'  }}</td>	
		    <td>{{ ($oms_offer->reestablished != null) ? $oms_offer->reestablished : 'No Data'  }}</td>
		    <td>{{ ($oms_offer->whitelist != null) ? $oms_offer->whitelist : 'No Data'  }}</td>
                    @else
                    <td>No Data</td>
                    @endif
                    <td>
			@if ($mirroring != null)
                        <td>{{ ($mirroring->offer_id_mirroring != null) ? $mirroring->offer_id_mirroring : 'No Data' }} | {{ ($mirroring->offer_name_mirroring != null) ? $mirroring->offer_name_mirroring : 'No Data' }}</td>
			@endif
                    </td>
                   {{--  <td>
                        @if ($mirroring != null)
                        {{ $mirroring->offer_name_mirroring }}
                        @endif
                    </td> --}}
                </tr>
                
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
