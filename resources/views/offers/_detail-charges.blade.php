<div class="modal fade" id="ocs_charges" tabindex="-1" role="dialog" aria-labelledby="ocs_charge">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Charges</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th class="success">Offer Name</th>
                    <th class="success">Bonus ID</th>
                    <th class="success">Bonus Name</th>
                    <th class="success">Bonus Description</th>
                    <th class="success">Priority</th>
                    <th class="success">Package ID</th>
                    <th class="success">Package Name</th>
                    <th class="success">Charge Code</th>
                    <th class="success">Rate</th>
                    <th class="success">Commitment Period</th>
                    <th class="success">Additional Rate</th>
                    <th class="success">Penalty Remaining Period</th>
                    <th class="success">Proration</th>
                    <th class="success">Invoicing Code</th>
                    <th class="success">Revenue Type</th>
                </tr>
                @forelse($ocs_charge as $charge)
                <tr> 
                    
                    <td>{{ $charge->offer_name }}</td>
                    <td>{{ $charge->bonus_id }}</td>
                    <td>{{ $charge->bonus_name }}</td>
                    <td>{{ $charge->bonus_desc }}</td>
                    <td>{{ $charge->priority }}</td>
                    <td>{{ $charge->package_id }}</td>
                    <td>{{ $charge->package_name }}</td>
                    <td>{{ $charge->charge_code }}</td>
                    <td>{{ $charge->rate }}</td>
                    <td>{{ $charge->commitment_period }}</td>
                    <td>{{ $charge->additional_rate }}</td>
                    <td>{{ $charge->penalty_for_remaining_period }}</td>
                    <td>{{ $charge->proration }}</td>
                    <td>{{ $charge->amdd_charge_code }}</td>
                    <td>{{ $charge->revenue_type }}</td>
                 </tr>
                 @empty
                 <tr>
                      <td colspan="8">No data</td>
                 </tr>
                 @endforelse
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
