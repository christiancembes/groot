@extends('layouts.app')

@section('breadcrumb')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2></h2>
	        <ol class="breadcrumb">
	            <li>
	                <a href="/">Home</a>
	            </li>
	            <li class="active">
	                <strong>Layouts</strong>
	            </li>
	        </ol>
	    </div>
	</div>
@endsection

@section('content')
    Home
@endsection
