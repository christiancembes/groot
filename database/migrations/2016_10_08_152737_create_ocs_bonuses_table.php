<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcsBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocs_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->string('offer_name');
            $table->integer('bonus_id');
            $table->string('bonus_type');
            $table->string('bonus_detail');
            $table->string('bonus_info');
            $table->integer('package_id');
            $table->string('package_name');
            $table->integer('priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocs_bonuses');
    }
}
