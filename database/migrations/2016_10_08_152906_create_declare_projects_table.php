<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclareProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declare_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('development_id');
            $table->string('project_name');
            $table->string('phase');
            $table->string('category')->nullable();
            $table->string('bau_ocs')->nullable();
            $table->string('bau_oms')->nullable();
            $table->text('project_brief');
            $table->string('project_document')->nullable();
            $table->string('project_justification')->nullable();
            $table->date('ocs_release');
            $table->date('oms_release');
            $table->date('release_target');
            $table->text('revenue_projection')->nullable();
            $table->text('subscriber_projection')->nullable();
            $table->text('traffic_projection')->nullable();
            $table->string('srs_document')->nullable();
            $table->string('justification_development')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('declare_projects');
    }
}
