<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMirroringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mirrorings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->integer('offer_id_mirroring');
            $table->string('offer_name_mirroring');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mirrorings');
    }
}
