<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('development_id');
            $table->string('project_name');
            $table->string('phase');
            $table->date('release_test')->nullable();
            $table->string('justification_release')->nullable();
            $table->date('rfs_schedule')->nullable();
            $table->text('remark_project')->nullable();
            $table->string('justification_rfs')->nullable();
            $table->string('status_service')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_projects');
    }
}
