<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcsOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocs_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->string('offer_name');
            $table->string('type1');
            $table->string('subtype1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocs_offers');
    }
}
