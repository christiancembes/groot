<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricePlanReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_plan_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('month_and_year');
            $table->string('price_plan1');
            $table->integer('number_price_plan1');
            $table->string('price_plan2');
            $table->integer('number_price_plan2');
            $table->string('price_plan3');
            $table->integer('number_price_plan3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_plan_reports');
    }
}
