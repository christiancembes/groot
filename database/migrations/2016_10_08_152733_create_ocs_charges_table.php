<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcsChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocs_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->string('offer_name');
            $table->string('charging_type');
            $table->string('charge');
            $table->string('vascode_name');
            $table->string('period');
            $table->string('proration');
            $table->string('penalty_indicator');
            $table->string('penalty_formula');
            $table->string('billing_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocs_charges');
    }
}
