<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bau');
            $table->text('product_spec_bau');
            $table->string('justification_bau');
            $table->text('list_bau');
            $table->date('timeline_bau');
            $table->string('testcase_bau');
            $table->string('result_bau');
            $table->double('recid', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locos');
    }
}
