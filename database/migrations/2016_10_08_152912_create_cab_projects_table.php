<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cab_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('development_id');
            $table->string('project_name');
            $table->string('phase');
            $table->text('business_flow')->nullable();
            $table->text('bill')->nullable();
            $table->text('prov')->nullable();
            $table->text('rep')->nullable();
            $table->date('development_schedule')->nullable();
            $table->date('uat_schedule')->nullable();
            $table->date('actual_uat_schedule')->nullable();
            $table->date('release_schedule')->nullable();
            $table->date('actual_release_schedule')->nullable();
            $table->string('status_project')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cab_projects');
    }
}
