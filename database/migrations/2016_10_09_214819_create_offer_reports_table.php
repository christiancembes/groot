<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('month_and_year');
            $table->string('legacy_name');
            $table->integer('legacy_number');
            $table->string('core_name');
            $table->integer('core_number');
            $table->string('bundling_name');
            $table->integer('bundling_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_reports');
    }
}
