<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmsOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oms_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id');
            $table->string('offer_name');
            $table->string('code_component');
            $table->string('component');
            $table->string('owner');
            $table->text('sales_channel');
            $table->text('user_type');
            $table->string('customer_type');
            $table->string('sub_type');
            $table->string('vas_code');
            $table->text('apn');
            $table->text('sapc');
            $table->string('rim');
            $table->string('code_act');
            $table->string('code_deact');
            $table->text('activation');
            $table->text('deactivation');
            $table->text('sch_1');
            $table->text('sch_2');
            $table->text('sch_3');
            $table->integer('scheduler_scenario');
            $table->string('contract_duration');
            $table->string('rules24h');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oms_offers');
    }
}
