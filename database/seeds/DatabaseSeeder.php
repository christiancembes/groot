<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // disable foreign key constraints
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); 

        $this->call(UsersTableSeeder::class);
        $this->call(OcsOfferTableSeeder::class);
        $this->call(OmsOfferTableSeeder::class);
        $this->call(RestrictionTableSeeder::class);
        $this->call(PricePlanTableSeeder::class);
        $this->call(PrerequisiteTableSeeder::class);
        $this->call(OcsChargesTableSeeder::class);
        $this->call(OcsBonusTableSeeder::class);
        $this->call(MirroringTableSeeder::class);
        $this->call(LocosTableSeeder::class);
        $this->call(KeywordTableSeeder::class);
        $this->call(BilcatsTableSeeder::class);
        $this->call(DeclareProjectTableSeeder::class);
        $this->call(CabProjectTableSeeder::class);
        $this->call(PostProjectTableSeeder::class);
        $this->call(ProjectOcsOfferTableSeeder::class);
        $this->call(CustomerBaseReportTableSeeder::class);
        $this->call(OfferReportTableSeeder::class);
        $this->call(PricePlanReportTableSeeder::class);

        // enable foreign key constraints
        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); 

        Model::reguard();
    }
}
