-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: l52_zoro
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `offer_reports`
--

DROP TABLE IF EXISTS `offer_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_and_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legacy_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legacy_number` int(11) NOT NULL,
  `core_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `core_number` int(11) NOT NULL,
  `bundling_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bundling_number` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer_reports`
--

LOCK TABLES `offer_reports` WRITE;
/*!40000 ALTER TABLE `offer_reports` DISABLE KEYS */;
INSERT INTO `offer_reports` VALUES (1,'Jan-16','voice 100K',2250896,'flash 225K',980987,'Myplan 100K',990876,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(2,'Feb-16','voice 200K',2456789,'flash 150K',990876,'Myplan 250K',990876,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(3,'Mar-16','voice 100K',2456789,'flash 150K',1000670,'Myplan 100K',990876,'2016-10-18 11:32:27','2016-10-18 11:32:27');
/*!40000 ALTER TABLE `offer_reports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18 12:39:07
