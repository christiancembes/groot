-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: l52_zoro
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer_base_reports`
--

DROP TABLE IF EXISTS `customer_base_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_base_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_and_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_regular` int(11) NOT NULL,
  `number_of_corporate` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_base_reports`
--

LOCK TABLES `customer_base_reports` WRITE;
/*!40000 ALTER TABLE `customer_base_reports` DISABLE KEYS */;
INSERT INTO `customer_base_reports` VALUES (1,'Jan-16',2250896,980987,3231883,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(2,'Feb-16',2456789,2456789,3447665,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(3,'Mar-16',2456789,1000670,3457459,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(4,'Apr-16',2456789,998907,3455696,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(5,'May-16',2508943,990876,3499819,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(6,'Jun-16',2550889,990876,3541765,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(7,'Jul-16',2476543,990876,3467419,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(8,'Aug-16',2476543,990876,3467419,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(9,'Sep-16',2356789,990876,3347665,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(10,'Oct-16',2550889,1000670,3551559,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(11,'Nov-16',2676543,998907,3675450,'2016-10-18 11:32:27','2016-10-18 11:32:27'),(12,'Dec-16',2756789,1040340,3797129,'2016-10-18 11:32:27','2016-10-18 11:32:27');
/*!40000 ALTER TABLE `customer_base_reports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18 12:38:54
