-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: l52_zoro
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `price_plan_reports`
--

DROP TABLE IF EXISTS `price_plan_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_plan_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_and_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_plan1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_price_plan1` int(11) NOT NULL,
  `price_plan2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_price_plan2` int(11) NOT NULL,
  `price_plan3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_price_plan3` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_plan_reports`
--

LOCK TABLES `price_plan_reports` WRITE;
/*!40000 ALTER TABLE `price_plan_reports` DISABLE KEYS */;
INSERT INTO `price_plan_reports` VALUES (1,'Jan-16','KartuHALO iPhone v2',4000,'KartuHALO Data v2',98765,'KartuHALO Corporate Business',102765,'2016-10-18 12:18:27','2016-10-18 12:18:27'),(2,'Feb-16','KartuHALO Bebas Komunitas v2',98765,'KartuHALO Bebas Bicara v2',102765,'KartuHALO Bebas Abonemen v2',4000,'2016-10-18 12:19:17','2016-10-18 12:19:17'),(3,'Mar-16','KartuHALO SME',2476543,'KartuHALO Employee',98765,'KartuHALO Hybrid v2',111198,'2016-10-18 12:20:10','2016-10-18 12:20:10');
/*!40000 ALTER TABLE `price_plan_reports` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18 12:39:01
