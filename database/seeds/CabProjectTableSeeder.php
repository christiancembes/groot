<?php

use Illuminate\Database\Seeder;

class CabProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump = database_path('seeds/dumps/cab_project.sql');

        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        $command = "mysql -u $username -p$password $database < $dump";

        exec($command);
    }
}
