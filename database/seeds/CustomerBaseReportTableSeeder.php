<?php

use Illuminate\Database\Seeder;
use App\Models\CustomerBaseReport;

class CustomerBaseReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Jan-16';
        // $category->number_of_regular = 2250896;
        // $category->number_of_corporate = 980987;
        // $category->total = 3231883;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Feb-16';
        // $category->number_of_regular = 2456789;
        // $category->number_of_corporate = 2456789;
        // $category->total = 3447665;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Mar-16';
        // $category->number_of_regular = 2456789;
        // $category->number_of_corporate = 1000670;
        // $category->total = 3457459;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Apr-16';
        // $category->number_of_regular = 2456789;
        // $category->number_of_corporate = 998907;
        // $category->total = 3455696;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'May-16';
        // $category->number_of_regular = 2508943;
        // $category->number_of_corporate = 990876;
        // $category->total = 3499819;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Jun-16';
        // $category->number_of_regular = 2550889;
        // $category->number_of_corporate = 990876;
        // $category->total = 3541765;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Jul-16';
        // $category->number_of_regular = 2476543;
        // $category->number_of_corporate = 990876;
        // $category->total = 3467419;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Aug-16';
        // $category->number_of_regular = 2476543;
        // $category->number_of_corporate = 990876;
        // $category->total = 3467419;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Sep-16';
        // $category->number_of_regular = 2356789;
        // $category->number_of_corporate = 990876;
        // $category->total = 3347665;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Oct-16';
        // $category->number_of_regular = 2550889;
        // $category->number_of_corporate = 1000670;
        // $category->total = 3551559;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Nov-16';
        // $category->number_of_regular = 2676543;
        // $category->number_of_corporate = 998907;
        // $category->total = 3675450;
        // $category->save();

        // $category = new CustomerBaseReport;
        // $category->month_and_year = 'Dec-16';
        // $category->number_of_regular = 2756789;
        // $category->number_of_corporate = 1040340;
        // $category->total = 3797129;
        // $category->save();
        
        $dump = database_path('seeds/dumps/customer_base_reports.sql');

        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        $command = "mysql -u $username -p$password $database < $dump";

        exec($command);
    }
}
