<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Admin";
		$adminRole->save();
	
	    // Membuat role customer
		$userRole = new Role();
		$userRole->name = "user";
		$userRole->display_name = "User";
		$userRole->save();

		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin';
		$admin->email = 'admin@gmail.com';
		$admin->password = bcrypt('secret');
		$admin->is_verified = 1;
		$admin->save();

		$admin->attachRole($adminRole);
		
		// Membuat sample user
		$user = new User();
		$user->name = "Basic User";
		$user->email = 'user@gmail.com';
		$user->password = bcrypt('secret');
		$user->is_verified = 1;
		$user->save();

		// Membuat sample user
		$user2 = new User();
		$user2->name = "Basic User 2";
		$user2->email = 'user2@gmail.com';
		$user2->password = bcrypt('secret');
		$user2->is_verified = 1;
		$user2->save();

		$user->attachRole($userRole);
		$user2->attachRole($userRole);
    }
}
