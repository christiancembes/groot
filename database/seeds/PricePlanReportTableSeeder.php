<?php

use Illuminate\Database\Seeder;

class PricePlanReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump = database_path('seeds/dumps/price_plan_reports.sql');

        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        $command = "mysql -u $username -p$password $database < $dump";

        exec($command);
    }
}
