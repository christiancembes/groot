<?php

use App\Models\OfferReport;
use Illuminate\Database\Seeder;

class OfferReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $category = new OfferReport;
        // $category->month_and_year = 'Jan-16';
        // $category->legacy_name = 'voice 100K';
        // $category->legacy_number = 2250896;
        // $category->core_name = 'flash 225K';
        // $category->core_number = 980987;
        // $category->bundling_name = 'Myplan 100K';
        // $category->bundling_number = 990876;
        // $category->save();

        // $category = new OfferReport;
        // $category->month_and_year = 'Feb-16';
        // $category->legacy_name = 'voice 200K';
        // $category->legacy_number = 2456789;
        // $category->core_name = 'flash 150K';
        // $category->core_number = 990876;
        // $category->bundling_name = 'Myplan 250K';
        // $category->bundling_number = 990876;
        // $category->save();

        // $category = new OfferReport;
        // $category->month_and_year = 'Mar-16';
        // $category->legacy_name = 'voice 100K';
        // $category->legacy_number = 2456789;
        // $category->core_name = 'flash 150K';
        // $category->core_number = 1000670;
        // $category->bundling_name = 'Myplan 100K';
        // $category->bundling_number = 990876;
        // $category->save();
        
        $dump = database_path('seeds/dumps/offer_reports.sql');

        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        $command = "mysql -u $username -p$password $database < $dump";

        exec($command);
    }
}
