var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    
    // backend
    mix.styles([
    	'font-awesome/css/font-awesome.min.css',
    	'bootstrap.min.css',
        'daterangepicker/daterangepicker.css',
    	'datapicker/datepicker3.css',
    	'fullcalendar/fullcalendar.css',
    	'select2/select2.min.css',
        'sweetalert/sweetalert.css',
    	'toastr/toastr.min.css',
    	'animate.css',
    	'style.css',
        'custom.css',
    ], 'public/css/app.css');


    // backend
    mix.scripts([
        'jquery-2.1.1.js',
        'bootstrap.min.js',
        'moment.min.js',
        'datapicker/bootstrap-datepicker.js',
        'daterangepicker/daterangepicker.js',
        'fullcalendar/fullcalendar.min.js',
        'metisMenu/jquery.metisMenu.js',
        'slimscroll/jquery.slimscroll.min.js',
        'inspinia.js',
        'chartJs/Chart.min.js',
        'sweetalert/sweetalert.min.js',
        'select2/select2.full.min.js',
        'toastr/toastr.min.js',
        'custom.js',
    ], 'public/js/app.js')

    mix.version(['css/app.css', 'js/app.js']);

    mix.copy('resources/assets/img', 'public/img');
    mix.copy('resources/assets/fonts', 'public/fonts');
    mix.copy('resources/assets/fonts', 'public/build/fonts');

});
